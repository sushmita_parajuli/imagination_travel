<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common_functions
 *
 * @author prabin
 */
class Common_functions extends MX_Controller {

//protected $MODULE = null;
    public function __construct() {
        parent::__construct();
        $this->load->model('modules/Mdl_moduleslist');
    }

    function check_permission($group_id, $module_name) {
        if ($group_id != 1) {
            $roles = $this->Mdl_permissions->unserialize_role_array($group_id);
//            echo '<pre>',print_r($roles),'</pre>';die('klnsd');
            $roles_combined = implode(" ", $roles);

            $module_id = $this->Mdl_moduleslist->get_id_from_modulename($module_name);
            if (strpos($roles_combined, 'e' . $module_id)) {
                $permission['edit'] = TRUE;
            }
            if (strpos($roles_combined, 'a' . $module_id)) {
                $permission['add'] = TRUE;
            }
            if (strpos($roles_combined, 'v' . $module_id)) {
                $permission['view'] = TRUE;
            }
            if (strpos($roles_combined, 'd' . $module_id)) {
                $permission['delete'] = TRUE;
            }
            return $permission;
        } else {
            $permission['edit'] = TRUE;
            $permission['add'] = TRUE;
            $permission['delete'] = TRUE;
            $permission['view'] = TRUE;
            return $permission;
        }
    }

    function check_only_permission($group_id) {
//        var_dump($group_id);
        if ($group_id != 1) {
            $roles = $this->Mdl_permissions->unserialize_role_array($group_id);
//            echo '<pre>',print_r($roles),'</pre>';die('klnsd');
//            var_dump($roles);die;
//            $roles_combined = implode(" ", $roles);
//            var_dump($roles_combined);die;
//            for($i= 0 ; $i<count($roles);$i++){
//                $module_id = $this->Mdl_moduleslist->get_id_from_module($roles);
//                print_r($module_id);
////                return $module_id;
//            }die;

            foreach ($roles as $key => $row) {
//               
                $data[$key] = $this->Mdl_moduleslist->get_id_from_module($row);
//                var_dump($data['module_id']);
            } return $data;
        } else {
            $permission['edit'] = TRUE;
            $permission['add'] = TRUE;
            $permission['delete'] = TRUE;
            $permission['view'] = TRUE;
            return $permission;
        }
    }

    function get_data_from_db($update_id, $select, $model) {
        $query = $this->$model->get_where_dynamic($update_id, $select);
        foreach ($query->result() as $key => $value) {
            foreach ($value as $k => $v) {
                $data[$k] = $v;
            }
        }
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }

    function get_dropdown($order_by, $select, $params, $model) {
        $dropdowns = $this->$model->get_where_dynamic('', $select, $order_by, '', '', $params, '')->result();
        if ($model == 'Mdl_navigation') {
            $dropdownlist[0] = ''; //for making parent value null as default
        }
        if ($model == 'mdl_groups') {
            $dropdownlist[0] = 'select group';
        }
        $s = explode(',', $select);
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->$s[0]] = $dropdown->$s[1];
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function delete_attachment($id, $model, $module) {
        // var_dump($module);die;
        $query = $this->$model->get_where_dynamic($id);
        // var_dump($query->result());die;
        foreach ($query->result() as $key => $value) {
            foreach ($value as $k => $v) {
                $data[$k] = $v;
            }
        }
        if ($data['attachment'] != NULL) {
            unlink($_SERVER['DOCUMENT_ROOT'] . '/imagination_git/uploads/' . $module . '/' . $data['attachment']);
            $message = ' Attachment was also deleted successfully!!!';
        } else {
            $message = ' There was not any attachment for this module!!';
        }
        return $message;
    }

    function do_upload($id, $module) {
        $config['upload_path'] = "./uploads/" . $module . "/";
        $config['file_name'] = md5($id);
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = "gif|jpg|jpeg|png";
        $config['max_size'] = "20480"; //that's 20MB
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";

        $this->load->library('upload', $config);


        if (!$this->upload->do_upload()) {
            // echo 'File cannot be uploaded';
            $datas = array('error' => $this->upload->display_errors());
        } else {
            // echo 'File has been uploaded';
            $datas = array('upload_data' => $this->upload->data());
        }

        return $datas;
    }

    function do_upload_multiple($id, $module, $count = null) {
        $this->load->library('upload');
        $files = $_FILES;
        if($count!=null){
            
            $j=1;
        for($i=0;$i<$count;$i++){
            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            if($j <= 6){
            $this->upload->initialize($this->set_upload_options($id,$module ,$i));
            }else{
            $this->upload->initialize($this->set_upload_options($id,$module,$i)); 
            }
            $j++;
            $this->upload->do_upload();
            
            $temp = $this->upload->data();
            $ext = $temp['file_ext'];
            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */            
            
              
//            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */
//            
            $datas[$i] = array('upload_data' => $this->upload->data());
           
//            $i++;
        }
        return $datas;
    
     }else{
         $i=0;
         foreach ($data_lang as $row) {
        
            $_FILES['userfile']['name'] = $files['userfile']['name'][$i ];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i ];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i ];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i ];

            $this->upload->initialize($this->set_upload_options($id,$module,$i));
            $this->upload->do_upload();
            
            $temp = $this->upload->data();
            $ext = $temp['file_ext'];
            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */            
            
              
//            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */
//            
            $datas[$i] = array('upload_data' => $this->upload->data());
            $i++;
        }
        // var_dump($data);
        return $datas;
       
     }
    }

    private function set_upload_options($id, $module,$i) {
//  upload an image options
        $config = array();
        $config['upload_path'] = './uploads/' . $module . '/';
//        var_dump($config);die;
//        $config['allowed_types'] = 'gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|docx|doc';
        $config['max_size'] = '20480';
        $config['overwrite'] = TRUE;
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['file_name'] = md5($id).$i;
        return $config;
    }

    function image_optimization($id, $module) {
        $file = $_FILES['userfile']['name'];
        $type = explode('.', $file);
        $type = $type[1];
        $config['image_library'] = 'gd2';
        $config['allowed_types'] = "gif|jpg|jpeg|png|docx|doc";
        $config['source_image'] = './uploads/' . $module . '/' . $id . '.' . $type;
        //$config['new_image']  = './uploads/notice_board/';
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = FALSE;
        $config['width'] = 200;
        $config['height'] = 250;
        $this->load->library('image_lib', $config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
            die();
        } else {
            echo 'success';
        }
    }

}
