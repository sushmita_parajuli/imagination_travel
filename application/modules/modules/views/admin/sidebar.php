<?php /*$modules is generated from admin_template controller of template module*/?>
<li>  
    <a href="javascript:void(0);">  
        <i class="icon-book"></i> Modules   
    </a>  
    <ul class="sub-menu">  
        <li>  
            <a href="<?php echo base_url().'admin/modules';?>">  
                <i class="icon-pencil"></i> Manage Modules 
            </a>  
        </li>  
        <?php foreach($modules as $row){ //displaying all modules ?>
        <li>  
            <a href="<?php echo base_url().'admin/'.$row['slug']?>">  
                <i class="icon-angle-right"></i> <?php echo $row['title']?>  
            </a>  
        </li>    
        <?php }?>
    </ul>  
</li>  