<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_trekking extends Mdl_crud {



    protected $_table = "up_trekking";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_trekking() {

        $table = $this->_table;

        $this->db->where('status', 'live');
        $this->db->order_by('id','DESC');
        $query = $this->db->get($table);

        return $query;

    }



    function get_trekking_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('up_trekking')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->title] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }

    function get_country_dropdown() {

        $this->db->select('id, country_name');

        $this->db->order_by('id');

        $dropdowns = $this->db->get('tbl_countries')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->country_name] = $dropdown->country_name;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }

    function get_details_from_slug($slug){
       $table= 'up_services';
       $this->db->where('slug',$slug);
       $query=$this->db->get($table);
       return $query;
   }


    function get_details_front($slug){
       $table= 'up_trekking';
       $this->db->where('slug',$slug);
       $query=$this->db->get($table);
       return $query;
   }

    function get_details($slug){
       $table= $this->_table;
       $this->db->where('slug',$slug);
       $query=$this->db->get($table);
       return $query;
   }

   function get(){
    $table = $this->_table;
    $this->db->where('status','live');
    $this->db->order_by('id','ASC');
    $query=$this->db->get($table);
    return $query;
}
function get_more($slug){
    $table = $this->_table;
    $this->db->where('status','live');
    $this->db->where('slug !=',$slug);
    $this->db->order_by('id','ASC');
    $query=$this->db->get($table);
    return $query;
}



}

