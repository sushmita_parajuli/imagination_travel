<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trekking extends MX_Controller {
    private $MODULE_PATH = "trekking/index";
    private $group_id;
    private $MODULE = 'trekking';
    private $model_name = 'mdl_trekking';


    function __construct() {
        $this->load->library('session');
        $this->load->helper('form');
        parent::__construct();

        $this->load->model('mdl_trekking');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }

    function index()
    {
     $slug=$this->uri->segment(1);
     $data['detail']=$this->get_details_from_slug($slug);
     $data['details']=$data['detail'];
     $data['query']= $this->get();
     // var_dump($data['query']);die;
     $data['view_file']="trekking";
     $this->load->module('template');
     $this->template->front($data);       
 }

 function details(){
    $slug=$this->uri->segment(3);
    $data['detail']=$this->get_detail_front($slug);
    $data['details']=$data['detail'];
    $data['country']= $this->get_country();
    $data['gallery']=$this->get_gallery($slug);
     // var_dump($data['gallery']);die;
    $data['query']= $this->get_more($slug);
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);

}

function get_detail_front($slug)
{
    $this->load->model('mdl_trekking');
    $query = $this->mdl_trekking->get_details_front($slug);
    return $query->result_array();
}

function get(){
 $this->load->model('mdl_trekking');
 $query = $this->mdl_trekking->get();
 return $query->result_array();
}
function get_more($slug){
 $this->load->model('mdl_trekking');
 $query = $this->mdl_trekking->get_more($slug);
 return $query->result_array();
}

function get_gallery($slug){
 $this->load->model('gallery/Mdl_gallery');
 $query = $this->Mdl_gallery->get_gallery($slug);
 return $query->result_array();
}

function get_country(){
    $this->load->model('mdl_trekking');
    $query = $this->mdl_trekking->get_country_dropdown();
    return $query;
}

function get_details_from_slug($slug)
{
    $this->load->model('mdl_trekking');
    $query = $this->mdl_trekking->get_details_from_slug($slug);
    return $query->result_array();
}

function submit() {

    $data['package'] = $package = $this->input->post('packages', TRUE);
    // var_dump($package);die;
    $data['start'] = $start = $this->input->post('start', TRUE);
    $data['end'] = $end = $this->input->post('end', TRUE);
    $data['people'] = $people = $this->input->post('people', TRUE);
    $data['name'] = $name = $this->input->post('name', TRUE);
    $data['country'] = $country = $this->input->post('country', TRUE);
    $data['dob'] = $dob = $this->input->post('dob', TRUE);
    $data['citizen'] = $citizen = $this->input->post('citizen', TRUE);
    $data['email'] = $sender = $this->input->post('email', TRUE);
    $data['phone'] = $phone = $this->input->post('phone', TRUE);
    $message = "name:".$name . PHP_EOL."phone:".$phone . PHP_EOL."country:".$country . PHP_EOL."dob:".$dob . PHP_EOL ."citizen no:" .$citizen . PHP_EOL."package:".$package . PHP_EOL."start date:".$start .PHP_EOL."end date:".$end.PHP_EOL."No of people:".$people;
    $status = $this->send_email($sender, $message, $name);
    if ($status == true) {
        echo true;
    } else {
        echo false;
    }
}


function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
//    $config = Array(
//        'protocol' => 'smtp',
//        'smtp_host' => 'ssl://smtp.googlemail.com',
//        'smtp_port' => 465,
//            'smtp_user' => 'bibek.munikar@gmail.com', // change it to yours
//            // 'smtp_pass' => 'sushie184617',  
//            // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
    $config['protocol'] = 'mail';
    $config['mailpath'] = '/usr/sbin/mail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('bibek.munikar@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Book Now Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}
