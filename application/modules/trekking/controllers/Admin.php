<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/trekking";
    private $MODULE = "trekking";
    private $group_id;
    private $model_name = 'Mdl_trekking';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('common_functions');
        $this->load->model('Mdl_trekking');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('trekking'); //module name is trekking here	
    }

    function index() {
        //table select parameters
        $main_table_params = 'id,title,slug,description,attachment,status,latitude,longitude,itinerary';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
//           
        } else {
            $params = '';
        }
        $count = $this->Mdl_trekking->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

//        print_r($params);
//        $count = $this->Mdl_trekking->count();
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_trekking->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
//        $data['query'] = $this->Mdl_trekking->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);

        $data['columns'] = array('title', 'itinerary', 'description', 'attachment', 'status');

        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
//         $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['title'] = $this->input->post('title', TRUE);
        $data['itinerary'] = $this->input->post('itinerary', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));
        $data['search_keys'] = $this->input->post('search_keys', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);
        $data['latitude'] = $this->input->post('latitude', TRUE);
        $data['longitude'] = $this->input->post('longitude', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        $data['option'] = $this->input->post('option', TRUE);
        if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            $data['attachment'] = $attach['attachment'];
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['attachment'] = $this->input->post('userfile', TRUE);
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function get_attachment_from_db($update_id) {
        $query = $this->Mdl_trekking->get_where_dynamic($update_id);
        foreach ($query->result() as $row) {
            $data['attachment'] = $row->attachment;
        }
        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,description,itinerary,attachment,status,meta_description,search_keys,latitude,longitude,option';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }

        $data['update_id'] = $update_id;

        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {

        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/trekking');
        } else {
            // $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_trekking->_delete($delete_id);
            $url = 'trekking';
            echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!' . $message);
            // redirect('admin/trekking');
        }
    }

    function submit() {
        //no validation in trekking because it has been created from jquery	
        $data = $this->get_data_from_post();
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

            $attach = $this->get_attachment_from_db($update_id);
            $uploadattachment = $this->common_functions->do_upload($update_id, $this->MODULE);
            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            if (empty($data['attachment'])) {
                $data['attachment'] = $attach['attachment'];
            }
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            if (isset($permission['edit'])) {
                $this->Mdl_trekking->_update($update_id, $data);
            }


            $this->session->set_flashdata('operation', 'Updated Successfully!!!');
        } else {
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);

            $nextid = $this->Mdl_trekking->get_max();
            $uploadattachment = $this->common_functions->do_upload($nextid, $this->MODULE);
            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            if (isset($permission['add'])) {
                $this->Mdl_trekking->_insert($data);
            }
            $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
        }
        $url = 'trekking';
        echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
        //redirect('admin/trekking');
    }

}
