<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/css/datepicker.css' ?>" />
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/less/datepicker.less' ?>" />  
<script src="<?php echo base_url() . 'bootstrap/datepicker/js/bootstrap-datepicker.js' ?>"></script>
<div class="clearfix">
	<div class="content-wrap pad clearfix">
		<!-- service -->
		<div class="clearfix">
			<div class="row">
				<?php  foreach ($details as $row) {?>
				<div class="col-sm-12">
					<div class="trek-description clearfix">
						<figure class="photo">
							<img class="img-responsive" src="<?php echo base_url()?>uploads/trekking/<?php echo $row['attachment']?>" alt="">
						</figure>
						<div class="title">
							<h2><?php echo $row['title']; ?> </h2>
						</div>
						<div class="cont">

							<p> <?php echo $row['description']; ?></p>

						</div>

					</div>
				</div>


				
				<div class="col-sm-8">

					<div class="responsive-tabs itinerary">
						<h2>Itinerary</h2>
						<div>
							<?php echo $row['itinerary']; ?>
						</div>

						<h2>Map</h2>
						<div id= "map" style="width:100%; height:400px"></div>

						
							<!-- <div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3509.622259796673!2d83.69187431467418!3d28.40047498250986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995fb974774e9cb%3A0x36846e2b0a54a8d2!2sPoon+Hill+Marga%2C+Ghode+Pani+33200!5e0!3m2!1sen!2snp!4v1502518406606" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div> -->
						
						
						<h2>Gallery</h2>
						<div class="gallery">
							<div class="row">
								<div class="col-sm-4">
									<a href="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" class="">
										<img src="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" alt="" title="Beautiful Image" />
									</a>
								</div>
								<div class="col-sm-4">

									<a href="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" class="">
										<img src="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" alt="" title="Beautiful Image" />
									</a>
								</div>
								<div class="col-sm-4">

									<a href="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" class="">
										<img src="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" alt="" title="Beautiful Image" />
									</a>
								</div>
								<div class="col-sm-4">

									<a href="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" class="">
										<img src="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" alt="" title="Beautiful Image" />
									</a>
								</div>
								<div class="col-sm-4">

									<a href="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" class="">
										<img src="<?php echo base_url()?>design/frontend/image/package/kathmandu.jpg" alt="" title="Beautiful Image" />
									</a>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php	} ?>

				<div class="col-md-4 trek-package">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Treking Activities</h3>
						</div>
						<div class="panel-body">
							<ul class="activities-links">
								<?php foreach ($query as $rows) {?>
								<li><a href="<?php echo base_url()?>trekking/details/<?php echo $rows['slug']; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $rows['title']?></a></li>
								<?php }?>
							</ul>
						</div>

					</div>

				</div>
				<div id = "replaceDiv"> </div>
				<div class="book-form-wrap col-sm-10 col-sm-offset-1">
					<div class="book-form-holder">
						<div class="heading">
							<h2 class="text-center">Book Now</h2>
						</div>
						<div class="form-wrapper clearfix">
							<div class="col-sm-12 holder">
								<?php echo form_open('', 'id="f1" class="book_now"'); ?>
								<div class="col-sm-4">
									<h4 class="col-sm-12">Package Info</h4>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="col-sm-12">
											<?php $package  = $this->uri->segment(3);
											$package_title = ucfirst(str_replace('-', ' ', $package));
											?>
											<input  id= "b_package"type="text" class=" form-control" name="package name" value="<?php echo $package_title?>" placeholder="package name">
										</div>
									</div>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Email</label> -->
										<div class="col-sm-12">
											<input type="text" class="form-control input-group date" id="b_start" data-date="" data-date-format="yyyy-mm-dd"  data-provide="datepicker" placeholder="Start Date">
											<!-- <input id="start_datepicker"  class="form-control" name="dob" placeholder="Start Date"> -->
										</div>


									</div>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="phone">Phone</label> -->
										<div class="col-sm-12">
											<input id="b_end"type="text" class="form-control" name="country" placeholder="End Date">
										</div>
									</div>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="col-sm-12">
											<input id="b_people"type="text" class=" form-control" name="people" placeholder="Number of People">
										</div>
									</div>
								</div>
								<div class="col-sm-8">
									<h4 class="col-sm-12">Personal Info</h4>
									<div class="form-group col-sm-6">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<input id="b_name" type="text" class=" form-control" name="name" placeholder="Your Full Name">
									</div>

									
									<!-- <label class="col-sm-2" for="phone">Phone</label> -->
										<!-- <div class="col-sm-12">
											<select  id= "b_country" type="text" class="form-control" name="country" placeholder="Enter Your Country">
											</div> -->
											
											<div class="form-group col-sm-6">
													<!-- <select class="form-control" id="sel1">
														<option>1</option>
														
													</select> -->
													<?php 
													$selected = $country;
													$options = $country;
													echo form_dropdown('country', $options, $selected ,'id="b_country" class="form-control"');
													?>
												</div> 
												<div class="form-group col-sm-6">
													<!-- <label class="col-sm-2" for="name">Email</label> -->
													<div class="">
														<input id="b_dob" type="email" class="form-control" name="dob" placeholder="Your DOB">
													</div>

												</div>
												<div class="form-group col-sm-6">
													<!-- <label class="col-sm-2" for="name">Name</label> -->
													<div class="">
														<input id="b_citizen" type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<!-- <label class="col-sm-2" for="name">Email</label> -->
													<div class="">
														<input id="b_email" type="email" class="form-control" name="email" placeholder="Your Email Address">
													</div>

												</div>
												<div class="form-group col-sm-6">
													<!-- <label class="col-sm-2" for="phone">Phone</label> -->
													<div class="">
														<input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number">
													</div>
												</div>
												<div class="col-sm-12">
													<input type="checkbox" name="vehicle1" value="Bike"  id="termsCheck"> I agree all terms and condition<br>
												</div>
												<div class="btn-wrap col-sm-12">
													<a onclick="submit()" id="submit1" class="btn btn-success">Book Now
													</a>
												</div>
											</div>

											<?php echo form_close(); ?>
										</div>
										<div id="replaceDiv"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- /.service end -->
			</div> <!-- /.content-wrap end -->
			<script>
// $('#submit1').click(function ()
// {
// 	if($('#termsCheck').prop('checked'))
// 	{
// 		location.href="register/registration_form";     
// 	}
// 	else
// 	{
// 		alert('Please accept our terms and conditions.');      
// 	}
// });   
// </script>

<script type="text/javascript">
function submit() {

	var base_url = ' <?php echo base_url(); ?>';
	var b_package = $("#b_package").val();
	var b_start = $("#b_start").val();
	var b_end = $("#b_end").val();
	var b_people = $("#b_people").val();
	var b_citizen = $("#b_citizen").val();
	var b_name = $('#b_name').val();
	var b_country = $('#b_country').val();
	var b_dob = $('#b_dob').val();
	var b_email = $('#b_email').val();
	var b_phone = $('#b_phone').val();
	if (b_package != '' && b_start != '' && b_start != '' && b_end != '' && b_citizen != '' && b_name !='' &&
		b_country != '' && b_dob != '' && b_email != '' && b_phone != '' && b_people != '') {
		$.ajax({
			type: 'POST',
			url: base_url + 'trekking/submit',
			data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', packages: b_package, start: b_start, end: b_end, citizen: b_citizen, name: b_name, country: b_country, dob:b_dob , email: b_email , phone: b_phone},
			success: function (data) {
				if (data == true) {
					$('#replaceDiv').html('<div  class="col-md-12" style="margin-top:-20px;">Thank you!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                	$('#replaceDiv').html('<div class="col-md-12" style="margin-top:-20px;">Sorry! Try again later.</div>');
                                                }

                                            }
                                        });
} else {
	alert('Please fill up all fields');
}
}


// modal image gallery
$(document).ready(function(){

	loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current){
    	$('#show-previous-image, #show-next-image').show();
    	if(counter_max == counter_current){
    		$('#show-next-image').hide();
    	} else if (counter_current == 1){
    		$('#show-previous-image').hide();
    	}
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

     function loadGallery(setIDs, setClickAttr){
     	var current_image,
     	selector,
     	counter = 0;

     	$('#show-next-image, #show-previous-image').click(function(){
     		if($(this).attr('id') == 'show-previous-image'){
     			current_image--;
     		} else {
     			current_image++;
     		}

     		selector = $('[data-image-id="' + current_image + '"]');
     		updateGallery(selector);
     	});

     	function updateGallery(selector) {
     		var $sel = selector;
     		current_image = $sel.data('image-id');
     		$('#image-gallery-caption').text($sel.data('caption'));
     		$('#image-gallery-title').text($sel.data('title'));
     		$('#image-gallery-image').attr('src', $sel.data('image'));
     		disableButtons(counter, $sel.data('image-id'));
     	}

     	if(setIDs == true){
     		$('[data-image-id]').each(function(){
     			counter++;
     			$(this).attr('data-image-id',counter);
     		});
     	}
     	$(setClickAttr).on('click',function(){
     		updateGallery($(this));
     	});
     }
 });
</script>
<script>
$('#datepicker').datepicker();

$("#datepicker").on('changeDate', function (ev) {
	$(this).datepicker('hide');
});
</script>
 <script 
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwqHHpNcYmMCB4qNFNSuhG8qwwWrqQh7s&callback=initMap"
    async defer></script>
<script TYPE="text/javascript">

function initMap() {
	var map

	var lat = parseInt(<?php echo $row['lat']; ?>);
	var lng = parseInt(<?php echo $row['lng']; ?>);
	var myLatLng = new google.maps.LatLng(lat,lng);

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
        	center: myLatLng,
        	zoom: 10
        });

        // Create a marker and set its position.
        var marker = new google.maps.Marker({
        	map: map,
        	position: myLatLng,
        	name: '<?php echo $row['title']?>'
        });
    }
    </script>
   
    


