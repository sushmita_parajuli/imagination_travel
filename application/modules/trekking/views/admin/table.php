<div class="manage">
    <?php if (isset($permission['add'])) { ?>
    <input type="button" value="Add Trekking" id="create" onclick="location.href = '<?php echo base_url() ?>admin/trekking/create';"/>
    <?php } ?>
</div>
<div class="search-field">
    <?php echo form_input('title', '', 'class="form-control searchKeys" data-type="varchar" id="username" placeholder="Title"'); ?>
    <?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>
<div class="clearfix"></div>
<div><?php
if (($this->session->flashdata('operation'))) {
    echo $this->session->flashdata('operation');
}
?></div>

<div class="widget box" id="replaceTable"> 
    

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> Trekking </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content" id="replaceTable"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive tablesorter" id="tblData"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th>
                    <?php foreach ($columns as $column) { ?>
                    <th><?php
                    $this->lang->load('trekking','english');
                    echo $this->lang->line(implode(' ', explode('_', $column)));
                    ?></th>
                    <?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>
                <tr> 
                    <td class="checkbox-column"><?php
                    echo $sno;
                    $sno++;
                    ?></td> 
                    <?php
                    foreach ($columns as $column) {
                        if($column == 'description'){?>
                        <td> <?php echo word_limiter($row->description,20) ?></td>
                        <?php  }
                        else if ($column == 'attachment') {
                            ?>
                            <td><img src="<?php echo base_url(); ?>uploads/trekking/<?php echo $row->$column; ?>" style="height:50px;"/></td>
                            <?php } else { ?>
                            <td><?php echo ucfirst($row->$column); ?></td><?php
                        }
                    }
                    ?> 
                    <td class="edit">
                        <?php if (isset($permission['edit'])) { ?>                 
                        <a href="<?php echo base_url(); ?>admin/trekking/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a> <?php } ?>                  
                        <?php if (isset($permission['delete'])) { ?>&nbsp;/&nbsp; <a href="<?php echo base_url(); ?>admin/trekking/delete/<?php echo base64_encode($row->id); ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a><?php } ?>
                    </td>
                </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
        <div id="pagination">
            <div class="pagination-input">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="pagination-text">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>

</div>
<script>
$('div').on('click', '#pagination a', function (e) {
//        alert('here');
e.preventDefault();
var searchParams = {};
var theUrl = $(this).attr('href');
//    var csrf =$('[name = "csrf_test_name"]').val();
//    alert(theUrl);
$(".searchKeys").each(function () {
    searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
});
$.ajax({
    type: 'POST',
//        data: 'parameters=' + JSON.stringify(searchParams),
data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
url: theUrl,
success: function (data) {
//            alert(data);
$('#replaceTable').html(data);
}
});
return false;
});

$('thead').click(function () {
    $("#tblData").tablesorter();
});
$('#goto').on('change', function () {
    var page = $('#goto').val();
    var base_url = $('.total').attr('b');
    var simUrl = base_url + "/";
    var theUrl = simUrl.concat(page);
    var searchParams = {};
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    var no_of_pages = parseInt($('.total').attr('a'));
    if (page != 0 && page <= no_of_pages) {
        $.ajax({
            type: 'POST',
//            data: 'parameters= ' + JSON.stringify(searchParams),
data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
url: theUrl,
success: function (data) {
    $('#replaceTable').html(data);
}
});
    }
    else {
        alert('Enter a PAGE between 1 and ' + no_of_pages);
        $('#goto').val("").focus();
        return false;
    }
});
$('#search').click(function () {
    var searchParams = {};
    var base_url = $('.total').attr('b');
//    var test = $(JSON.stringify(searchParams)
//   var csrf1 = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)},
//    var csrf= $("'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'");
//    var csrf= $("input[name=csrf_test_name]").val();
//    alert(csrf);
//        var post_data = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)};
$(".searchKeys").each(function () {
    searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
});
$.ajax({
    type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
url: base_url,
success: function (data) {
    $('#replaceTable').html(data);
}
});
return false;
});
    //$("#downl o adFile").on('click', function() {
    //    downloadFile('test');
    ////   alert ("inside onclick");
    ////   window.location = "http://www.google.com";
    //});
</script>	
<script type="text/javascript">
function myKeyPress(e){
    var code= (e.keyCode ? e.keyCode : e.which);
    var searchParams = {};
    var base_url = $('.total').attr('b');
    if(code == 13) { //Enter keycode
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
     $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
url: base_url,
success: function (data) {
    $('#replaceTable').html(data);
}
});
     return false;
 }

}
</script>