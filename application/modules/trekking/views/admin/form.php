<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Trekking</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo form_open_multipart('admin/trekking/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Title</label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('title', $title, 'class="form-control"'); ?>
                    </div> 
                </div>

                <div class="form-group"> 
                    <label class="col-md-2 control-label">Description</label> 
                    <div class="col-md-10">
                        <?php echo form_textarea(array('id' => 'editor1', 'name' => 'description', 'value' => $description, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
                    </div> 
                </div>

                <div class="form-group"> 
                    <label class="col-md-2 control-label">Itinerary</label> 
                    <div class="col-md-10">
                        <?php echo form_textarea(array('id' => 'editor2', 'name' => 'itinerary', 'value' => $itinerary, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
                    </div> 
                </div>

                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta Description</label> 
                    <div class="col-md-10">
                        <?php echo form_textarea(array('id' => 'editor3', 'name' => 'meta_description', 'value' => $meta_description, 'rows' => '5', 'cols' => '20', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta Search Keys <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('search_keys', $search_keys, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Latitude</label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('latitude', $latitude, 'class="form-control"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Longitude</label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('longitude', $longitude, 'class="form-control"'); ?>
                    </div> 
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        if (!empty($update_id)) {

                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile',
                                'value' => $attachment
                            );
                        } else {
                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile',
                                'value' => $attachment,
                                'class' => 'required'
                            );
                        }
                        ?>

<?php echo form_upload($attach_prop); ?>
                        <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                        <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if (!empty($update_id)) { ?>
                                <img src="<?php echo base_url(); ?>uploads/trekking/<?php echo $attachment; ?>" style="height:100px;"/>
<?php } ?>
                        </label>
                    </div>

                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Show As Banner</label> 
                    <div class="col-md-10"> 
                        <?php
                        $selected = $option;
                        $options = array(
                            'no' => 'NO',
                            'yes' => 'YES',
                        );
                        echo form_dropdown('option', $options, $selected, 'class="form-control" id="option"');
                        ?>
                    </div> 
                </div> 

                <div class="form-group"> 
                    <label class="col-md-2 control-label">Status</label> 
                    <div class="col-md-10"> 
                        <?php
                        $selected = $status;
                        $options = array(
                            'draft' => 'draft',
                            'live' => 'live',
                        );
                        echo form_dropdown('status', $options, $selected, 'class="form-control"');
                        ?>
                    </div> 
                </div>  




                <div class="form-actions"> 
                    <?php
                    echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                    if (!empty($update_id)) {
                        echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

<?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>

<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
    CKEDITOR.replace('editor1', {
        filebrowserBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',

    });
    CKEDITOR.replace('editor2', {
        filebrowserBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',

    });
    CKEDITOR.replace('editor3', {
        filebrowserBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>assets/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',

    });
</script>