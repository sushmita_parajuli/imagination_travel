
<div class="clearfix"></div>
<div class="row">
    <?php  foreach ($detail as $row) {?>
    <div class="col-md-12">
        <div class="">
            <figure class="photo">
                <img class="img-responsive" src="<?php echo base_url()?>uploads/services/<?php echo $row['attachment'] ?>" alt="">
            </figure>
        </div>
    </div>
    <div class="col-md-8">
        <div class="trek-list">
            <h2><?php echo $row['title'] ?> in Nepal</h2>
            <p>
                <?php echo $row['description']; ?>
            </p>

            <?php }?>
        </div>
    </div>
    <div class="col-md-4 trek-package">   
       <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Treking Activities</h3>
        </div>
        <div class="panel-body">
            <ul class="activities-links">
                <?php foreach ($query as $rows) {?>
                
                <li><a href="<?php echo base_url()?>trekking/details/<?php echo $rows['slug']; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $rows['title']?></a></li>
                            <!-- <li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Poon Hill Treking</a></li>
                            <li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sagarmatha Treking</a></li>
                            <li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ghandruk Treking</a></li>
                        -->  
                        <?php }?>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
