<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sightseeing extends MX_Controller {
    private $MODULE_PATH = "sightseeing/index";
    private $group_id;
    private $MODULE = 'sightseeing';
    private $model_name = 'mdl_sightseeing';


    function __construct() {
        $this->load->library('session');
        parent::__construct();

        $this->load->model('mdl_sightseeing');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }

    function index()
    {
      $data['query'] = $this->get_location();  
      $data['view_file'] = "sightseeing";
      $this->load->module('template');
      $this->template->front($data);
  }

  function detail(){
    $slug=$this->uri->segment(3);
    $data['location']= $this->get_location_from_slug($slug);
    $data['detail']=$this->get_details_from_slug($slug);
    $data['details']=$data['detail'];
//        var_dump($data['detail']);die;
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);

}
function details_more(){
    $slug=$this->uri->segment(3);
    $data['detail']=$this->get_details_front($slug);
    $data['country']= $this->get_country();
//        var_dump($data['detail']);die;
    $data['view_file']="details_more";
    $this->load->module('template');
    $this->template->front($data);

}

function details()
{
    $slug = $this->uri->segment(3);
    $data['location']= $this->get_location_from_slug($slug);
    $data['details'] =  $this->get_details_from_slug($slug);
    // var_dump($data['details']);die;
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);
}
function get_details_from_slug($slug)
{
    $this->load->model('mdl_sightseeing');
    $query = $this->mdl_sightseeing->get_details_from_slug($slug);
    return $query->result_array();
}
function get_location_from_slug($slug)
{
    $this->load->model('location/mdl_location');
    $query = $this->mdl_location->get_location_from_slug($slug);
    return $query->result_array();
}

function get_details_front($slug)
{
    $this->load->model('mdl_sightseeing');
    $query = $this->mdl_sightseeing->get_details_front($slug);
    return $query->result_array();
}

function get_location(){
    $this->load->model('location/Mdl_location');
    $query = $this->Mdl_location->get_location();
    return $query->result_array();
}
function get_country(){
    $this->load->model('trekking/Mdl_trekking');
    $query = $this->Mdl_trekking->get_country_dropdown();
    return $query;
}
function submit() {
    $data['package'] = $package = $this->input->post('package', TRUE);
    $data['start'] = $start = $this->input->post('start', TRUE);
    $data['end'] = $end = $this->input->post('end', TRUE);
    $data['people'] = $people = $this->input->post('people', TRUE);
    $data['name'] = $name = $this->input->post('name', TRUE);
    $data['country'] = $country = $this->input->post('country', TRUE);
    $data['dob'] = $dob = $this->input->post('dob', TRUE);
    $data['citizen'] = $citizen = $this->input->post('citizen', TRUE);
    $data['email'] = $sender = $this->input->post('email', TRUE);
    $data['phone'] = $phone = $this->input->post('phone', TRUE);
    $message = "name:".$name ."<br>". PHP_EOL."phone:".$phone ."<br>". PHP_EOL."country:".$country ."<br>". PHP_EOL."dob:".$dob ."<br>". PHP_EOL ."citizen no:" .$citizen ."<br>". PHP_EOL."package:".$package ."<br>". PHP_EOL."start date:".$start ." "."end date:".$end." "."No of people:".$people;
    $status = $this->send_email($sender, $message, $name);
    if ($status == true) {
        echo true;
    } else {
        echo false;
    }
}
function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
//    $config = Array(
//        'protocol' => 'smtp',
//        'smtp_host' => 'ssl://smtp.googlemail.com',
//        'smtp_port' => 465,
//            'smtp_user' => 'bibek.munikar@gmail.com', // change it to yours
//            // 'smtp_pass' => 'sushie184617',  
//            // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
    $config['protocol'] = 'mail';
    $config['mailpath'] = '/usr/sbin/mail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('bibek.munikar@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Book Now Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}
