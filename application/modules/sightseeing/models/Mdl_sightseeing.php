<?php



if (!defined('BASEPATH'))

  exit('No direct script access allowed');



class Mdl_sightseeing extends Mdl_crud {



  protected $_table = "up_sightseeing";

  protected $_primary_key = 'id';



  function __construct() {

    parent::__construct();

  }



  function get_id() {
    $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
    $row = mysql_fetch_array($result);
    $nextId = $row['Auto_increment']; 
    return $nextId;

  }



  function get_sightseeing() {

    $table = $this->_table;

    $this->db->where('status', 'live');
    $this->db->order_by('id','DESC');
    $query = $this->db->get($table);

    return $query;

  }



  function get_sightseeing_dropdown() {

    $this->db->select('id, title');

    $this->db->order_by('title');

    $dropdowns = $this->db->get('sightseeing')->result();

    foreach ($dropdowns as $dropdown) {

      $dropdownlist[$dropdown->id] = $dropdown->title;

    }

    if (empty($dropdownlist)) {

      return NULL;

    }

    $finaldropdown = $dropdownlist;

    return $finaldropdown;

  }
  function get_details_from_slug($slug){
   $table= 'up_sightseeing';
   $this->db->select('*');
   $this->db->select('up_sightseeing.slug as slugs,up_sightseeing.title as name,up_sightseeing.attachment as image ,up_sightseeing.description as description'); 
   $this->db->join('up_location','up_location.id = up_sightseeing.location_id','left');
   $this->db->where('up_location.title',$slug);
   $query=$this->db->get($table);
       // var_dump($query->result());die;
   return $query;
 }

 function get_details_front($slug){
   $table= 'up_sightseeing';
   $this->db->select('*');
   $this->db->where('slug',$slug);
   $query=$this->db->get($table);
       // var_dump($query->result());die;
   return $query;
 }




}

