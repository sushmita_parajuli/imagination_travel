<div class="sightseeing">
        <div class="row">
            <h2 class="text-center">Sightseeing</h2>
            <?php echo $row['description']?>
            <?php foreach ($query as $row) {?>
            <div class="col-sm-4">
                <div class="box">
                    <a href="<?php echo base_url()?>sightseeing/details/<?php echo $row['slug']; ?>">
                        <figure>
                            <img src="<?php echo base_url() ?>uploads/location/<?php echo $row['attachment']?>" class="img-responsive"/>
                            <figcaption><?php echo $row['title']?></figcaption>
                        </figure>
                    </a>
                </div>
            </div>

            <?php  }?>

        </div>
</div>
