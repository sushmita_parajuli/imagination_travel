<div class="sightseeing">
    <h2 class="text-center"><?php echo ucfirst($this->uri->segment(3)); ?></h2>
    <?php foreach ($location as $location) { ?>
        <p> <?php echo $location['description'] ?> </p>
    <?php } ?>
    <div class="row">

        <h2 class="text-center"><?php echo ucfirst($this->uri->segment(3)); ?></h2>
        <?php foreach ($location as $location) { ?>
            <p> <?php echo $location['description'] ?> </p>
        <?php } ?>

        <?php foreach ($details as $row) {
            ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="<?php echo base_url() ?>uploads/sightseeing/<?php echo $row['image'] ?>" alt="...">
                    <div class="caption">
                        <h3><?php echo $row['name'] ?></h3>
                        <p><?php echo word_limiter($row['description'], 45) ?> </p>
                        <?php $new_title = $this->uri->segment(3); ?>
                        <p><a href="<?php echo base_url() ?>sightseeing/details_more/<?php echo $row['slugs']; ?>/<?php echo $new_title ?>" class="btn btn-success" role="button">Read More</a> 
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
