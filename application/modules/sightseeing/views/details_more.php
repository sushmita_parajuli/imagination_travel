<div class="content-wrap pad  clearfix">
	<!-- service -->
	<div class="clearfix">
		<div class="row">
			<div class="clearfix">
				
				<div class="col-sm-12">
					<?php foreach($detail as $row){?>


					<div class="trek-description clearfix">
						<figure class="photo">
							<img class="img-responsive" src="<?php echo base_url() ?>uploads/sightseeing/<?php echo $row['attachment']?>" alt="">
						</figure>
						<div class="title">
							<h2><?php echo $row['title']?> </h2>
						</div>
						<div class="cont">
							<p><?php echo $row['description']?></p>
						</div>	

					</div>
					<?php } ?>
				</div>
				<div id = "replaceDiv"> </div>

				<div class="book-form-wrap col-sm-10 col-sm-offset-1">
					<div class="book-form-holder">
						<div class="heading">
							<h2 class="text-center">Book Now</h2>
						</div>
						<div class="form-wrapper clearfix">
							<div class="holder">
								<?php echo form_open('', 'id="f1" class="book_now"'); ?>
								<div class="col-sm-4">
									<h4 class="col-sm-12">Package Info</h4>

									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="col-sm-12">
											<?php $package  = $this->uri->segment(3);
											$package_title = ucfirst(str_replace('-', ' ', $package));
											?>
											<input  id= "b_package" type="text" class=" form-control" name="package name" value = <?php echo $package_title  ?> placeholder="package name">
										</div>
									</div>
									
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Email</label> -->
										<div class="col-sm-12 datepicker">
											<input type="text" class="form-control input-group date" id="b_start" data-date="" data-date-format="yyyy-mm-dd" placeholder="Start Date">
											<!-- <input id="start_datepicker"  class="form-control" name="dob" placeholder="Start Date"> -->
										</div>
									</div>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="phone">Phone</label> -->
										<div class="col-sm-12 datepicker">
											<input id="b_end" type="text" class="form-control input-group date"  data-date="" data-date-format="yyyy-mm-dd"  placeholder="End Date">
										</div>
									</div>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="col-sm-12">
											<input id="b_people"type="text" class=" form-control" name="people" placeholder="Number of People">
										</div>
									</div>
								</div>
								<div class="col-sm-8">
									<h4 class="col-sm-12">Personal Info</h4>
									<div class="form-group clearfix">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="col-sm-12">
											<input id="b_name" type="text" class=" form-control" name="name" placeholder="Your Full Name">
										</div>
									</div>

									<div class="form-group col-sm-6">
										<?php 
										$selected = $country;
										$options = $country;
										echo form_dropdown('country', $options, $selected ,'id="b_country" class="form-control"');
										?>
									</div> 
									<div class="form-group col-sm-6">
										<!-- <label class="col-sm-2" for="name">Email</label> -->
										<div class="">
											<input id="b_dob" type="text" class="form-control input-group date" data-date="" data-date-format="yyyy-mm-dd" data-provide="datepicker3" placeholder="Your DOB">
										</div>

									</div>
									<div class="form-group col-sm-6">
										<!-- <label class="col-sm-2" for="name">Name</label> -->
										<div class="">
											<input id="b_citizen" type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
										</div>
									</div>
									<div class="form-group col-sm-6">
										<!-- <label class="col-sm-2" for="name">Email</label> -->
										<div class="">
											<input id="b_email" type="email" class="form-control" name="email" placeholder="Your Email Address">
										</div>

									</div>
									<div class="form-group col-sm-6">
										<!-- <label class="col-sm-2" for="phone">Phone</label> -->
										<div class="">
											<input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number">
										</div>
									</div>
									<div class="form-group col-sm-12">
										<?php echo form_checkbox('featuretype', 'is_featured', isset($post->featuretype) && $post->featuretype == 'is_featured' ? TRUE : FALSE, 'id="termsCheck" '); ?>
										I accept all terms and conditions.
									</div>
									<div class="btn-wrap col-sm-12">
										<a onclick="submit()" id="submit1" class="btn btn-success">Book Now
										</a>
									</div>
								</div>

								<?php echo form_close(); ?>
							</div>
							<div id="replaceDiv"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div> <!-- /.service end -->

<script type="text/javascript">
function submit() {

	var base_url = ' <?php echo base_url(); ?>';
	var b_package = $("#b_package").val();
	var b_start = $("#b_start").val();
	var b_end = $("#b_end").val();
	var b_people = $("#b_people").val();
	var b_citizen = $("#b_citizen").val();
	var b_name = $('#b_name').val();
	var b_country = $('#b_country').val();
	var b_dob = $('#b_dob').val();
	var b_email = $('#b_email').val();
	var b_phone = $('#b_phone').val();
	if (b_package != '' && b_start != '' && b_start != '' && b_end != '' && b_citizen != '' && b_name !='' &&
		b_country != '' && b_dob != '' && b_email != '' && b_phone != '' && b_people != '') {
		if($('#termsCheck').prop('checked')){
			$.ajax({
				type: 'POST',
				url: base_url + 'sightseeing/submit',
				data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', packages: b_package, start: b_start, end: b_end, citizen: b_citizen, name: b_name, country: b_country, dob:b_dob , email: b_email , phone: b_phone},
				success: function (data) {
					if (data == true) {
						$('#replaceDiv').html('<div  class="col-md-12" style="margin-top:-20px;">Thank you!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                	$('#replaceDiv').html('<div class="col-md-12" style="margin-top:-20px;">Sorry! Try again later.</div>');
                                                }

                                            }
                                        });
		} else{
			alert('Please accept our terms and conditions.')
		}
	} else {
		alert('Please fill up all fields');
	}
}
</script>
