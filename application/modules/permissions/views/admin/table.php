<?php
$error_msz = $this->session->flashdata('error');
if (!empty($error_msz)) {
    ?>
    <div class="alert alert-danger fade in">
        <i class="icon-remove close" data-dismiss="alert"></i> 
        <strong>Error!</strong> <?php echo $error_msz; ?>
    </div>		
<?php } ?>

<div class="search-field">
    <?php echo form_input('name', '', 'class="form-control searchKeys" data-type="varchar" id="title"  placeholder="name"'); ?>
    <?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>
<div class="widget box" id="replaceTable"> 
    <div><?php
        if (($this->session->flashdata('operation'))) {
            echo $this->session->flashdata('operation');
        }
        ?></div>


    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> User Permissions </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive tablesorter" id="tblData"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th>
                    <?php foreach ($columns as $column) { ?>
                        <th><?php
                            $this->lang->load('permissions','english');
                            echo $this->lang->line(implode(' ', explode('_', $column)));
                            ?></th>
                    <?php } ?>
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <?php
                        foreach ($columns as $column) {
                            if ($column == 'set_permission') {
                                ?><td><?php
                                        if ($row->id != '1') {
                                            echo '<a href="' . base_url() . 'admin/permissions/group/' . $row->id . '"><i class="icon-key"></i>  Set Permission</a>';
                                        } else {
                                            echo "The Admin group has access to everything";
                                        }
                                        ?></td><?php
                            } else {
                                ?>
                                <td><?php echo ucfirst($row->$column) ?></td>
                                <?php
                            }
                        }
                        ?>
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
        <div id="pagination">
            <div class="pagination-input">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="pagination-text">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>
</div><!--end of class="widget box"-->