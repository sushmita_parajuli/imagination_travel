<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_permissions extends Mdl_crud {

    protected $_table = "up_permissions";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_permissions() {
        $table = $this->_table;
        $query = $this->db->get($table);
        return $query;
    }

//    function get_where($name) {
//        $table = $this->_table;
//        $this->db->select($name);
//        $this->db->where('id', 1);
//        $query = $this->db->get($table);
//        return $query;
//    }

    /* function _update($id, $data){
      $table = $this->_table;
      $this->db->where('id', $id);
      $this->db->update($table, $data);
      } */

    function check_permission_existence($group_id) {
        $table = $this->_table;
        $this->db->select('group_id');
        $this->db->where('group_id', $group_id);
        $result = $this->db->get($table)->result();
        if (empty($result)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _insert_permissions($group_id, $data) {
        $array_string = serialize($data);
        //echo $array_string;die();
        $insertarray['group_id'] = $group_id;
        $insertarray['roles'] = $array_string;
        $table = $this->_table;
        $this->db->insert($table, $insertarray);
    }

    function _update_permissions($group_id, $data) {
        $array_string = serialize($data);
        $updateroles['roles'] = $array_string;

        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->update($table, $updateroles);
    }

    function unserialize_role_array($group_id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $query_results = $this->db->get($table)->result();
//        var_dump($query_results);die;
        if (empty($query_results)) {
            return NULL;
        }
        $result = $query_results[0]->roles;
        $roles_array = unserialize($result);
        return $roles_array;
    }

}
