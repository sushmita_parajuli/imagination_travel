<div class="row">
    <div class="col-md-8">
        <?php foreach ($query as $transport) {?>

        <div class="trek-description clearfix">
            <figure class="photo">
                <img class="img-responsive" src="<?php echo base_url() ?>uploads/ticketing/<?php echo $transport['attachment']?>" alt="">
            </figure>
            <div class="title">
                <h2><?php echo $transport['title']?> </h2>
            </div>
            <div class="cont">
                <p><?php echo $transport['description']?></p>
            </div>  

        </div>

        <?php }?>
    </div>

    <div class="col-md-4">
        <div class="trek-package">   
           <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Related Services</h3>
            </div>
            <div class="panel-body">
                <ul class="activities-links">
                   <?php foreach ($facility as $service) {
                    $name = str_replace('-', '_', $service['slug']);
                    ?> <li>
                    <a href="<?php echo base_url() ?><?php echo $name?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $service['title']; ?>
                    </a>
                </li>
                <?php   }?>
            </ul>
        </div>
    </div>

</div>

</div>
</div>t

<div class="book-form-wrap col-sm-10 col-sm-offset-1">
    <div class="book-form-holder">
        <div class="heading">
            <h2 class="text-center">Book Now</h2>
        </div>
        <div class="form-wrapper clearfix">
            <div class="holder">
                <?php echo form_open('', 'id="f1" class="book_now"'); ?>
                <h4 class="col-sm-12"> Info</h4>
                <div class="col-sm-6">
                    <div class="form-group">
                        <select class="form-control" id="b_airway">
                            <option>Airway List</option>
                            <option>Yeti Airlines</option>
                            <option>Nepal Airlines</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php $package  = $this->uri->segment(3);
                        $package_title = ucfirst(str_replace('-', ' ', $package));
                        ?>
                        <input  id= "b_package" type="text" class=" form-control" name="package name" value="<?php echo $package_title?>" placeholder="Destination name">
                    </div>
                </div>

                <div class="col-sm-6">
                   <div class="form-group">
                    <input id="b_people" type="text" class=" form-control" name="people" placeholder="Number of People">
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="form-group datepicker">
                    <input type="text" class="form-control input-group " id="b_start" data-date-format="yyyy-mm-dd"  placeholder="Arrival Date">

                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group datepicker">
                    <input id="b_end" type="text" class="form-control" name="date" placeholder="Departure Date">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <input id="b_name" type="text" class=" form-control" name="name" placeholder="Your Full Name">
                </div>
            </div>
            <div class= "col-md-6">
                <div class="form-group ">
                   <?php 
                   $selected = $country;
                   $options = $country;
                   echo form_dropdown('country', $options, $selected ,'id="b_country" class="form-control"');
                   ?>
               </div>
           </div> 
           <div class="form-group col-sm-6">
            <!-- <label class="col-sm-2" for="name">Name</label> -->
            <div class="">
                <input id="b_citizen" type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
            </div>
        </div>
        <div class="form-group col-sm-6">
            <!-- <label class="col-sm-2" for="name">Email</label> -->
            <div class="">
                <input id="b_email" type="email" class="form-control" name="email" placeholder="Your Email Address">
            </div>

        </div>
        <div class="form-group col-sm-6">
            <!-- <label class="col-sm-2" for="phone">Phone</label> -->
            <div class="">
                <input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number">
            </div>
        </div>
        <div class="col-sm-6" >
            <div class="form-group" id="b_way">
                <label class="radio-inline"><input type="radio" name="optradio">One Way </label>
                <label class="radio-inline"><input type="radio" name="optradio">Two Way</label>
            </div>
        </div>
            <!-- <div class="col-sm-12">
                <input type="checkbox" name="vehicle1" value="Bike"  id="termsCheck"> I agree all terms and condition<br>
            </div> -->
            <div class="btn-wrap col-sm-12">
                <a onclick="submit()" id="submit1" class="btn btn-success">Book Now
                </a>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div id="replaceDiv" >
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function submit() {

        var base_url = ' <?php echo base_url(); ?>';
        var b_airway = $("#b_airway").val();
        var b_package = $("#b_package").val();
        var b_start = $("#b_start").val();
        var b_end = $("#b_end").val();
        var b_people = $("#b_people").val();
        var b_citizen = $("#b_citizen").val();
        var b_name = $('#b_name').val();
        var b_country = $('#b_country').val();
        var b_email = $('#b_email').val();
        var b_phone = $('#b_phone').val();
        var b_way = $('#b_way').val();
        console.log(b_way);

        if (b_package != '' && b_start != '' && b_citizen != '' && b_name !='' &&
            b_country != '' && b_airway != '' && b_email != '' && b_phone != '' && b_people != '') {
            $.ajax({
                type: 'POST',
                url: base_url + 'ticketing/submit',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', packages: b_package, arrival_date: b_start, departure_date: b_end, citizen: b_citizen, name: b_name, country: b_country , email: b_email , phone: b_phone , airway: b_airway,people: b_people ,way: b_way},
                success: function (data) {
                    if (data == true) {
                        $('#replaceDiv').html('<div  class="col-md-12 alert alert-success" >Thank you for booking. We will reply soon!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="col-md-12 alert alert-danger" >Sorry! Try again. Please provide all the details. </div>');
                                                }

                                            }
                                        });
    } else {
        alert('Please fill up all fields');
    }
}
</script>



