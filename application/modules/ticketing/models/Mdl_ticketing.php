<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_ticketing extends Mdl_crud {



    protected $_table = "up_ticketing";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_ticketing() {

        $table = $this->_table;

        $this->db->where('status', 'live');
        $this->db->order_by('id','DESC');
        $query = $this->db->get($table);

        return $query;

    }



    function get_ticketing_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('ticketing')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->id] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }


     function get(){
        $table = $this->_table;
        $this->db->where('status','live');
        $this->db->order_by('id','ASC');
        $query=$this->db->get($table);
        return $query;
    }
    function get_details_from_slug($slug){
     $table= 'up_services';
     $this->db->where('slug',$slug);
     $query=$this->db->get($table);
     return $query;
 }



}

