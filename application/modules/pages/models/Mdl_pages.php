<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_pages extends Mdl_crud {

    protected $_table = "up_pages";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }
    
    function get_table() {
    $table = "up_pages";
    return $table;
    }
    
    function get_pages_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('up_pages')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

function get_about_us() {
        $table = 'up_pages';
        $this->db->where('status', 'live');
        $this->db->where('slug','welcome-to-imagination-trek');
        $query = $this->db->get($table);

        return $query;

    }

//
//    function get_with_limit($limit, $offset, $order_by) {
//    $table = $this->_table;
//    $this->db->limit($limit, $offset);
//    $this->db->order_by($order_by);
//    $query=$this->db->get($table);
//    return $query;
//    }
//
//    function get_where($id){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $query=$this->db->get($table);
//    return $query;
//    }

    function get_pages_if_live($col, $value) {
//        echo $col; echo "--"; echo $value; die;
        $table = $this->_table;
        $this->db->where($col, $value);
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }

//    function get_where_custom($col, $value) {
//    $table = $this->_table;
//    $this->db->where($col, $value);
//    $query=$this->db->get($table);
//    return $query;
//    }
//
//    function _insert($data){
//    $table = $this->_table;
//    $this->db->insert($table, $data);
//    }
//
//    function _update($id, $data){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $this->db->update($table, $data);
//    }
//
//    function _delete($id){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $this->db->delete($table);
//    }
//
//    function count_where($column, $value) {
//    $table = $this->_table;
//    $this->db->where($column, $value);
//    $query=$this->db->get($table);
//    $num_rows = $query->num_rows();
//    return $num_rows;
//    }
//
//    function count_all() {
//    $table = $this->_table;
//    $query=$this->db->get($table);
//    $num_rows = $query->num_rows();
//    return $num_rows;
//    }
//
//    function get_max() {
//    $table = $this->_table;
//    $this->db->select_max('id');
//    $query = $this->db->get($table);
//    $row=$query->row();
//    $id=$row->id;
//    return $id;
//    }
//
//    function _custom_query($mysql_query) {
//    $query = $this->db->query($mysql_query);
//    return $query;
//    }
    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_pages'");
        $row = mysql_fetch_array($result);
//        var_dump($row);die;
        $nextId = $row['Auto_increment'];
        return $nextId;
    }

    function get_saarc_page($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_pages');
        return $query->result_array();
    }

    function get_how_it_work($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_pages');
        return $query->result_array();
    }

//    function get_page_live() {
//        $table = $this->_table;
//        $this->db->where('status', 'live');
//        $query = $this->db->get($table);
//        return $query;
//    }

    
}
