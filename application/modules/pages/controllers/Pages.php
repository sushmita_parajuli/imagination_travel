<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mdl_pages');
//          $this->load->model('product/Mdl_product');
    }

    function index() {
//            die('hjgjhgj');
//            echo base_url(); die;
        $first_bit = $this->uri->segment(1);
        $second_bit = $this->uri->segment(2);
//die($first_bit);
        if ($second_bit == "") {
            //this must be a custom page from the CMS

            if ($first_bit == "") {
//                die($first_bit);
                $first_bit = "home";
                //$banner = $this->banner();
            }
        }
        
//        die(var_dump($data['product']));die;
        $query = $this->get_pages_if_live('slug', $first_bit);
//        var_dump($query->result());die('dfsf');
//        $data['replace']=$this->get_pages_if_live('slug', $first_bit);
        foreach ($query->result() as $row) {

            $data['title'] = $row->title;
            $data['slug'] = strtolower(url_title($data['title']));
            $data['attachment'] = $row->attachment;
            $data['description'] = $row->description;
            $data['meta_description'] = $row->meta_description;
            $data['option'] = $row->option;
            $data['search_keys'] = $row->search_keys;
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        $data['result'] = $query->result();
       
        if (count($query->result_array()) == 0) {
            
            $this->load->module('template');
            $this->template->errorpage();
        } else {
//            die('hjgjhgj');
              
            $data['click'] = 'yes';
            $first_bit=$this->uri->segment(1);
            
            $this->load->module('template');
//            $data['view_file'] = $this->load->view('front');
           // $data
//            var_dump($data);die;
            if($first_bit=='privacy-policy'){
            $data['view_file']='page_replace';
            }
            
            $this->template->front($data);
            
//                $this->load->view('page_replace',$data);
            
        }
    }

    function get_pages_if_live($col, $value) {
        $query = $this->mdl_pages->get_pages_if_live($col, $value);
        return $query;
    }
    

    function get_where_custom($col, $value) {
        $query = $this->mdl_pages->get_where_custom($col, $value);
        return $query;
    }

    function get_how_we_have_dream() {
        $query = $this->mdl_pages->get_how_we_have_dream();
        return $query;
    }

    function get_short_terms_goals() {
        $query = $this->mdl_pages->get_short_terms_goals();
        return $query;
    }

    function get_long_terms_goals() {
        $query = $this->mdl_pages->get_long_terms_goals();
        return $query;
    }

    function get_children() {
        $query = $this->mdl_pages->get_children();
        return $query;
    }

}
