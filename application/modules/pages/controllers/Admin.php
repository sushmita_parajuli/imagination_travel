<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/pages";
    private $MODULE = "pages";
    private $group_id;
    private $model_name = 'Mdl_pages';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('Mdl_pages');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('pages'); //module name is pages here	
    }

    function index() {
//        die();
        //table select parameters
        $main_table_params = 'id,title,description,attachment,status';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
//            var_dump($params);die;
        } else {
            $params = '';
        }
        $count = $this->Mdl_pages->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] = 5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_pages->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['columns'] = array('title', 'description', 'attachment', 'status');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
//            die('hh');
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['attachment'] = $this->input->post('attachment', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));
        $data['description'] = $this->input->post('description',FALSE);
        $data['option'] = $this->input->post('option', TRUE);
        $data['search_keys'] = $this->input->post('search_keys', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            $data['attachment'] = $attach['attachment'];
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['attachment'] = $this->input->post('userfile', TRUE);
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function create() {
//        var_dump('$expression');die;
//        $group_id = $this->session->userdata['group_id']; //to set the permession of user group

        $update_id = base64_decode($this->uri->segment(4));
//        var_dump($update_id);die;
        $select = 'title,attachment,description,status,meta_description,option,search_keys';
        $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
//        var_dump('$permission');die;
        if ($update_id != 1)/* this prevent home page from updating */ {
            
            $submit = $this->input->post('submit', TRUE);
            if ($submit == "Submit") {
//                var_dump('$submit');die;
                //person has submitted the form
                $data = $this->get_data_from_post();
            } else {

                if (is_numeric($update_id)) {
                    if ($permission['edit'] == FALSE) {
                        redirect('admin/dashboard');
                    } else
                        $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                } else {



                    if ($permission['add'] == FALSE) {
                        redirect('admin/dashboard');
                    }
                }
            }

            if (!isset($data)) {
                $data = $this->get_data_from_post();
            }

            $data['update_id'] = $update_id;
            $data['view_file'] = "admin/form";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            
            redirect('admin/pages');
        }
    }

   function delete() {
        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
//            var_dump('test');die;
            unset($delete_id);
            redirect('admin/pages');
        } else {
             $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_pages->_delete($delete_id);
            $url = 'pages';
            echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
            //  redirect('admin/publications');
        }
    }
	
    function view() {
        $update_id = base64_decode($this->uri->segment(4));
        $view_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $select = 'title,attachment,description,status,meta_description,option,search_keys';
        if ($group_id != 1) {
            if (is_numeric($view_id)) {
                if ($permission['view'] == FALSE) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                }
            } else {
                if ($permission['view'] == false) {
                    redirect('admin/dashboard');
                }
            }
        }
        $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
        $data['view_id'] = $view_id;
        $data['view_file'] = "admin/view";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
        $this->load->model('mdl_pages');
        $data = $this->get_data_from_post();

        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

            $attach = $this->get_attachment_from_db($update_id);
            if ($_FILES["userfile"]["error"] != 4) {
                $uploadattachment = $this->common_functions->do_upload($update_id, $this->MODULE);
                $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            }
//            $uploadattachment = $this->common_functions->do_upload($update_id,$this->MODULE);
//            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            if (empty($data['attachment'])) {
                $data['attachment'] = $attach['attachment'];
            }
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            if (isset($permission['edit'])) {
                $this->mdl_pages->_update($update_id, $data);
            }


            $this->session->set_flashdata('operation', 'Updated Successfully!!!');
        } else {
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);

            $nextid = $this->mdl_pages->get_max();
//          var_dump($_FILES);die('bjj');
            if ($_FILES["userfile"]["error"] != 4) {
                $uploadattachment = $this->common_functions->do_upload($nextid, $this->MODULE);
                $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            }

            if (isset($permission['add'])) {
                $this->mdl_pages->_insert($data);
            }


            $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
        }
//redirect('/admin/pages', 'location', 301);
        $url = 'pages';
        echo'
<script>
window.location.href = "' . base_url() . 'admin/' . $url . '";
</script>
';
    }

    function get_attachment_from_db($update_id) {
        $this->load->model('mdl_pages');
        $query = $this->Mdl_pages->get_where_dynamic($update_id);
        foreach ($query->result() as $row) {
            $data['attachment'] = $row->attachment;
        }
        return $data;
    }

    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_id_from_modulename($modulename) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }

}
