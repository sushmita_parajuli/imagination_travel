<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_gallery extends Mdl_crud {



    protected $_table = "up_gallery";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {

        // $nextId = $row['Auto_increment'];

        // $result = $this->_custom_query("SHOW TABLE STATUS LIKE 'up_gallery'");

        // $row = $result->result();

        // $nextId = $row[0]->Auto_increment;



        // return $nextId;
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_gallery($slug) {
        $slug_new= str_replace('-', ' ', $slug);
        // var_dump($slug_new);die;
        $table = 'up_gallery';
        $this->db->select('*');
        $this->db->where('up_gallery.status', 'live');
        $this->db->where('up_gallery.trekking_id',$slug_new);
        $this->db->order_by('up_gallery.id','DESC');
        $this->db->limit(1);
        $query = $this->db->get($table);

       return $query;

    }
//    function get_gallery(){
//        $query = "SELECT * FROM (SELECT `title`,`attachment`,`slug`,`option`,`status` FROM up_sightseeing UNION SELECT `title`,`attachment`,`slug`,`option`,`status` FROM up_trekking) a where a.option ='yes' && a.status='live'";
//        $var= $this->db->query($query);
//        return($var);
//    }



    function get_gallery_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('gallery')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->id] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }



}

