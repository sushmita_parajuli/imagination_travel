<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/gallery";
    private $MODULE = "gallery";
    private $group_id;
    private $model_name = 'Mdl_gallery';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('common_functions');
        $this->load->model('Mdl_gallery');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('gallery'); //module name is gallery here	
    }

    function index() {
        //table select parameters
        $main_table_params = 'id,title,title1,attachment0,attachment1,status';
        //search parameter
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
//           
        } else {
            $params = '';
        }
        $count = $this->Mdl_gallery->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

//        print_r($params);
//        $count = $this->Mdl_gallery->count();
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_gallery->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
//        $data['query'] = $this->Mdl_gallery->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);

        $data['columns'] = array('title', 'title1', 'attachment', 'status');

        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
//         $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['title'] = $this->input->post('title', TRUE);
        $data['trekking_id'] = $this->input->post('trekking_id',TRUE);
        $data['title1'] = $this->input->post('title1', TRUE);
        $data['title2'] = $this->input->post('title2', TRUE);
        $data['title3'] = $this->input->post('title3', TRUE);
        $data['title4'] = $this->input->post('title4', TRUE);
        $data['title5'] = $this->input->post('title5', TRUE);
        // $data['sub_title'] = $this->input->post('sub_title', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            // $data['attachment0'] = $attach['attachment0'];
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['attachment0'] = $this->input->post('userfile', TRUE);
            $data['attachment1'] = $this->input->post('userfile', TRUE);
            $data['attachment2'] = $this->input->post('userfile', TRUE);
            $data['attachment3'] = $this->input->post('userfile', TRUE);
            $data['attachment4'] = $this->input->post('userfile', TRUE);
            $data['attachment5'] = $this->input->post('userfile', TRUE);
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function get_attachment_from_db($update_id) {
        $query = $this->Mdl_gallery->get_where_dynamic($update_id);
        foreach ($query->result() as $row) {
            $data['attachment0'] = $row->attachment0;
            $data['attachment1'] = $row->attachment1;
            $data['attachment2'] = $row->attachment2;
            $data['attachment3'] = $row->attachment3;
            $data['attachment4'] = $row->attachment4;
            $data['attachment5'] = $row->attachment5;
        }
        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,attachment0,title1,attachment1,status,title2,title3,title4,title5,attachment2,attachment3,attachment4,attachment5';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }

        $data['update_id'] = $update_id;
        $data['trekking']= $this->get_trekking();

        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {

        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/gallery');
        } else {
            // $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_gallery->_delete($delete_id);
            $url = 'gallery';
            echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!' . $message);
            // redirect('admin/gallery');
        }
    }

    function submit() {
        //no validation in gallery because it has been created from jquery	
        $data = $this->get_data_from_post();
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

            $attach = $this->get_attachment_from_db($update_id);
            $uploadattachment = $this->common_functions->do_upload_multiple($update_id, $this->MODULE,6);
            for ($j = 0; $j < 6; $j++) {
                $data_attach['attachment' . $j ] = $uploadattachment[$j]['upload_data']['file_name'];

                $attachment = explode('.', $data_attach['attachment' . $j]);
                if (!isset($attachment[1])) {
                    /* for updating the image without uploading after edit having next id except english */
                    $data_attach['attachment' . $j ] = $attach[$value]['attachment' . $j ];
                }
            }
            
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            if (isset($permission['edit'])) {
                $this->Mdl_gallery->_update($update_id, $data);
            }


            $this->session->set_flashdata('operation', 'Updated Successfully!!!');
        } else {
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);

            $nextid = $this->Mdl_gallery->get_id();
            $uploadattachment = $this->common_functions->do_upload_multiple($nextid, $this->MODULE, 6);
//            var_dump($uploadattachment);die;
            for ($j = 0; $j < 6; $j++) {
                $attachments[$j] = $uploadattachment[$j]['upload_data']['file_name'];
                $data['attachment' . $j] = $attachments[$j];
            }
//            var_dump($data);die;
            if (isset($permission['add'])) {
                $this->Mdl_gallery->_insert($data);
            }
            $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
        }
        $url = 'gallery';
        echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
        //redirect('admin/gallery');
    }
    
    function get_trekking(){
        $this->load->model('trekking/Mdl_trekking');
        $query = $this->Mdl_trekking->get_trekking_dropdown();
//        var_dump($query);die;
        return $query;
    }

}
