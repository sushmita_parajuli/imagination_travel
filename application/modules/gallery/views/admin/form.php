<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Gallery </h4> 
            </div> 
            <div class="widget-content">
            	<?php
               echo form_open_multipart('admin/gallery/submit', 'class="form-horizontal row-border" id="validate-1"');				
               ?>                
               <div class="form-group"> 
                   <label class="col-md-2 control-label">Title</label> 
                   <div class="col-md-10"> 
                       <?php echo form_input('title', $title, 'class="form-control"');?>
                   </div> 
               </div>

                <div class="form-group"> 
        <label class="col-md-2 control-label">Trekking Packge<span class="required">*</span></label> 
        <div class="col-md-10"> 
          <?php $selected = $trekking_id;
          // var_dump($selected);
          $options = $trekking;
          echo form_dropdown('trekking_id', $options, $selected, 'class="form-control"');
          ?>
        </div> 
      </div> 



               <div class="form-group">
                   <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                   <div class="col-md-10"> 
                    <?php  if(!empty($update_id)){

                     $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment0
                        );
                 }else{
                     $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment0,
                        'class' => 'required'
                        );
                 }
                 ?>

                 <?php echo form_upload($attach_prop);?>
                 <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment0;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>

             

          <div class="form-group"> 
            <label class="col-md-2 control-label">Second Title</label> 
            <div class="col-md-10">
                <?php echo form_input('title1', $title1, 'class="form-control"');?>
            </div> 
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
            <div class="col-md-10"> 
                <?php  if(!empty($update_id)){

                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment1
                        );
                }else{
                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment1,
                        'class' => 'required'
                        );
                }
                ?>

                <?php echo form_upload($attach_prop);?>
                <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment1;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>
                <div class="form-group"> 
            <label class="col-md-2 control-label">Third Title</label> 
            <div class="col-md-10">
                <?php echo form_input('title2', $title2, 'class="form-control"');?>
            </div> 
        </div>
                
                <div class="form-group">
            <label class="col-md-2 control-label">Attachment  <span class="required">*</span></label> 
            <div class="col-md-10"> 
                <?php  if(!empty($update_id)){

                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment2
                        );
                }else{
                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment2,
                        'class' => 'required'
                        );
                }
                ?>

                <?php echo form_upload($attach_prop);?>
                <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment2;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>
                <div class="form-group"> 
            <label class="col-md-2 control-label">Forth Title</label> 
            <div class="col-md-10">
                <?php echo form_input('title3', $title3, 'class="form-control"');?>
            </div> 
        </div>
                <div class="form-group">
            <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
            <div class="col-md-10"> 
                <?php  if(!empty($update_id)){

                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment3
                        );
                }else{
                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment3,
                        'class' => 'required'
                        );
                }
                ?>

                <?php echo form_upload($attach_prop);?>
                <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment3;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>
                <div class="form-group"> 
            <label class="col-md-2 control-label">Fifth Title</label> 
            <div class="col-md-10">
                <?php echo form_input('title4', $title4, 'class="form-control"');?>
            </div> 
        </div>
                <div class="form-group">
            <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
            <div class="col-md-10"> 
                <?php  if(!empty($update_id)){

                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment4
                        );
                }else{
                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment4,
                        'class' => 'required'
                        );
                }
                ?>
                

                <?php echo form_upload($attach_prop);?>
                <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment4;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>
                <div class="form-group"> 
            <label class="col-md-2 control-label">Sixth Title</label> 
            <div class="col-md-10">
                <?php echo form_input('title5', $title5, 'class="form-control"');?>
            </div> 
        </div>
                <div class="form-group">
            <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
            <div class="col-md-10"> 
                <?php  if(!empty($update_id)){

                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment5
                        );
                }else{
                    $attach_prop = array(
                        'type' => 'file',
                        'name' => 'userfile[]',
                        'value' => $attachment5,
                        'class' => 'required'
                        );
                }
                ?>

                <?php echo form_upload($attach_prop);?>
                <p class="help-block">
                    Images only (jpg/jpeg/gif/png)</p>
                    <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                        <?php if(!empty($update_id)){?>
                        <img src="<?php echo base_url();?>uploads/gallery/<?php echo $attachment5;?>" style="height:100px;"/>
                        <?php }?>
                    </label>
                </div>

            </div>
                
                 <div class="form-group"> 
               <label class="col-md-2 control-label">Status</label> 
               <div class="col-md-10"> 
                <?php $selected = $status;
                
                $options = array(
                  'draft'  => 'draft',
                  'live'    => 'live',
                  );                            
                  echo form_dropdown('status', $options, $selected,'class="form-control"');?>
              </div> 
          </div>

            <div class="form-actions"> 
              <?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
                          ?>
                      </div>                 

                      <?php echo form_close(); ?>                
                  </div> 
              </div> 
          </div>
      </div>