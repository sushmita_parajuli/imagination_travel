<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('users/Mdl_users');
        $this->load->library('Common_functions');
        //always check if session userdata value "logged_in" is not true
        if (!$this->session->userdata("logged_in"))/* every logged_in user get privilage to access dashboard */ {
            redirect('admin');
        }
    }

    function index() {//this is admin/dashboard(check config/routes.php to be clear
        $data['permission'] = $this->common_functions->check_only_permission($this->group_id);
//                var_dump($data['module']);	die;
        $data['view_file'] = "admin/home";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function get_data_from_post() {
        $data['username'] = $this->input->post('username', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['display_name'] = $this->input->post('display_name', TRUE);
        $data['group_id'] = $this->input->post('group_id', TRUE);
        $data['country'] = $this->input->post('country', TRUE);

        $update_id = $this->input->post('update_id', TRUE);
        if (!is_numeric($update_id)) {//
            $data['password'] = $this->input->post('password', TRUE);
            //$pwd = $this->zenareta_pasaoro($pwd);
            $data['created_on'] = date("Y-m-d");
            $data['activation_code'] = NULL;
            $data['last_login'] = NULL;
        }

        return $data;
    }

    function chpwd() {
//        die('here');
        $update_id = ($this->uri->segment(4));
//        var_dump($update_id);die;
        if (is_numeric($update_id)) {
//             $data['old_password']= $this->input->post('old_password',TRUE);
//            die('hello');
            $chpwd_submit = $this->input->post('submit', TRUE);

//          var_dump($chpwd_submit);die('here');
            if ($chpwd_submit == "Submit") {
                $chpwd = $this->get_chpwd_from_post();
            }

            if (!isset($chpwd)) {
                $chpwd = $this->get_data_from_post();
            }

            $chdata['update_id'] = $update_id;
            $chdata['password'] = $chpwd;
            $chdata['old_password'] = $this->input->post('old_password');

//            var_dump($chdata['password']);die;
//            $this->load->view('chpwd',$chdata);
            $chdata['view_file'] = "admin/chpwd";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($chdata);
        } else {
            redirect('admin/users');
        }
    }

    public function is_valid_password() {
        $new_password = $_POST['new_password'];
//        print_r($new_password);die;
//        if (preg_match_all('$S*(?=S{8,})(?=S*[a-z])(?=S*[A-Z])(?=S*[d])(?=S*[W])S*$', $new_password)) {
        if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/', $new_password)) {
            echo'1';
        } else {
            echo'0';
        }
    }

    function get_chpwd_from_post() {
        $chpwd = $this->input->post('chpwd', TRUE);
        return $chpwd;
    }

    function check_old_password() {

        $id = $this->session->userdata('user_id');

        $pd = $_POST['old_password'];


        $this->load->model('users/mdl_users');
        // die('-----');
        $password = $this->Mdl_users->get_old_password_where($id);
//        var_dump($password);
        $hashed_pd = $this->Mdl_users->enc_hash_pwd($pd);
//        var_dump($hashed_pd);die;
        if ($password == $hashed_pd) {

            echo TRUE;
        } else {
            echo FALSE;
        }
    }

    function chpwd_submit() {



        $old_password = $this->input->post('old_password');
//            var_dump($old_password);die;
        $new_password = $this->input->post('chpwd');
//            var_dump($new_password);die;
        $id = $this->session->userdata('user_id');
        $update_id = $this->input->post('update_id', TRUE);


        $chdata['password'] = $this->zenareta_pasaoro($new_password);
//            var_dump($chdata['password']);die;
        $this->chpwd_update($update_id, $chdata);
//            $this->session->unset_userdata('logged_in');
        redirect('admin');

//        }
    }

    function chpwd_update($id, $chpwd) {
        $this->load->model('Mdl_users');
        $this->Mdl_users->chpwd_update($id, $chpwd);
    }

    function zenareta_pasaoro($pasaoro) {
        $this->load->model('admin_login/mdl_admin_login');
        $query = $this->mdl_admin_login->enc_hash_pwd($pasaoro);
        return $query;
    }

    public function is_valid_password_front() {
        $new_password = $_POST['new_password'];
        print_r($new_password);
        die;
//        if (preg_match_all('$S*(?=S{8,})(?=S*[a-z])(?=S*[A-Z])(?=S*[d])(?=S*[W])S*$', $new_password)) {
        if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/', $new_password)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
