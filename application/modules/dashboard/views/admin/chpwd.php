<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Add Users</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/dashboard/chpwd_submit', 'class="form-horizontal row-border" id="validate-1"');
                ?> 
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Old Password <span class="required">*</span></label> 
                    <div class="col-md-5"> 
                        <?php echo form_password('old_password', $old_password, 'class="form-control " onblur="checkOldPassword()" id="old_password" placeholder="Enter password",required'); ?>
                   
                    </div> 
                    <div class="col-md-5" id="matched"> 

                    </div> 
                </div> 
                <div class="form-group"> 
                    <label class="col-md-2 control-label">New Password <span class="required">*</span></label> 
                    <div class="col-md-5"> 
                        <?php echo form_password('chpwd', '', 'class="form-control required" onblur="checknewPassword()" id= "new_password"'); ?>
                   (Minimum 1 uppercase alphabet,one lowercase alphabet and one number required)
                    </div> 
                    <div class="col-md-5" id="correct"> 

                    </div>
                </div>  
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Confirm Password <span class="required">*</span></label> 
                    <div class="col-md-5"> 
                        <?php echo form_password('confirm_password', '', 'class="form-control required" onblur="confirmPassword()" id= "confirm_password" '); ?>
                    </div> 
                    <div class="col-md-5" id="confirm"> 

                    </div>
                </div> 

                <div class="form-actions"> 
                    <?php
                    echo form_button('button', 'Submit', 'class="btn btn-primary pull-right" id="submit_button" '); //name,value...type is default submit 
                    if (!empty($update_id)) {
                        echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
<script>
    function checkOldPassword() {

        var old_password = $("#old_password").val();



        var url = "<?php echo base_url() ?>admin/dashboard/check_old_password";
        $.ajax({
            type: 'POST',
            data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "old_password": old_password},
            url: url,
            success: function (result) {
                if (result) {

                    $("#matched").html("Correct Old password")
                    $("#matched").css("color", "green")
                } else {

                    $("#matched").html("Incorrect Old password")
                    $("#matched").css("color", "red")
                     $("#old_password").val('');
                      $("#new_password").val('');
                     

                }
            }
        });
        return false;
    }
</script>
<script>
    function checknewPassword() {

        var new_password = $("#new_password").val();
//        alert(new_password);
        var password = /^.*(?=.{8,20})(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z0-9!@#$%]+$/;
//        var result= new_password.match(password);
//        alert(password);
        if (new_password != '') {
            if (new_password.match(password)) {
                $("#correct").html("Strong Password");
                $("#correct").css("color", "green");
            } else {
                $("#correct").html("Incorrect password format")
                $("#correct").css("color", "red")
                $("#new_password").val('');
                 
                
            }
        }
//        $("#correct").html("Required")
//        $("#correct").css("color", "red")



//        var url = "<?php echo base_url() ?>admin/dashboard/is_valid_password";
//        $.ajax({
//            type: 'POST',
//            data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "new_password": new_password},
//            url: url,
//            success: function (result) {
//                if (result == '1') {
//
//                    $("#correct").html("Strong Password")
//                    $("#correct").css("color", "green")
//                } else {
//                    $("#correct").html("Incorrect password format")
//                    $("#correct").css("color", "red")
//                }
//            }
//        });
//        return false;
    }
    function confirmPassword() {
        var confirm_password = $("#confirm_password").val();
        var new_password = $("#new_password").val();
        if (confirm_password != '') {
            if (confirm_password === new_password)
            {
                $("#confirm").html("Password Matched")
                $("#confirm").css("color", "green")
            } else {
                $("#confirm").html("Password Didnot Matched")
                $("#confirm").css("color", "red")
                 $("#confirm_password").val('');
                
            }
        }
        
    }
</script>
<script>

    $('#submit_button').click(function ()
    {
        var confirm_password = $("#confirm_password").val();
//        alert(confirm_password);
        var new_password = $("#new_password").val();
        var password = /^.*(?=.{8,20})(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z0-9!@#$%]+$/;
        var form = document.getElementById("validate-1");
        console.log(new_password);
        console.log(confirm_password);
        var old_password = $("#old_password").val();
        var url = "<?php echo base_url() ?>admin/dashboard/check_old_password";
        $.ajax({
            type: 'POST',
            data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "old_password": old_password},
            url: url,
            success: function (result) {
                if (result) {
                    if (new_password.match(password) && confirm_password.match(password) && new_password === confirm_password) {

                        console.log(new_password);
                        console.log(confirm_password);
                        form.submit();
                    } else {
                        $("#confirm").html("Password Didnot Matched")
                        $("#confirm").css("color", "red")
                        form.reset();
                        $("#confirm").html('');
                    }
                } else {
                    $("#matched").html("Incorrect Old password")
                    $("#matched").css("color", "red")
                    $("#matched").html('');
                     $("#confirm").html('');
                      $("#correct").html('');
                     $("#old_password").html('');
                    form.reset();
                    
                }
               
            }
        });
    });

</script>