
<?php $group_id = $this->session->userdata('group_id');
// var_dump($group_id);die('here');
?>

<div class="bs-docs-section">
    <div class="bs-glyphicons">
        <ul class="bs-glyphicons-list">
            <div class="clearfix"></div>

            <?php if ($group_id == 1){ ?>
            <?php if((in_array("pages", $sidebar))==1){?>
            <div class="row">
                <legend class="legendStyle">
                 <a data-toggle="collapse" data-target="#demo" href="#">Manage Pages</a>
                </legend>
             
                <a href="<?php echo base_url();?>admin/contact_us">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Contact Us</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/pages">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Page</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/testimonial">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Testimonial</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/register">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Newsletter</span>
                    </li>
                </a>
            </div>
            <div class="row" style="margin-top: 10px;">

                <legend class="legendStyle">
                    <a data-toggle="collapse" data-target="#demo" href="#">Manage Packages</a>
                </legend>
                <a href="<?php echo base_url();?>admin/services">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Services</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/hotel_booking">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Hotel Booking</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/mountain_flight">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Mountain Flight</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/ticketing">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Ticketing</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/transport">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Transport</span>
                    </li>
                </a>
            </div>

            
             <div class="row" style="margin-top: 10px;">

                <legend class="legendStyle">
                    <a data-toggle="collapse" data-target="#demo" href="#">Manage Trekking</a>
                </legend>

                <a href="<?php echo base_url();?>admin/trekking">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Trekking</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/gallery">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Gallery - Trekking</span>
                    </li>
                </a>

            </div>
            <div class="row" style="margin-top: 10px;">

                <legend class="legendStyle">
                    <a data-toggle="collapse" data-target="#demo" href="#">Manage Sightseeing</a>
                </legend>

                <a href="<?php echo base_url();?>admin/sightseeing">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Sightseeing</span>
                    </li>
                </a>
                <a href="<?php echo base_url();?>admin/location">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Location for Sight Seeing</span>
                    </li>
                </a>

            </div>


        <?php }?>
    </ul>
    <?php } else{?>
    <?php foreach ($permission as $row){?>
    <a href="<?php echo base_url();?>admin/<?php echo strtolower($row) ?>">
        <li>
            <span class="glyphicon glyphicon-plus"> <?php echo $row?></span>
            <!--<span class="glyphicon-class"></span>-->
        </li>
    </a>
    
    <?php }?> 
    <?php }?>



</div>
</div>