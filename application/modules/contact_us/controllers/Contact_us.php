<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_us extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mdl_contact_us');
    }

    function index() {
        $data['query']= $this->get_contact_us();
        $data['view_file'] = "contact_us";
        $this->load->module('template');
        $this->template->front($data);
    }

    function submit() {
        $data['email'] = $sender = $this->input->post('email', TRUE);
        $data['name'] = $name = $this->input->post('name', TRUE);
        $data['contact_no'] = $contact_no = $this->input->post('contact_no', TRUE);
        $data['service_type'] = $service_type = $this->input->post('service_type', TRUE);
        $data['message'] = $message = $this->input->post('message', TRUE);
        $status = $this->send_email($sender, $message, $name);
        if ($status == true) {
            echo true;
        } else {
            echo false;
        }
    }
    function submit_front() {
        $data['email'] = $sender = $this->input->post('email', TRUE);
        $data['name'] = $name = $this->input->post('name', TRUE);
        $data['message'] = $message = $this->input->post('message', TRUE);
        $status = $this->send_email($sender, $message, $name);
        if ($status == true) {
            $this->Mdl_contact_us->_insert($data);
            echo true;
        } else {
            echo false;
        }
    }

    function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            // 'smtp_user' => 'sushmi2468@gmail.com', 
            // change it to yours
            // 'smtp_pass' => 'sushie184617',
             // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
//    $config['protocol'] = 'mail';
//    $config['mailpath'] = '/usr/sbin/mail';
//    $config['charset'] = 'iso-8859-1';
//    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('sushmi2468@gmail.com, bibek.munikar@gmail.com, raaz.pjt@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Imagination Travel Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    function _insert($data) {
        $this->load->model('Mdl_contact_us');
        $this->Mdl_contact_us->_insert($data);
    }

    function get_contact_us(){
        $this->load->model('contact_us/Mdl_contact_us');
        $query = $this->Mdl_contact_us->get_contact_us();
        return $query->result_array();
    }
}
