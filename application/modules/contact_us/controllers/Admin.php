<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/contact_us";
    private $group_id;
    private $MODULE = 'contact_us';
    private $model_name = 'Mdl_contact_us';

    public function __construct() {
        parent::__construct();
        $this->load->model('Mdl_contact_us');
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('modules/Mdl_moduleslist');
        $this->load->module('admin_login/admin_login');
        $this->load->model('settings/Mdl_settings');
        $this->load->library('Common_functions');
        $this->load->library('pagination');
        $this->load->library('up_pagination');
        $this->admin_login->check_session_and_permission('contact_us'); //module name is groups here	
    }

    function index() {
        $main_table_params = 'id,address,email,contact_no,status,email1';

        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
//           
        } else {
            $params = '';
        }
        $count = $this->Mdl_contact_us->count($params);
        // print_r($count);die;
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_contact_us->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['columns'] = array('address', 'email', 'contact_no');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['theUrl'] = $module_url;
        $data['per_page'] = $config['per_page'];
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['address'] = $this->input->post('address', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['email1'] = $this->input->post('email1', TRUE);
        $data['contact_no'] = $this->input->post('contact_no', TRUE);
        // $data['slug'] = strtolower(url_title($data['title']));
        $data['status'] = $this->input->post('status', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
//            $attach = $this->get_attachment_from_db($update_id);
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function get_data_from_db($update_id) {
        $query = $this->Mdl_contact_us->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['email'] = $row->email;
            $data['email1'] = $row->email1;
            $data['address'] = $row->address;
            $data['contact_no'] = $row->contact_no;
            $data['status'] = $row->status;
            $data['ent_date'] = $row->ent_date;
        }

        if (!isset($data)) {
            $data = "";
        }

        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'email,contact_no,address,ent_date,status,email1';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }


        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
        $group_id = $this->uri->segment(3);
        $data = $this->get_data_from_post();
        $update_id = $this->input->post('update_id',TRUE);
        if (is_numeric($update_id)) {
            
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
              if (isset($permission['edit'])) {
                $this->Mdl_contact_us->_update($update_id, $data);
            }
        } else {
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            if (isset($permission['add'])) {
                $this->Mdl_contact_us->_insert($data);
            }
        }

        redirect('admin/contact_us');
    }

    function delete() {
        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/contact_us');
        } else {
            $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_contact_us->_delete($delete_id);
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!' . $message);
            redirect('admin/contact_us');
        }
    }
}
