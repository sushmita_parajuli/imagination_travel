<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title> Contact Us </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>design/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>design/frontend/scss/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        

        <!-- </div> -->
        <!-- <img src="img/contact.jpg" class="img-responsive"/>  -->
        <div class="landing-block bg5">
            <div class="container" style="margin-top:6em;">
                <div class="col-xs-12 col-sm-5 col-sm-offset-7">
                    <h3 class="title text-center">Contact Us</h3>
                    <div id = "replaceDiv"> </div>
                    <!--<form class="contact-us">-->
                    <?php echo form_open('', 'id="f1" class="contact-us"'); ?>
                    <div class="row">	
                        <div class="form-group col-md-6">
                            <input id="c_name" type="text" name="Name" class="form-control input-lg" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="c_email" type="text" class="form-control input-lg" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="c_contact"type="text" class="form-control input-lg" placeholder="Contact No">
                        </div>
                        <div class="col-md-12">
                            <textarea id="c_message" class="form-control" placeholder="Message"></textarea>
                        </div>
                        <div class="col-xs-12">
                            <a onclick="submit()" id="submit1">
                                <div id="c_submit" class="button" type="submit">
                                    <p class="btnText">Submit</p>
                                    <div class="btnTwo">
                                        <p class="btnText2"><i class="fa fa-send" aria-hidden="true"></i></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <footer>
           
        </footer>
        <script type="text/javascript" src="<?php echo base_url() ?>design/frontend/js/main.js"></script>	

        <script type="text/javascript">
                                function submit() {
                                    var base_url = ' <?php echo base_url(); ?>';
                                    var c_name = $("#c_name").val();
                                    var c_email = $("#c_email").val();
                                    var c_message = $('#c_message').val();
                                    var c_contact_no = $('#c_contact').val();
                                    var c_service_type = $('#c_service').val();
                                    if (c_name != '' && c_email != '' && c_message != '' && c_service_type != '' && c_contact_no != '') {
                                        $.ajax({
                                            type: 'POST',
                                            url: base_url + 'contact_us/submit',
                                            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', name: c_name, contact_no: c_contact_no, service_type: c_service_type, email: c_email, message: c_message},
                                            success: function (data) {
                                                if (data == true) {
                                                    $('#replaceDiv').html('<div  class="col-md-12" style="margin-top:-20px;">Thank you!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="col-md-12" style="margin-top:-20px;">Sorry! Try again later.</div>');
                                                }

                                            }
                                        });
                                    } else {
                                        alert('Please fill up all fields');
                                    }
                                }


        </script>
    </body>
</html>
