<div class="clearfix"></div>
<div class="content-wrap pad clearfix">
    <div class="contact-wrap">
        <!-- contact details -->
        <div class="contact-holder clearfix">
            <div class="row">
                <div class="col-sm-7 map-wrap">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d226387.73274878692!2d85.19057931456064!3d27.552940028517398!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb143cc669c123%3A0x40d854bf34db9c39!2sLalitpur!5e0!3m2!1sen!2snp!4v1509719813086" width="100%" height="570" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-5 contact-details">
                    <div class="details-wrap clearfix">
                        <h3>Contact Detail</h3>

                        <?php foreach ($query as $row) {?>
                        <address class="address">
                            <span><i class="fa fa-map-marker">&nbsp;</i> <?php echo $row['address'];?></span>
                            <span><i class="fa fa-phone">&nbsp;</i> <?php echo $row['contact_no'];?></span>
                            <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank"><?php echo $row['email'];?></a></span>
                            <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank"><?php echo $row['email1'];?></a></span>
                        </address>
                        <?php  }?>

                    </div>
                    <div id="replaceDiv"></div>
                    <div class="clearfix"></div>
                    <div class="contact-form-wrap">
                        <div class="contact-form clearfix">
                            <p>Fill the form below to contact with us.</p>
                            <?php echo form_open('', 'id="f1" class="contact-us"'); ?>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="c_name" placeholder="Your Full Name">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" id="c_contact" placeholder="Your Phone Number">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="c_email" placeholder="Your email address">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="message">Message</label>
                                    <textarea class="form-control" id="c_message" cols="30" rows="3"></textarea>
                                </div>
                                
                                <div class="btn-wrap col-sm-12">
                                            <a onclick="submit()" id="submit1" class="btn btn-success">Submit
                                            </a>
                                        </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div> <!-- /.contact-details -->

            </div>
        </div>
    </div>
</div> <!-- /.content-wrap end -->


<script type="text/javascript">
function submit() {
    var base_url = ' <?php echo base_url(); ?>';
    var c_name = $("#c_name").val();
    var c_email = $("#c_email").val();
    var c_message = $('#c_message').val();
    var c_contact_no = $('#c_contact').val();
    var c_service_type = $('#c_service').val();
    if (c_name != '' && c_email != '' && c_message != '' && c_service_type != '' && c_contact_no != '') {
        $.ajax({
            type: 'POST',
            url: base_url + 'contact_us/submit',
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', name: c_name, contact_no: c_contact_no, service_type: c_service_type, email: c_email, message: c_message},
            success: function (data) {
                if (data == true) {
                    $('#replaceDiv').html('<div  class="alert alert-success col-md-12" style="margin-bottom:10px;">Thank you!! Your message has been sent.</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="alert alert-danger col-md-12" style="margin-bottom:10px;">Sorry! Try again later.</div>');
                                                }

                                            }
                                        });
} else {
    alert('Please fill up all fields');
}
}


</script>
