<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_contact_us extends Mdl_crud{
     protected $_table = "up_contact_us";
    protected $_primary_key = 'id';
    

    function __construct() {
        parent::__construct();
    }

    function get_id() {
        $result = $this->db->query("SHOW TABLE STATUS LIKE 'up_sample'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment'];
        return $nextId;
    }


    function get_contact_us() {
        $table = "up_contact_us";
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }

}
