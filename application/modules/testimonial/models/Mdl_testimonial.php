<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_testimonial extends Mdl_crud {



    protected $_table = "up_testimonial";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_testimonial() {
        $table = $this->_table;
        $this->db->where('status', 'live');
        $this->db->order_by('id','DESC');
        $query = $this->db->get($table);
        return $query;

    }



    function get_testimonial_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('testimonial')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->id] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }




}

