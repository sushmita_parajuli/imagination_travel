<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonial extends MX_Controller {
    private $MODULE_PATH = "testimonial/index";
    private $group_id;
    private $MODULE = 'testimonial';
    private $model_name = 'mdl_testimonial';


    function __construct() {
        $this->load->library('session');
        parent::__construct();

        $this->load->model('mdl_testimonial');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }

    function index()
    {
        $main_table_params = 'id,title,address,description,attachment,slug,status';
        if ($this->input->post('parameters')) { 
            $params = json_decode($this->input->post('parameters'));
//            var_dump($params);die;

        } else {
            $params = '';
        }
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $count = $this->mdl_testimonial->count($params); 
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
//                var_dump($config);die;
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = 10;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
//        $data['query']=$this->Mdl_staff_member->get_staff_member($slug,$language_id);
        $data['query'] = $this->mdl_testimonial->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'],$params,'');
//       var_dump($data['query']->result());die('hello');
//         $data['columns'] = array('name', 'published_date', 'group');
        $data['page'] = $page;
        $data['total'] = $count;
//        var_dump($data['total']);die;
        $data['total_page'] = ceil($count / $config['per_page']);
//        var_dump($data['total_page']);die;
        $data['per_page'] = $config['per_page'];
//        var_dump($data['per_page']);die;
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
//        var_dump($data);die;
        if ($this->uri->segment(3) == ''&& ($params == '')) {
            $data['view_file'] = "testimonial";
            $this->load->module('template');
            $this->template->front($data);
        } else {
//            die('here');
            $this->load->view('new_testimonial', $data);

        }       
    }

    function detail(){
        $slug=$this->uri->segment(3);
        $data['detail']=$this->get_details_from_slug($slug);
        $data['details']=$data['detail'];
//        var_dump($data['detail']);die;
        $data['view_file']="details";
        $this->load->module('template');
        $this->template->front($data);
        
    }
    function detail_front(){
        $slug=$this->uri->segment(3);
        $data['detail']=$this->get_details_front($slug);
        $data['details'] =  $data['detail'];
//        var_dump($data['detail']);die;
        $data['view_file']="details_front";
        $this->load->module('template');
        $this->template->front($data);
        
    }

    function getParam($param) {
        $this->load->model('mdl_testimonial');
        $query = $this->mdl_testimonial->getParam($param);
        return $query->result_array();
    } 
    function get_where($id){

       $query = $this->mdl_testimonial->get_where($id);
       return $query;
   }
   function get_where_1($year,$month){

       $query = $this->mdl_testimonial->get_where_1($year,$month);
       return $query;
   }
   function get($order_by){
       $this->load->model('mdl_testimonial');
       $query = $this->mdl_testimonial->get($order_by);
       return $query->result_array();
   }
   function details()
   {

    $slug = $this->uri->segment(3);
    $data['details'] =  $this->get_details_from_slug($slug);
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);
}
function get_details_from_slug($slug)
{
    $this->load->model('mdl_testimonial');
    $query = $this->mdl_testimonial->get_details_from_slug($slug);
    return $query->result_array();
}
function get_details_front($slug)
{
    $this->load->model('mdl_testimonial');
    $query = $this->mdl_testimonial->get_details_front($slug);
    return $query->result_array();
}

function get_testimonial_archive(){
	$this->load->model('mdl_testimonial');
	$query = $this->mdl_testimonial->get_testimonial_archive();
	return $query;
}
function get_list_archive(){
	$this->load->model('mdl_testimonial');
	$query = $this->mdl_testimonial->get_list_archive();
	return $query;
}
function testimonial_front(){
 if ($this->input->post('parameters')) { 
    $params = json_decode($this->input->post('parameters'));
//            var_dump($params);die;

} else {
    $params = '';
//            var_dump($params);die;
}
if ($this->input->post('order_by')) {
    $order_by = $this->input->post('order_by');
} else {
    $order_by = 'id';
}

//        var_dump($count);die;
$page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
$config['per_page'] = 20;

//      
$count = count($this->mdl_testimonial->count_testimonial_front($params));
//        /var_dump($count);die;

$module_url = base_url() . 'testimonial/testimonial_front';

$config = $this->up_pagination->set_pagination_config_front($count, $module_url);
//          var_dump($config['per_page']);die;

$entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');

//            var_dump($page);die;

$data['page'] = $page;
$start= (($page - 1) * $config['per_page']);
//           var_dump($start);die;
$limit= 10;
//         var_dump($limit);die;
//         var_dump($limit);die;

$this->pagination->initialize($config);  
$data['query'] = $this->mdl_testimonial->get_testimonial_front($params,($page - 1) * $config['per_page'],$limit);
//        $data['query']= $this->Mdl_downloads->get_document_all($params,$limit, $start);
//        var_dump($data['query']);die;
$data['total'] = $count;
//        var_dump($data['total']);die;
//        $data['total_page']=5;
$data['total_page'] = ceil($count / $config['per_page']);
//        var_dump($data['total_page']);die;
$data['per_page'] = $config['per_page'];
//        var_dump($data['per_page']);die;
$data['theUrl'] = $module_url;
//        var_dump($data['theUrl']);die;
$data['page_links'] = $this->pagination->create_links();
if ($this->uri->segment(3) == ''&& ($params == '')) {
//                 die('here');
    $data['view_file'] = "table";
    $this->load->module('template');
    $this->template->front($data);
} else {
//            die('hello');
//            $data['view_file'] = "new_table";
//            $this->load->module('template');
//            $this->template->front($data);
    $this->load->view('new_table', $data);
}
//      
//            $this->load->model('Mdl_testimonial');
//           $data['query'] = $this->Mdl_testimonial->get_testimonial_front();
//            $data['view_file'] = "form";
//            $this->load->module('template');
//            $this->template->front($data);
//            return $query;
}
}
