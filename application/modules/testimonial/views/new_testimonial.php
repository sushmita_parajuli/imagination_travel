
<div class="clearfix"></div>
<div id="replaceTable">
   <?php foreach($query->result() as $row){ ?>
   <div class="media" style="overflow:hidden;">
<!--      <div class="col-md-2">
        <img src="<?php echo base_url();?>uploads/news/<?php echo $row->attachment;?>" class="img-responsive">
    </div>-->
    <div class="col-md-9" style="margin-bottom:20px;">
        <h4 style="color: #9e4f24;margin: 0;"><?php echo $row->title;?></h4>
        <h5 style="color: #9e4f24;margin: 0;"><?php echo $row->published_date;?></h5>
        
        <p><a style="cursor:pointer;color:green; padding-top:5px;" href="<?php echo base_url()?>news/detail_front/<?php echo $row->slug;?>">
            Read More</a></p>
            <div class="clearfix"></div>
            <!--<a class="pull-left" style="color:#9e4f24; padding-top:10px;" href="<?php echo base_url()?>news/detail/<?php echo $row->slug;?>">Read More</a>-->
        </div>
    </div>
    <?php } ?>
</div>
<div id="pagination">
    
    <div class="col-sm-6" style="margin-left: 30px;">
        <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
    </div>
    <ul class="pager">
        <?php
        if (!empty($page_links)) {
            echo $page_links;
        }
        ?>
    </ul>
</div>

<script>
    $('div').on('click', '#pagination a', function (e) {
//        alert('here');
    e.preventDefault();
    var searchParams = {};
    var theUrl = $(this).attr('href');
//    var csrf =$('[name = "csrf_test_name"]').val();
//    alert(theUrl);
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: 'parameters=' + JSON.stringify(searchParams),
            data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
        url: theUrl,
        success: function (data) {
//            alert(data);
            $('#replaceTable').html(data);
        }
    });
    return false;
});

$('thead').click(function () {
    $("#tblData").tablesorter();
});
    $('#goto').on('change', function () {
    var page = $('#goto').val();
    var base_url = $('.total').attr('b');
    var simUrl = base_url + "/";
    var theUrl = simUrl.concat(page);
     var searchParams = {};
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    var no_of_pages = parseInt($('.total').attr('a'));
    if (page != 0 && page <= no_of_pages) {
        $.ajax({
            type: 'POST',
//            data: 'parameters= ' + JSON.stringify(searchParams),
           data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
            url: theUrl,
            success: function (data) {
                $('#replaceTable').html(data);
            }
        });
    }
    else {
        alert('Enter a PAGE between 1 and ' + no_of_pages);
        $('#goto').val("").focus();
        return false;
    }
});
    $('#search1').click(function () {
    var searchParams = {};
    var base_url = $('.total').attr('b');
//    var test = $(JSON.stringify(searchParams)
//   var csrf1 = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)},
//    var csrf= $("'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'");
//    var csrf= $("input[name=csrf_test_name]").val();
//    alert(csrf);
//        var post_data = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)};
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
       data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
});
    //$("#downl o adFile").on('click', function() {
    //    downloadFile('test');
    ////   alert ("inside onclick");
    ////   window.location = "http://www.google.com";
    //});
</script> 
<script type="text/javascript">
  function myKeyPress(e){
    var code= (e.keyCode ? e.keyCode : e.which);
     var searchParams = {};
    var base_url = $('.total').attr('b');
    if(code == 13) { //Enter keycode
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
       data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
}

  }
</script>