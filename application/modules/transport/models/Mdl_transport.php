<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_transport extends Mdl_crud {



    protected $_table = "up_transport";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_transport() {

        $table = $this->_table;

        $this->db->where('status', 'live');
        $this->db->order_by('id','DESC');
        $query = $this->db->get($table);

        return $query;

    }
    
    function get(){
        $table = $this->_table;
        $this->db->where('status','live');
        $this->db->order_by('id','ASC');
        $query=$this->db->get($table);
        return $query;
    }


    function get_transport_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('transport')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->id] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }
    function get_details_from_slug($slug){
     $table= 'up_services';
     $this->db->where('slug',$slug);
     $query=$this->db->get($table);
     return $query;
 }


}

