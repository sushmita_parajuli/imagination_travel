<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transport extends MX_Controller {
    private $MODULE_PATH = "transport/index";
    private $group_id;
    private $MODULE = 'transport';
    private $model_name = 'mdl_transport';


    function __construct() {
        $this->load->library('session');
        parent::__construct();

        $this->load->model('mdl_transport');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }

    function index()
    {
        $slug=$this->uri->segment(1);
        $data['detail']=$this->get_details_from_slug($slug);
        $data['query']= $this->get();
        $data['country']= $this->get_country();
        $data['view_file']="transport";
        $this->load->module('template');
        $this->template->front($data);
    }

    function get(){
        $this->load->model('mdl_transport');
        $query = $this->mdl_transport->get();
        return $query->result_array();
    }
    function detail(){
        $slug=$this->uri->segment(3);
        $data['detail']=$this->get_details_from_slug($slug);
        $data['details']=$data['detail'];

//        var_dump($data['detail']);die;
        $data['view_file']="details";
        $this->load->module('template');
        $this->template->front($data);

    }
    function detail_front(){
        $slug=$this->uri->segment(3);
        $data['detail']=$this->get_details_front($slug);
        $data['details'] =  $data['detail'];
//        var_dump($data['detail']);die;
        $data['view_file']="details_front";
        $this->load->module('template');
        $this->template->front($data);

    }
    function get_country(){
        $this->load->model('trekking/Mdl_trekking');
        $query = $this->Mdl_trekking->get_country_dropdown();
        return $query;
    }

    function submit() {
        $data['package'] = $package = $this->input->post('packages', TRUE);
        // var_dump($package);die;
        $data['name'] = $name = $this->input->post('name', TRUE);
        $data['country'] = $country = $this->input->post('country', TRUE);
        $data['citizen'] = $citizen = $this->input->post('citizen', TRUE);
        $data['email'] = $sender = $this->input->post('email', TRUE);
        $data['phone'] = $phone = $this->input->post('phone', TRUE);
        $data['message']= $messge1 = $this->input->post('message',TRUE);
        $message = "name:".$name . PHP_EOL."phone:".$phone . PHP_EOL."Email:".$sender . PHP_EOL."country:".$country . PHP_EOL."citizen no:" .$citizen . PHP_EOL."package:".$package . PHP_EOL.'message:'.$messge1;
        $status = $this->send_email($sender, $message, $name);
        if ($status == true) {
            echo true;
        } else {
            echo false;
        }
    }

    function details()
    {

        $slug = $this->uri->segment(3);
        $data['details'] =  $this->get_details_from_slug($slug);
        $data['view_file']="details";
        $this->load->module('template');
        $this->template->front($data);
    }
    function get_details_from_slug($slug)
    {
        $this->load->model('mdl_transport');
        $query = $this->mdl_transport->get_details_from_slug($slug);
        return $query->result_array();
    }

    function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
//        $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://smtp.googlemail.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'bibek.munikar@gmail.com', // change it to yours
//            // 'smtp_pass' => 'sushie184617',  
//            // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
    $config['protocol'] = 'mail';
    $config['mailpath'] = '/usr/sbin/mail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('bibek.munikar@gmail.com');
         $this->email->cc('sushmi2468@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Book Now Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

}
