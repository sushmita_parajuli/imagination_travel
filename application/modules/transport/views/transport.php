<div class="content-wrap pad clearfix">
    <!-- service -->
    <div class="row">
        <?php foreach ($detail as $row) {?>
        <div class="clearfix">
            <div class="col-sm-12">
                <figure class="photo">
                    <img class="img-responsive" src="<?php echo base_url() ?>uploads/<?php echo $row['attachment'] ?>" alt="">
                </figure>
            </div>
        </div>

        <div class="col-md-12">
            <div class="trek-list">
                <h2>Transportation in Nepal</h2>
                <p><?php echo $row['description']; ?></p>
            </div>
        </div>
        <?php  }?>
        <?php foreach ($query as $transport) {?>
        <div class="col-sm-12">
            <div class="responsive-tabs itinerary">
                <h2>Half day</h2>
                <div>
                    <p><?php echo $transport['half_day'] ;?></p>

                </div>
                <h2>Full Day</h2>
                <div>
                    <p><?php echo $transport['full_day'];?></p>
                </div>
            </div>
        </div>
        <?php }?>
        
        <div class="book-form-wrap col-sm-10 col-sm-offset-1">
            <div class="book-form-holder">
                <div class="heading">
                    <h2 class="text-center">Book Now</h2>
                </div>
                <div class="form-wrapper clearfix">
                    <div class="col-sm-12 holder">
                        <?php echo form_open('', 'id="f1" class="book_now"'); ?>
                        <h4 class="col-sm-12">Personal Info</h4>
                        <div class="form-group col-sm-6">
                            <!-- <label class="col-sm-2" for="name">Name</label> -->
                            <input id="b_name" type="text" class=" form-control" name="name" placeholder="Your Full Name">
                        </div>

                        <div class="form-group col-sm-6">
                                                    <!-- <select class="form-control" id="sel1">
                                                        <option>1</option>
                                                        
                                                    </select> -->
                                                    <?php 
                                                    $selected = $country;
                                                    $options = $country;
                                                    echo form_dropdown('country', $options, $selected ,'id="b_country" class="form-control"');
                                                    ?>
                                                </div> 
                                                <div class="form-group col-sm-6">
                                                    <!-- <label class="col-sm-2" for="name">Email</label> -->
                                                    <div class="">
                                                        <!-- <input id="b_dob" type="email" class="form-control" name="dob" placeholder="Your DOB"> -->
                                                        <select class="form-control" id="b_package" name="package">
                                                            <option>Half Day</option>
                                                            <option>Full Day</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <!-- <label class="col-sm-2" for="name">Name</label> -->
                                                    <div class="">
                                                        <input id="b_citizen" type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <!-- <label class="col-sm-2" for="name">Email</label> -->
                                                    <div class="">
                                                        <input id="b_email" type="email" class="form-control" name="email" placeholder="Your Email Address">
                                                    </div>

                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <!-- <label class="col-sm-2" for="phone">Phone</label> -->
                                                    <div class="">
                                                        <input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <!-- <label class="col-sm-2" for="phone">Phone</label> -->
                                                        <textarea placeholder="Message" rows="3" id="b_message"></textarea>
                                                        <!-- <input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number"> -->
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <?php echo form_checkbox('featuretype', 'is_featured', isset($post->featuretype) && $post->featuretype == 'is_featured' ? TRUE : FALSE, 'id="termsCheck"'); ?>
                                                        I accept all terms and conditions.
                                                    </div>
                                                <div class="btn-wrap col-sm-12">
                                                    <a onclick="submit()" id="submit1" class="btn btn-success">Book Now
                                                    </a>
                                                </div>

                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div id="replaceDiv"></div>
                            </div>

                        </div>
                    </div>
                </div> <!-- /.service end -->
            </div> <!-- /.content-wrap end -->


            <script type="text/javascript">
            function submit() {


                var base_url = ' <?php echo base_url(); ?>';
                var b_package = $("#b_package").val();
                var b_citizen = $("#b_citizen").val();
                var b_name = $('#b_name').val();
                var b_country = $('#b_country').val();
                var b_email = $('#b_email').val();
                var b_phone = $('#b_phone').val();
                var b_message= $('#b_message').val();
                // alert(b_package);

                if (b_package != ''  && b_citizen != '' && b_name !='' &&
                    b_country != '' && b_email != '' && b_phone != '' && b_message != '' ) {
                    if($('#termsCheck').prop('checked')){
                        $.ajax({
                            type: 'POST',
                            url: base_url + 'transport/submit',
                            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', packages: b_package,  citizen: b_citizen, name: b_name, country: b_country , email: b_email , phone: b_phone, message: b_message},
                            success: function (data) {
                                if (data == true) {
                                    $('#replaceDiv').html('<div  class="col-md-12 alert alert-success" >Thank you for booking. We will reply soon!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="col-md-12 alert alert-danger" >Sorry! Try again. Please provide all the details. </div>');
                                                }

                                            }
                                        });
} else{
    alert('Please accept our terms and conditions.')
}
}
else {
    alert('Please fill up all fields');
}
}
</script>
<script>
$('#datepicker').datepicker();

$("#datepicker").on('changeDate', function (ev) {
    $(this).datepicker('hide');
});
</script>



 <script>
    $(document).ready(function () {
        RESPONSIVEUI.responsiveTabs();
    })
    </script>