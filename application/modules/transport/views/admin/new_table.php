<div class="widget box" id="replaceTable"> 
    <div><?php
    if (($this->session->flashdata('operation'))) {
        echo $this->session->flashdata('operation');
    }
    ?></div>

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> Transport </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content" id="replaceTable"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive "> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <?php foreach ($columns as $column) { ?>
                    <th><?php 
                    $this->lang->load('transport','english');
                    echo $this->lang->line(implode(' ', explode('_', $column))); ?></th>
                    <?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 

            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                <tr> 
                    <td class="checkbox-column"><?php
                    echo $sno;
                    $sno++;
                    ?></td> 
                    <?php foreach ($columns as $column) {
                        ?>
                        <?php if ($column == "attachment") { ?>
                        <td><img src="<?php echo base_url(); ?>uploads/transport/<?php echo $row->$column; ?>" style="height:50px;"/></td>
                        <?php } else{ ?>
                        <td><?php echo Ucfirst($row->$column); ?></td><?php }
                    }
                    ?>
                    <td class="edit">
                        <?php if (isset($permission['view'])) { ?><a href="<?php echo base_url() ?>admin/transport/view/<?php echo base64_encode($row->id); ?>"><i class="icon-eye-open"></i></a><?php } ?>
                        <?php if (isset($permission['edit'])) { ?>&nbsp;/&nbsp;                 
                        <a href="<?php echo base_url() ?>admin/transport/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a> <?php } ?>                  
                    </td> 
                </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
        
    </div>
</div>
