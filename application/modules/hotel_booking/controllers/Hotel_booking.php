<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotel_booking extends MX_Controller {
    private $MODULE_PATH = "hotel_booking/index";
    private $group_id;
    private $MODULE = 'hotel_booking';
    private $model_name = 'mdl_hotel_booking';


    function __construct() {
        $this->load->library('session');
        parent::__construct();

        $this->load->model('mdl_hotel_booking');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }


    function index(){
        // die('here');
        $slug = $this->uri->segment(1);
        if($this->input->post('location_id')){
         $location_id= $this->input->post('location_id'); 
     }
     else{
        $location_id = '' ;
    }

    $data['location']= $this->get_location();
    $data['facility']= $this->get_services($slug);
      // var_dump($data['facility']);die;
    $data['hotel_two'] =$this->get_hotel_two($location_id);
    $data['hotel_one'] =$this->get_hotel_one($location_id);
    $data['hotel']= $this->get_hotel($location_id);
    // var_dump($location_id);die;

    if ($this->uri->segment(3) == ''&& ($location_id == '')) {
        $data['view_file']="hotel_booking";
        $this->load->module('template');
        $this->template->front($data);
    }else if($location_id == '1'){
        $this->load->view('new_hotel_booking', $data);
    }else{
        $this->load->view('other_hotel_booking', $data);
    }

        

    
}


function detail(){
    $slug=$this->uri->segment(3);
    $data['detail']=$this->get_details_from_slug($slug);
    $data['details']=$data['detail'];
//        var_dump($data['detail']);die;
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);

}
function detail_front(){
    $slug=$this->uri->segment(3);
    $data['detail']=$this->get_details_front($slug);
    $data['details'] =  $data['detail'];
//        var_dump($data['detail']);die;
    $data['view_file']="details_front";
    $this->load->module('template');
    $this->template->front($data);

}

function getParam($param) {
    $this->load->model('mdl_hotel_booking');
    $query = $this->mdl_hotel_booking->getParam($param);
    return $query->result_array();
} 
function get_where($id){

   $query = $this->mdl_hotel_booking->get_where($id);
   return $query;
}
function get_where_1($year,$month){

   $query = $this->mdl_hotel_booking->get_where_1($year,$month);
   return $query;
}
function get($order_by){
   $this->load->model('mdl_hotel_booking');
   $query = $this->mdl_hotel_booking->get($order_by);
   return $query->result_array();
}
function details()
{

    $slug = $this->uri->segment(3);
    $data['details'] =  $this->get_details_from_slug($slug);
     $data['country']= $this->get_country();
    $data['view_file']="details";
    $this->load->module('template');
    $this->template->front($data);
}
function get_details_from_slug($slug)
{
    $this->load->model('mdl_hotel_booking');
    $query = $this->mdl_hotel_booking->get_details_from_slug($slug);
    return $query->result_array();
}
function get_details_front($slug)
{
    $this->load->model('mdl_hotel_booking');
    $query = $this->mdl_hotel_booking->get_details_front($slug);
    return $query->result_array();
}




function get_location(){
    $this->load->model('location/Mdl_location');
    $query = $this->Mdl_location->get_location_dropdown();
//        var_dump($query);die;
    return $query;
}
function get_services($slug){
    $this->load->model('services/Mdl_services');
    $query = $this->Mdl_services->get_services_new($slug);
    return $query->result_array();
}
function get_hotel_two($location_id){
  $this->load->model('Mdl_hotel_booking');
  $query = $this->Mdl_hotel_booking->get_hotel_two($location_id);
  return $query->result_array();  
}
function get_hotel($location_id){
  $this->load->model('Mdl_hotel_booking');
  $query = $this->Mdl_hotel_booking->get_hotel($location_id);
  return $query->result_array();  
}
function get_hotel_one($location_id){
  $this->load->model('Mdl_hotel_booking');
  $query = $this->Mdl_hotel_booking->get_hotel_one($location_id);
  return $query->result_array();  
}

function get_country(){
    $this->load->model('trekking/mdl_trekking');
    $query = $this->mdl_trekking->get_country_dropdown();
    return $query;
}

function submit() {
    $data['package'] = $package = $this->input->post('packages', TRUE);
    $data['start'] = $checkin = $this->input->post('Checkin', TRUE);
    $data['end'] = $checkout = $this->input->post('checkout', TRUE);
    $data['people'] = $people = $this->input->post('people', TRUE);
    $data['name'] = $name = $this->input->post('name', TRUE);
    $data['country'] = $country = $this->input->post('country', TRUE);
    // $data['dob'] = $dob = $this->input->post('dob', TRUE);
    // $data['citizen'] = $citizen = $this->input->post('citizen', TRUE);
    $data['email'] = $sender = $this->input->post('email', TRUE);
    $data['phone'] = $phone = $this->input->post('phone', TRUE);
    // var_dump($data);die;
    $message = "name:".$name . PHP_EOL."phone:".$phone . PHP_EOL."country:".$country . PHP_EOL ."package:".$package . PHP_EOL."check In:".$checkin .PHP_EOL."check out:".$checkout.PHP_EOL."No of people:".$people;
    $status = $this->send_email($sender, $message, $name);
    if ($status == true) {
        echo true;
    } else {
        echo false;
    }
}


function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
//    $config = Array(
//        'protocol' => 'smtp',
//        'smtp_host' => 'ssl://smtp.googlemail.com',
//        'smtp_port' => 465,
//            'smtp_user' => 'sushmi2468@gmail.com', // change it to yours
//            'smtp_pass' => 'Sushie184617',  
//            // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
    $config['protocol'] = 'mail';
    $config['mailpath'] = '/usr/sbin/mail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('sushmi2468@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Book Now Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}
