<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_hotel_booking extends Mdl_crud {



    protected $_table = "up_hotel_booking";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }

    function get_hotel_booking() {

        $table = $_table;
        $this->db->where('status', 'live');

        $this->db->order_by('id','DESC');
        $query = $this->db->get($table);

        return $query;

    }
    function get_hotel_two($location_id) {

        $table = $this->_table;
        if($location_id == ''){
           $this->db->where('location_id',1);
       }else{
           $this->db->where('location_id',$location_id);
       }
       
       $this->db->where('category','normal');
       $this->db->where('status', 'live');
       $this->db->order_by('id','DESC');
       $query = $this->db->get($table);

       return $query;

   }

   function get_hotel_one($location_id) {

    $table = $this->_table;
    if($location_id == ''){
       $this->db->where('location_id',1);
   }else{
       $this->db->where('location_id',$location_id);
   }
   $this->db->where('category','five_star');
   $this->db->where('status', 'live');
   $this->db->order_by('id','DESC');
   $query = $this->db->get($table);

   return $query;

}

function get_hotel($location_id) {

    $table = $this->_table;
    if($location_id == ''){
       $this->db->where('location_id',1);
   }else{
       $this->db->where('location_id',$location_id);
   }
  
   $this->db->where('status', 'live');
   $this->db->order_by('id','DESC');
   $query = $this->db->get($table);

   return $query;

}


function get_hotel_booking_dropdown() {

    $this->db->select('id, title');

    $this->db->order_by('title');

    $dropdowns = $this->db->get('hotel_booking')->result();

    foreach ($dropdowns as $dropdown) {

        $dropdownlist[$dropdown->id] = $dropdown->title;

    }

    if (empty($dropdownlist)) {

        return NULL;

    }

    $finaldropdown = $dropdownlist;

    return $finaldropdown;

}
function get_details_from_slug($slug){
       $table= "up_hotel_booking";
       $this->db->where('slug',$slug);
       $query=$this->db->get($table);
       return $query;
   }


}

