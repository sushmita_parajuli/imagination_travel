<div class="booking ">
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <?php
                echo form_open_multipart('hotel_booking', 'class="form-horizontal row-border" id="validate-1"');

                ?>
                <div class=" pull-right" >
                   <span > Select Location: 
                        <?php 
                        $selected = $location_id;
                        $options = $location;
                        echo form_dropdown('location_id', $options, $selected ,'id="location_id"');
                        ?>
                </span>

            </div>
            <?php echo form_close();
            ?>  
            <h2>Hotel Booking</h2>
        </div>
        <div class="responsive-tabs">
            <h2>Our Top Picks</h2>
            <div>
                <div class="media">
                    <div class="row">
                        <div class="media-left col-sm-4">
                            <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                        </div>
                        <div class="media-body col-sm-8">
                            <h4 class="media-heading">Hotel Kathmandu</h4>
                            <p>Thamel, Kathmandu</p>
                            <p>Lorem ipsum...</p>
                            <a href="booking-details.php" class="btn btn-success">View Detail</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="media">
                    <div class="row">
                        <div class="media-left col-sm-4">
                            <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                        </div>
                        <div class="media-body col-sm-8">
                            <h4 class="media-heading">Hotel Kathmandu</h4>
                            <p>Thamel, Kathmandu</p>
                            <p>Lorem ipsum...</p>
                            <a href="booking-details.php" class="btn btn-success">View Detail</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="media">
                    <div class="row">
                        <div class="media-left col-sm-4">
                            <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                        </div>
                        <div class="media-body col-sm-8">
                            <h4 class="media-heading">Hotel Kathmandu</h4>
                            <p>Thamel, Kathmandu</p>
                            <p>Lorem ipsum...</p>
                            <a href="booking-details.php" class="btn btn-success">View Detail</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="media">
                    <div class="row">
                        <div class="media-left col-sm-4">
                            <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                        </div>
                        <div class="media-body col-sm-8">
                            <h4 class="media-heading">Hotel Kathmandu</h4>
                            <p>Thamel, Kathmandu</p>
                            <p>Lorem ipsum...</p>
                            <a href="booking-details.php" class="btn btn-success">View Detail</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="media">
                    <div class="row">
                        <div class="media-left col-sm-4">
                            <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                        </div>
                        <div class="media-body col-sm-8">
                            <h4 class="media-heading">Hotel Kathmandu</h4>
                            <p>Thamel, Kathmandu</p>
                            <p>Lorem ipsum...</p>
                            <a href="booking-details.php" class="btn btn-success">View Detail</a>
                        </div>
                    </div>
                </div>
                <hr>
                <ul class="pagination pull-right">
                    <li><a href="#">1</a></li>
                    <li class="active"><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>

            <h2>Lower First Price</h2>
            <div>
                <div>
                    <div class="media">
                        <div class="row">
                            <div class="media-left col-sm-4">
                                <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                            </div>
                            <div class="media-body col-sm-8">
                                <h4 class="media-heading">Hotel Kathmandu</h4>
                                <p>Thamel, Kathmandu</p>
                                <p>Lorem ipsum...</p>
                                <a href="booking-details.php" class="btn btn-success">View Detail</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="row">
                            <div class="media-left col-sm-4">
                                <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                            </div>
                            <div class="media-body col-sm-8">
                                <h4 class="media-heading">Hotel Kathmandu</h4>
                                <p>Thamel, Kathmandu</p>
                                <p>Lorem ipsum...</p>
                                <a href="booking-details.php" class="btn btn-success">View Detail</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="row">
                            <div class="media-left col-sm-4">
                                <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                            </div>
                            <div class="media-body col-sm-8">
                                <h4 class="media-heading">Hotel Kathmandu</h4>
                                <p>Thamel, Kathmandu</p>
                                <p>Lorem ipsum...</p>
                                <a href="booking-details.php" class="btn btn-success">View Detail</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="row">
                            <div class="media-left col-sm-4">
                                <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                            </div>
                            <div class="media-body col-sm-8">
                                <h4 class="media-heading">Hotel Kathmandu</h4>
                                <p>Thamel, Kathmandu</p>
                                <p>Lorem ipsum...</p>
                                <a href="booking-details.php" class="btn btn-success">View Detail</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="row">
                            <div class="media-left col-sm-4">
                                <img src="image/hotel/delux.jpg" class="media-object img-responsive">
                            </div>
                            <div class="media-body col-sm-8">
                                <h4 class="media-heading">Hotel Kathmandu</h4>
                                <p>Thamel, Kathmandu</p>
                                <p>Lorem ipsum...</p>
                                <a href="booking-details.php" class="btn btn-success">View Detail</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <ul class="pagination pull-right">
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-4">
        <div class="trek-package">   
         <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Related Services</h3>
            </div>
            <div class="panel-body">
                <ul class="activities-links">
                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Treking</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mountain Flight</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sight Seeing</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Transport</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ticketing</a></li>

                </ul>
            </div>
        </div>

    </div>
</div>
</div>

</div>

<script type="text/javascript">

    $("#location_id").keyup(function(){
        var base_url = ' <?php echo base_url(); ?>';
        var location_id = $("#location_id").val();
        if(location_id != ''){
            $.ajax({
                type: 'POST',
                url: base_url ,
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', location_id: location_id},
                success: function (data) {
                    if (data == true) {
                        $('#replaceDiv').html('<div  class="col-md-12" style="margin-top:-20px;">Thank you!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="col-md-12" style="margin-top:-20px;">Sorry! Try again later.</div>');
                                                }

                                            }
                                        });
        }

    });


</script>

