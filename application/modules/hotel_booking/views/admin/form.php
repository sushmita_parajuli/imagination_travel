<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<div class="row"> 
	<div class="col-md-12"> 
    <div class="widget box"> 
      <div class="widget-header"> 
       <h4><i class="icon-reorder"></i> Hotel Booking</h4> 
     </div> 
     <div class="widget-content">
       <?php
       echo form_open_multipart('admin/hotel_booking/submit', 'class="form-horizontal row-border" id="validate-1"');				
       ?>                
       <div class="form-group"> 
         <label class="col-md-2 control-label">Title</label> 
         <div class="col-md-10"> 
           <?php echo form_input('title', $title, 'class="form-control"');?>
         </div> 
       </div>
       <div class="form-group"> 
        <label class="col-md-2 control-label">Location<span class="required">*</span></label> 
        <div class="col-md-10"> 
          <?php $selected = $location_id;
          $options = $location;
          echo form_dropdown('location_id', $options, $selected, 'class="form-control"');
          ?>
        </div> 
      </div> 


      <div class="form-group"> 
       <label class="col-md-2 control-label">Address</label> 
       <div class="col-md-10"> 
         <?php echo form_input('address', $address, 'class="form-control"');?>
       </div> 
     </div>

     <div class="form-group"> 
       <label class="col-md-2 control-label">Category</label> 
       <div class="col-md-10"> 
        <?php $selected = $category;$options = array(
          'five_star'  => 'Five star',
          'normal'    => 'Normal',
          );                            
          echo form_dropdown('category', $options, $selected,'class="form-control"');?>
        </div> 
      </div>  

      <div class="form-group"> 
        <label class="col-md-2 control-label">Description</label> 
        <div class="col-md-10">
          <?php echo form_textarea(array('id' => 'editor1', 'name' => 'description', 'value' => $description, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
        </div> 
      </div>
      <div class="form-group"> 
       <label class="col-md-2 control-label">Bed</label> 
       <div class="col-md-10"> 
        <?php $selected = $bed;$options = array(
          '1 Single Bed' => 'One single bed',
          '2 Single Bed' => 'Two single bed',
          '1 Double Bed' =>'One double bed',
          '2 Double Bed' =>'Two double bed',
          );                            
          echo form_dropdown('bed', $options, $selected,'class="form-control"');?>
        </div> 
      </div> 
      <div class="form-group"> 
       <label class="col-md-2 control-label">View</label> 
       <div class="col-md-10"> 
        <?php $selected = $view;$options = array(
          'Mountain' => 'Mountain',
          'City' => 'City',
          'Lake' =>'Lake',
          'Sunshine' =>'sunshine',
          );                            
          echo form_dropdown('view', $options, $selected,'class="form-control"');?>
        </div> 
      </div> 
      <div class="form-group"> 
       <label class="col-md-2 control-label">Wifi</label> 
       <div class="col-md-10"> 
        <?php $selected = $wifi;$options = array(
          'Yes' => 'Yes',
          'No' => 'No',
          );                            
          echo form_dropdown('wifi', $options, $selected,'class="form-control"');?>
        </div> 
      </div>
       <div class="form-group"> 
       <label class="col-md-2 control-label">Room Service</label> 
       <div class="col-md-10"> 
        <?php $selected = $room_service;$options = array(
          'Yes' => 'Yes',
          'No' => 'No',
          );                            
          echo form_dropdown('room_service', $options, $selected,'class="form-control"');?>
        </div> 
      </div>
       <div class="form-group"> 
       <label class="col-md-2 control-label">Breakfast</label> 
       <div class="col-md-10"> 
        <?php $selected = $breakfast;$options = array(
          'Yes' => 'Yes',
          'No' => 'No',
          );                            
          echo form_dropdown('breakfast', $options, $selected,'class="form-control"');?>
        </div> 
      </div>

 <div class="form-group"> 
       <label class="col-md-2 control-label">Hot & Cold Water</label> 
       <div class="col-md-10"> 
        <?php $selected = $water;$options = array(
          'Yes' => 'Yes',
          'No' => 'No',
          );                            
          echo form_dropdown('water', $options, $selected,'class="form-control"');?>
        </div> 
      </div>


      <div class="form-group">
       <label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
       <div class="col-md-10"> 
        <?php  if(!empty($update_id)){

         $attach_prop = array(
          'type' => 'file',
          'name' => 'userfile',
          'value' => $attachment
          );
       }else{
         $attach_prop = array(
          'type' => 'file',
          'name' => 'userfile',
          'value' => $attachment,
          'class' => 'required'
          );
       }
       ?>

       <?php echo form_upload($attach_prop);?>
       <p class="help-block">
        Images only (jpg/jpeg/gif/png)</p>
        <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
          <?php if(!empty($update_id)){?>
          <img src="<?php echo base_url();?>uploads/hotel_booking/<?php echo $attachment;?>" style="height:100px;"/>
          <?php }?>
        </label>
      </div>

    </div>

    <div class="form-group"> 
     <label class="col-md-2 control-label">Status</label> 
     <div class="col-md-10"> 
      <?php $selected = $status;$options = array(
        'draft'  => 'draft',
        'live'    => 'live',
        );                            
        echo form_dropdown('status', $options, $selected,'class="form-control"');?>
      </div> 
    </div>  




    <div class="form-actions"> 
      <?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
              ?>
            </div>                 

            <?php echo form_close(); ?>                
          </div> 
        </div> 
      </div>
    </div>

    <script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1', {
      filebrowserBrowseUrl: '<?php echo base_url()?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
      filebrowserUploadUrl: '<?php echo base_url()?>assets/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
      filebrowserImageBrowseUrl: '<?php echo base_url()?>assets/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',

    });
    </script>