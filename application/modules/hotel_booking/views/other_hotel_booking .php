<div class="responsive-tabs">
<h2>Our hotels</h2>
<div>
   <?php foreach ($hotel as $row) {?>
   <div class="media">
    <div class="row">
        <div class="media-left col-sm-4">
            <img src="<?php echo base_url()?>uploads/hotel_booking/<?php echo $row['attachment'] ?>" class="media-object img-responsive">
        </div>
        <div class="media-body col-sm-8">
            <h4 class="media-heading"><?php echo $row['title']; ?></h4>
            <p><?php echo $row['address']; ?></p>
            <p><?php echo word_limiter ($row['description'],1);?></p>
            <a href="<?php echo base_url()?>hotel_booking/details/<?php echo $row['slug']?>" class="btn btn-success">View Detail</a>
        </div>
    </div>
</div>
<?php  }
?>
<hr>
</div>
</div>
<script>
    $(document).ready(function () {
        RESPONSIVEUI.responsiveTabs();
    })
</script>
