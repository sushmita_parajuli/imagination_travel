<div class="booking ">
    <div class="row">
    
        <div class="col-md-8">
            <div class="box">
                <?php
                echo form_open_multipart('hotel_booking', 'class="form-horizontal row-border" id="validate-1"');
                ?>
                <div class=" pull-right" >
                   <span> <strong style="color: #fff; font-weight: 400;">Select Location: </strong>
                        <?php 
                            // $selected = $location_id;
                        $options = $location;
                        echo form_dropdown('location_id', $options, '' ,'id="location_id"');
                        ?>
                    </span>
                </div>
                <?php echo form_close();?>  
                <h2>Hotel Booking</h2>
            </div>
                <div id="replaceTable">
                    <div class="responsive-tabs">
                        <h2>Five stars hotel</h2>
                        <div>
                         <!-- <div class="row"> -->
                           <?php foreach ($hotel_one as $row) {?>
                           <div class="media">
                                <div class="media-left col-sm-4">
                                    <img src="<?php echo base_url()?>uploads/hotel_booking/<?php echo $row['attachment'] ?>" class="media-object img-responsive">

                                </div>
                                <div class="col-sm-8">
                                    <h4 class="media-heading"><?php echo $row['title']; ?></h4>
                                    <p><?php echo $row['address']; ?></p>
                                    <p><?php echo word_limiter ($row['description'],15);?></p>
                                    <a href="<?php echo base_url()?>hotel_booking/details/<?php echo $row['slug']?>" class="btn btn-success">View Detail</a>
                                </div>
                            </div>
                            <hr>
                            <?php  }  ?>
                    <!-- </div> -->
                        </div>
                        <h2>Others</h2>
                        <div>
                            <!-- <div class="row"> -->
                                <?php foreach ($hotel_two as $hotel) {?>
                                <div class="media">
                                    <div class="media-left col-sm-4">
                                        <img src="<?php echo base_url()?>uploads/hotel_booking/<?php echo $hotel['attachment']?>" class="media-object img-responsive">
                                    </div>
                                    <div class="col-sm-8">
                                        <h4 class="media-heading"><?php echo $hotel['title']; ?></h4>
                                        <p><?php echo $hotel['address'];?></p>
                                        <p><?php echo word_limiter($hotel['description'],20);?>.</p>
                                        <a href="<?php echo base_url()?>hotel_booking/details/<?php echo $hotel['slug']?>" class="btn btn-success">View Detail</a>
                                    </div>
                                </div>
                                <hr>
                                <?php }?>
                                <div>
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="trek-package">   
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Related Services</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="activities-links">
                             <?php foreach ($facility as $facilities) {

                                $name = str_replace('-', '_', $facilities['slug']);
                                ?> <li>
                                <a href="<?php echo base_url() ?><?php echo $name?>">
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $facilities['title']; ?>
                                </a>
                            </li>
                            <?php   }?>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>

    </div>
</div>

<script type="text/javascript">
$("#location_id").change(function(){
    var base_url = ' <?php echo base_url(); ?>hotel_booking';
    var location_id = $("#location_id").val();
    if(location_id != ''){
        $.ajax({
            type: 'POST',
            url: base_url ,
            data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","location_id":location_id},
            success: function (data) {
                $('#replaceTable').html(data);
            }
        });
    }
});
</script>

 <script>
    $(document).ready(function () {
        RESPONSIVEUI.responsiveTabs();
    })
    </script>