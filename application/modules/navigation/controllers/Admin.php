<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/navigation";
    private $group_id;
    private $model_name = 'Mdl_navigation_group';
    private $model_name1 = 'Mdl_navigation';
    private $MODULE = 'navigation';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('Mdl_navigation_group');
        $this->load->model('pages/mdl_pages');
        $this->load->model('modules/mdl_moduleslist');
        $this->load->model('Mdl_navigation');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('navigation'); //module name is navigation here	
    }

    //-------------------start of group nav----------------------//	
    function index() {
        //table select parameters
        $main_table_params = 'id,title,slug';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
        }
        $count = $this->Mdl_navigation_group->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] = 5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_navigation_group->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['column'] = 'title';
        $data['slug'] = 'slug';
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/group_table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function create_group() {
        $update_id = base64_decode($this->uri->segment(4));
        if ($update_id != 1 && $update_id != 2) { //because 1 is header and 2 is footer which i don't want to change or edit or delete
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
                //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,slug';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/group_form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    } else {
        redirect('admin/navigation');
    }
}

function delete_group() {
    $delete_id = $this->uri->segment(4);

    if (!isset($delete_id) || !is_numeric($delete_id)) {
        unset($delete_id);
        redirect('admin/navigation');
    } else {
        $this->Mdl_navigation_group->_delete_group($delete_id);
        redirect('admin/navigation');
    }

    $data['view_file'] = "admin/group_table";
    $this->load->module('template/admin_template');
    $this->admin_template->admin($data);
}

function get_data_from_post() {

    $data['title'] = $this->input->post('title', TRUE);
    $data['slug'] = strtolower(url_title($data['title']));
    return $data;
}

function submit_group() {

    $update_id = $this->input->post('update_id', TRUE);

    $this->load->library('form_validation');

    $this->form_validation->set_rules('title', 'Title', 'required|xss_clean');

    if ($this->form_validation->run($this) == FALSE) {
        $this->create();
    } else {
        $data = $this->get_data_from_post();

        if (is_numeric($update_id)) {
            $this->Mdl_navigation_group->_update($update_id, $data);
        } else {
            $this->Mdl_navigation_group->_insert($data);
        }

        redirect('admin/navigation');
    }
}

    //-------------------end of group nav----------------------//
    //-------------------------------------------------start of individual navigation----------------------------------------------------//


    function group() {//to list out every navigation individually
        $manage = $this->uri->segment(5);
        if (empty($manage)) {
            $group_id = $this->uri->segment(4);
            $existence = $this->Mdl_navigation_group->check_group_existence($group_id);
            if ($existence == FALSE) {
                redirect('admin/navigation');
            }//checking if the group_id exist or not
            else {
                // $data['query'] = $this->all_nav($group_id);

                $data['query'] = $this->all_navigation($group_id);
                 // var_dump($data['query']);die;
                $data['parent_name'] = $this->Mdl_navigation->get_parent_name($group_id);
                $data['group_id'] = $group_id;
                $data['group_title'] = $this->Mdl_navigation_group->get_group_title($group_id);
                $data['view_file'] = "admin/nav_table";
                $this->load->module('template/admin_template');
                $this->admin_template->admin($data);
            }
        } else {
            $this->$manage();
        }
    }
    function all_navigation($group_id){

        $parents = $this->get_parent($group_id);
        // $data['parentnav'] = $parents;
        // var_dump($parents);
        $i = 0;
        foreach($parents as $parent) {
            $children = $this->get_child($group_id,$parent['id']);
            // var_dump($children);

            $data[$i] = $parent;

            if($children != null){

                // $data['children'] = $children;
                $j=0;
                foreach ($children as $child) {
                    $grand_child = $this->all_get_grand_child($group_id,$child['id']);

                    $data[$i]['children'][$j] = $child;

                    if($grand_child != null){
                        $data[$i]['children'][$j]['grand_children'] = $grand_child;
                    }else{
                        $data[$i]['children'][$j]['grand_children'] ='';
                    }
                    $j++;
                }
            }else{
                $data[$i]['children'] ='';
            }
            
            $i++;
        }
        // print_r($data);
        return $data;

    }

   function all_nav($group_id){//to get navigation list of specific group

		$data['parentnav'] = $this->get_parent($group_id);	//

		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
				$nav['children'] = $children;
                /*for grand child*/
                // if($nav['children'] == NULL){return NULL;}
                $j=0;

                foreach($nav['children'] as $sub_child){
                    // var_dump($sub_child);
                    $grand_children =  $this->get_grand_child($group_id,$sub_child['id']);

                    if($grand_children != NULL){ 
                        $nav['children']['grand_children'] = $grand_children;
                        // $navigation[$i][$j]['children']['grand_children'] = $grand_children;
                                   // var_dump($nav['children']['grand_children']); die();

                    }
                    $j++;	
                               // var_dump($nav['children']); die();
                }
                // die();
                $navigation[$i][$j] = $nav;
                /*grand child end*/
            }
            $navigation[$i] = $nav;
            $i++;	

        }
        // var_dump($navigation); die;
        return $navigation;

    }
    function get_ind_nav($group_id) {//to get navigation list of specific group
        $query = $this->Mdl_navigation->get_ind_nav($group_id);
        return $query;
    }

    function create() {

        $update_id = $this->uri->segment(6);
        $group_id = $this->uri->segment(4);
        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_ind_data_from_post();
        } else {
            if (is_numeric($update_id)) {

                $group_id = $this->uri->segment(4);
                $nav_id = $this->uri->segment(6);
                $check_status = $this->Mdl_navigation->check_if_nav_belongs_to_group($group_id, $nav_id);

                if ($check_status == TRUE) {
//                    $select = 'title,slug,navtype,parent_id,group_id,module_id,page_id,site_uri,link_url,status';
//                    $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name1);
                    $data = $this->get_ind_data_from_db($update_id);
//                    var_dump($data);die('df');
                } else {
                    redirect('admin/navigation/group/' . $group_id);
                }
            }
        }

        if (!isset($data)) {
            $data = $this->get_ind_data_from_post();
        }

        $data['update_id'] = $update_id;
        $select = 'id,title';
        $params = array('parent_id' => 'integer=>"0"', 'group_id' => 'integer=>' . $group_id);
//        $data['parent'] = $this->common_functions->get_dropdown('title', $select, $params, $this->model_name1);
        $data['parent'] = $this->get_navigation($group_id);
        $data['modulelist'] = $this->common_functions->get_dropdown('title', $select, '', 'mdl_moduleslist');
        $data['pagelist'] = $this->common_functions->get_dropdown('title', $select, '', 'mdl_pages');
//         $this->load->model('category/mdl_category');
//        $data['categorylist'] = $this->mdl_category->get_category_dropdown_slug();
//        var_dump($data['categorylist']);die('d');
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function get_ind_data_from_db($update_id){
      $query = $this->get_where($update_id);
//                var_dump($query->result());die('dfs');
      foreach($query->result() as $row){
         $data['title'] = $row->title;
         $data['slug'] = $row->slug;
         $data['navtype'] = $row->navtype;
         $data['parent_id'] = $row->parent_id;
         $data['group_id'] = $row->group_id;	
         $data['module_id'] = $row->module_id;
         $data['page_id'] = $row->page_id;
         $data['site_uri'] = $row->site_uri;
         $data['link_url'] = $row->link_url;
         $data['status'] = $row->status;		
     }
     if(!isset($data)){
        $data = "";
    }
    return $data;
}
function get_where($id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_where($id);
	return $query;
}
function delete() {
    $delete_id = $this->uri->segment(6);
    $group_id = $this->uri->segment(4);

    if (!isset($delete_id) || !is_numeric($delete_id)) {
        unset($delete_id);
        redirect('admin/navigation/group/' . $group_id);
    } else {
        $this->Mdl_navigation->_delete($delete_id);
        $this->session->set_flashdata('operation', 'Deleted Successfully!!!');
        redirect('admin/navigation/group/' . $group_id);
    }
}

function submit() {
   $group_id = $this->uri->segment(4);
   $update_id = $this->input->post('update_id', TRUE);
   $this->load->library('form_validation');
   $this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
   if ($this->form_validation->run($this) == FALSE) {
    $this->create();
} else {
    $data = $this->get_ind_data_from_post();

    if ($data['navtype'] == 'Module') {
        $data['page_id'] = 'NULL';
        $data['link_url'] = 'NULL';
    } elseif ($data['navtype'] == 'Page') {
        $data['module_id'] = 'NULL';
        $data['link_url'] = 'NULL';
    } else {
        $data['module_id'] = 'NULL';
        $data['page_id'] = 'NULL';
    }
    if (is_numeric($update_id)) {
        $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);
        if (isset($permission['edit'])) {
            $this->Mdl_navigation->_update($update_id, $data);
            $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);
            if (isset($permission['edit'])) {
//                         var_dump($data);die('df');
                $this->Mdl_navigation->_update($update_id, $data);
            }
        }
        $this->session->set_flashdata('operation', 'Updated Successfully!!!');
    } else {
        $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);

                    $new_position = $this->Mdl_navigation->get_next_position($data['group_id']); //becuz we have to set the position as well
                    $data['position'] = $new_position;
                    //and the next position will be be fore new nav
                    if (isset($permission['add'])) {
//                        var_dump($data);die('df');
                        $this->Mdl_navigation->_insert($data);
                    }
                    $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
                   // redirect('admin/navigation/group/' . $data['group_id']);
                }
                redirect('admin/navigation/group/' . $data['group_id']);
                die;
            }
        }
        


        function submit_nest() {
            $new_array = $_POST;

            $group_id = $new_array['group_id'];

            $jsoncode = json_decode($new_array['jsoncode'], true, 64);
            $i = 1;
            $position_array = array();
            foreach ($jsoncode as $row) {
                $parent_id = 0;
                $position = $row['id'];

                $position_array[$i] = $row['id'];
                $i++;


                $this->Mdl_navigation->update_parent_id_from_position($parent_id, $position, $group_id);

                if (isset($row['children'])) {
                    foreach ($row['children'] as $child) {
                        $parent_id_for_children = $this->Mdl_navigation->get_id_of_parent_from_position_number($row['id']);
                        $parent_id = $parent_id_for_children;

                        $position = $child['id'];
                        $position_array[$i] = $child['id'];
                        $i++;
                        $this->Mdl_navigation->update_parent_id_from_position($parent_id, $position, $group_id);
                    }
                }
            }
            $submit = $new_array['submit'];
            unset($new_array['group_id']);
            unset($new_array['submit']);
            $this->Mdl_navigation->update_nest($position_array);
            redirect('admin/navigation');
        }


        function get_ind_data_from_post() {
            $data['title'] = $this->input->post('title', TRUE);
            $data['slug'] = strtolower(url_title($data['title']));
            $data['navtype'] = $this->input->post('navtype', TRUE);
            $data['parent_id'] = $this->input->post('parent_id', TRUE);
            $data['group_id'] = $this->input->post('group_id', TRUE);
            $data['module_id'] = $this->input->post('module_id', TRUE);
            $data['page_id'] = $this->input->post('page_id', TRUE);
            $data['site_uri'] = $this->input->post('site_uri', TRUE);
//        $data['category_id'] = $this->input->post('category_id', TRUE);
            $data['link_url'] = $this->input->post('link_url', TRUE);
            $data['status'] = $this->input->post('status', TRUE);

            return $data;
        }




        function get_parent($group_id){
           $this->load->model('mdl_navigation');
           $query = $this->mdl_navigation->get_parentnav($group_id);	
           return $query;
       }
       function get_child($group_id,$parent_id){
           $this->load->model('mdl_navigation');
           $query = $this->mdl_navigation->get_childnav($group_id,$parent_id);
           return $query;
       }
       function get_grand_child($group_id,$parent_id){
        $this->load->model('mdl_navigation');
        $query = $this->mdl_navigation->get_childnav($group_id,$parent_id);
        return $query;   
    }
    function all_get_grand_child($group_id,$parent_id){
        $this->load->model('mdl_navigation');
        $query = $this->mdl_navigation->all_get_childnav($group_id,$parent_id);
        return $query;   
    }
    function get_navigation($group_id){
       $this->load->model('navigation/mdl_navigation');
       $query = $this->mdl_navigation->get_navigation_dropdown($group_id);
       return $query;
   }
    //-------------------------------------------------end of individual navigation----------------------------------------------------//
   function hello(){
    $two['a']['b']= 'hello';
    $three[0][0] = $two;

    var_dump($three[0][0]['a']);die;
}

}
