<?php /*$navigation is generated from admin_template controller of template module*/?>
<li>  
    <a href="javascript:void(0);">  
        <i class="icon-list-ol"></i> Navigation 
    </a>  
    <ul class="sub-menu">    
        
        <li>  
            <a href="<?php echo base_url().'admin/navigation';?>">  
                <i class="icon-pencil"></i> Manage Navigation Groups
            </a>  
        </li> 
        
        <?php foreach($navigation as $row){ ?>
		<li>	
			<a href="<?php echo base_url().'admin/navigation/group/'.$row->id;?>">  
                <i class="icon-reorder"></i> <?php echo $row->title;?>   
            </a>
		</li>
		<?php }//end of foreach ?>    
        
    </ul>  
</li> 