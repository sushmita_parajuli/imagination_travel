
<div class="row">
    <div class="col-md-8">
        <?php foreach ($query as $transport) {?>
        
        <div class="trek-description clearfix">
            <figure class="photo">
                <img class="img-responsive" src="<?php echo base_url() ?>uploads/ticketing/<?php echo $transport['attachment']?>" alt="">
            </figure>
            <div class="title">
                <h2><?php echo $transport['title']?> </h2>
            </div>
            <div class="cont">
                <p><?php echo $transport['description']?></p>
            </div>  

        </div>
        
        <?php }?>
    </div>
    
    <div class="col-md-4">
        <div class="trek-package">   
           <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Related Services</h3>
            </div>
            <div class="panel-body">
                <ul class="activities-links">
                   <?php foreach ($facility as $service) {
                    $name = str_replace('-', '_', $service['slug']);
                    ?> <li>
                    <a href="<?php echo base_url() ?><?php echo $name?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $service['title']; ?>
                    </a>
                </li>
                <?php   }?>
            </ul>
        </div>
    </div>

</div>

</div>
</div>
<div class="book-form-wrap col-sm-10 col-sm-offset-1">
    <div class="book-form-holder">
        <div class="heading">
            <h2 class="text-center">Book Now</h2>
        </div>
        <div class="form-wrapper clearfix">
            <div class="holder">
                <?php echo form_open('', 'id="f1" class="book_now"'); ?>

                    <h4 class="col-sm-12">Personal Info</h4>
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="name">Name</label> -->
                        <input id="b_name" type="text" class=" form-control" name="name" placeholder="Your Full Name">
                    </div>
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="name">Email</label> -->
                        <div class="">
                            <input id="b_location" type="location" class="form-control" name="location" placeholder="Location">
                        </div>

                    </div>
                    <div class="form-group col-sm-6">
                        <?php 
                        $selected = $country;
                        $options = $country;
                        echo form_dropdown('country', $options, $selected ,'id="b_country" class="form-control"');
                        ?>
                    </div> 
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="name">Name</label> -->
                        <div class="">
                            <input id="b_people" type="number" class=" form-control" name="no" placeholder="Number Of People">
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="name">Name</label> -->
                        <div class="">
                            <input id="b_citizen" type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="name">Email</label> -->
                        <div class="">
                            <input id="b_email" type="email" class="form-control" name="email" placeholder="Your Email Address">
                        </div>

                    </div>
                    <div class="form-group col-sm-6">
                        <!-- <label class="col-sm-2" for="phone">Phone</label> -->
                        <div class="">
                            <input id="b_phone" type="text" class="form-control" name="phone" placeholder="Your Number">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <!-- <label class="col-sm-2" for="name">Email</label> -->
                        <div class="col-sm-6 datepicker">
                            <input type="text" class="form-control input-group date" id="b_start" data-date="" data-date-format="yyyy-mm-dd" placeholder="Arival Date">
                            <!-- <input id="start_datepicker"  class="form-control" name="dob" placeholder="Start Date"> -->
                        </div>


                    </div>
                    <div class="form-group col-sm-12">
                        <?php echo form_checkbox('featuretype', 'is_featured', isset($post->featuretype) && $post->featuretype == 'is_featured' ? TRUE : FALSE, 'id="termsCheck" '); ?>
                        I accept all terms and conditions.
                    </div>
                    <div class="btn-wrap col-sm-12">
                        <a onclick="submit()" id="submit1" class="btn btn-success">Book Now
                        </a>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <div id="replaceDiv" ></div>
    
</div>
<script type="text/javascript">
function submit() {

    var base_url = ' <?php echo base_url(); ?>';
    // var b_package = $("#b_package").val();
    var b_start = $("#b_start").val();
    // var b_end = $("#b_end").val();
    var b_people = $("#b_people").val();
    var b_citizen = $("#b_citizen").val();
    var b_name = $('#b_name').val();
    var b_country = $('#b_country').val();
    var b_location = $('#b_location').val();
    var b_email = $('#b_email').val();
    var b_phone = $('#b_phone').val();
    if ( b_start != '' && b_citizen != '' && b_name !='' &&
        b_country != '' &&  b_email != '' && b_phone != '' && b_people != '') {
       if($('#termsCheck').prop('checked')){
        $.ajax({
            type: 'POST',
            url: base_url + 'mountain_flight/submit',
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', arrival: b_start, citizen: b_citizen, name: b_name, country: b_country , email: b_email , phone: b_phone},
            success: function (data) {
                if (data == true) {
                    $('#replaceDiv').html('<div  class="col-md-12 alert alert-success" >Thank you for booking. We will reply soon!!</div>');
                                                    //resetting the form 
                                                    $('#f1').trigger("reset");
                                                } else {
                                                    $('#replaceDiv').html('<div class="col-md-12 alert alert-danger" >Sorry! Try again. Please provide all the details. </div>');
                                                }

                                            }
                                        });
} else{
    alert('Please accept our terms and conditions.')
}
} else {
    alert('Please fill up all fields');
}
}
</script>


