<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mountain_flight extends MX_Controller {
    private $MODULE_PATH = "mountain_flight/index";
    private $group_id;
    private $MODULE = 'mountain_flight';
    private $model_name = 'mdl_mountain_flight';


    function __construct() {
        $this->load->library('session');
        parent::__construct();

        $this->load->model('mdl_mountain_flight');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->library('Up_pagination');
        
        $this->load->library('Common_functions');
        $this->load->model('settings/Mdl_settings');
    }

    function index()
    {
        $slug=$this->uri->segment(1);
        $data['detail']=$this->get_details_from_slug($slug);
        $data['query']= $this->get();
        $data['country']= $this->get_country();
        $data['facility']= $this->get_services($slug);
        // var_dump($data['services']);die;
        $data['view_file']="mountain_flight";
        $this->load->module('template');
        $this->template->front($data);      
    }
    function get(){
        $this->load->model('mdl_mountain_flight');
        $query = $this->mdl_mountain_flight->get();
        return $query->result_array();
    }
    function get_country(){
        $this->load->model('trekking/Mdl_trekking');
        $query = $this->Mdl_trekking->get_country_dropdown();
        return $query;
    }
    


    
    function get_details_from_slug($slug)
    {
        $this->load->model('mdl_mountain_flight');
        $query = $this->mdl_mountain_flight->get_details_from_slug($slug);
        return $query->result_array();
    }

    function get_services($slug){
        $this->load->model('services/Mdl_services');
        $query = $this->Mdl_services->get_services_new($slug);
        return $query->result_array();
    }
    function submit() {
        // $data['package'] = $package = $this->input->post('package', TRUE);
        $data['arrival'] = $start = $this->input->post('arrival', TRUE);
        // $data['end'] = $end = $this->input->post('end', TRUE);
        $data['people'] = $people = $this->input->post('people', TRUE);
        $data['name'] = $name = $this->input->post('name', TRUE);
        $data['country'] = $country = $this->input->post('country', TRUE);
        $data['dob'] = $dob = $this->input->post('dob', TRUE);
        $data['citizen'] = $citizen = $this->input->post('citizen', TRUE);
        $data['email'] = $sender = $this->input->post('email', TRUE);
        $data['phone'] = $phone = $this->input->post('phone', TRUE);
        $message = "name:".$name ."<br>". PHP_EOL."phone:".$phone ."<br>". PHP_EOL."country:".$country ."<br>". PHP_EOL ."citizen no:" .$citizen  ."<br>". PHP_EOL."Arrival date:".$start ." "."No of people:".$people;
        $status = $this->send_email($sender, $message, $name);
        if ($status == true) {
            echo true;
        } else {
            echo false;
        }
    }
    function send_email($sender, $message, $name) {
        // die($sender);
        //code for --------------Localhost-end----------------
//        $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://smtp.googlemail.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'bibek.munikar@gmail.com', // change it to yours
//            // 'smtp_pass' => 'sushie184617',  
//            // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//            );
        //code for --------------Localhost-end---------------- 
        //code for --------------live-start------------------- 
    $config['protocol'] = 'mail';
    $config['mailpath'] = '/usr/sbin/mail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
        //code for --------------live-end---------------------
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender, $name); // change it to yours
        $this->email->to('bibek.munikar@gmail.com');
        // $this->email->cc($sender_email);// change it to yours
        $this->email->subject('Book Now Notification');
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }


}
