<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Add Users</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/users/chpwd_submit', 'class="form-horizontal row-border" id="validate-1"');
                ?> 
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Previous Password <span class="required">*</span></label> 
                    <div class="col-md-5"> 
                        <?php echo form_input('old_password', $old_password, 'class="form-control required" onblur="checkOldPassword()" id="old_password"'); ?>
                    </div> 
                    <div class="col-md-5" id="matched"> 

                    </div> 
                </div> 
                <div class="form-group"> 
                    <label class="col-md-2 control-label">New Password <span class="required">*</span></label> 
                    <div class="col-md-5"> 
                        <?php echo form_password('chpwd', '', 'class="form-control required" onblur="checknewPassword()" id= "new_password" '); ?>
                    </div> 
                    <div class="col-md-5" id="correct"> 
                       
                    </div>
                </div>  

                <div class="form-actions"> 
                    <?php
                    echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                    if (!empty($update_id)) {
                        echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
<script>
    function checkOldPassword() {

        var old_password = $("#old_password").val();
//        alert(old_password);

        var url = "<?php echo base_url() ?>admin/users/check_old_password";
        $.ajax({
            type: 'POST',
            data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "old_password": old_password},
            url: url,
            success: function (result) {
                if (result == '1') {

                    $("#matched").html("Correct Old password")
                     $("#matched").css("color","green")
                } else {
                    $("#matched").html("Incorrect Old password")
                    $("#matched").css("color","red")
                }
            }
        });
        return false;
    }
</script>
<script>
    function checknewPassword() {

        var new_password = $("#new_password").val();
        

        var url = "<?php echo base_url() ?>admin/users/is_valid_password";
        $.ajax({
            type: 'POST',
            data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "new_password": new_password},
            url: url,
            success: function (result) {
                if (result == '1') {

                    $("#correct").html("Strong Password")
                     $("#correct").css("color","green")
                } else {
                    $("#correct").html("Incorrect password format")
                    $("#correct").css("color","red")
                }
            }
        });
        return false;
    }
</script>