<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE = 'users';
    private $MODULE_PATH = "admin/users";
    private $group_id;
    private $model_name = 'Mdl_users';
    private $join_table = 'up_users_groups';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->module('permissions/Mdl_permissions');
        $this->load->library('pagination');
        $this->load->library('Common_functions');
        $this->load->library('Up_pagination');
        $this->load->model('Mdl_users');
        $this->load->module('admin_login/admin_login');
        $this->load->model('settings/Mdl_settings');
        $this->admin_login->check_session_and_permission('groups'); //module name is groups here	
    }

    function index() {

        //table select parameters
        $main_table_params = 'id,email,display_name,username,created_on';

        // table join parameters
// table join parameters
        // table join parameters

        $join_params['join_table'] = 'up_users_groups';
        $join_params['select_params'] = 'title as group_name,group_id';
        $join_params['join_condition'] = 'up_users_groups.id=up_users.group_id';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
        }

        $count = $this->Mdl_users->count($params);
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
//        $data['query'] = $this->Mdl_users->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['query'] = $this->Mdl_users->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params, $join_params);
//        var_dump($data['query']);die;
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        $data['columns'] = array('username', 'email', 'display_name', 'group_name', 'created_on');
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['username'] = $this->input->post('username', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['display_name'] = $this->input->post('display_name', TRUE);
        $data['group_id'] = $this->input->post('group_id', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (!is_numeric($update_id)) {//
            $data['password'] = $this->input->post('password', TRUE);
            //$pwd = $this->zenareta_pasaoro($pwd);
            $data['created_on'] = date("Y-m-d");
            $data['activation_code'] = NULL;
            $data['last_login'] = NULL;
        }

        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);
        $select = '*';
        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['update_id'] = $update_id;
        $data['group_array'] = $this->get_groups();

        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $this->load->model('mdl_users');
        $delete_id = $this->uri->segment(4);

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/users');
        } else {
            $this->mdl_users->_delete($delete_id);
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!');
            redirect('admin/users');
        }
    }

    function submit() {

        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

            $this->form_validation->set_rules('username', 'Username', 'required|xss_clean'); //we don't want unique_validation error while editing
            $this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean');
            $this->form_validation->set_rules('display_name', 'Display Name', 'required|xss_clean');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|xss_clean|is_unique[up_users.username]'); //unique_validation check while creating new
            $this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean|is_unique[up_users.email]');
            $this->form_validation->set_rules('display_name', 'Display Name', 'required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
        }
        /* end of validation rule */

        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {

            $data = $this->get_data_from_post();
//            var_dump($data);die;
            $update_id = $this->input->post('update_id', TRUE);
//            var_dump($update_id);die;
            if (is_numeric($update_id)) {
                $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
//                var_dump($permission);die;
                if (isset($permission['edit'])) {

                    $this->Mdl_users->_update($update_id, $data);
                }

                $this->session->set_flashdata('operation', 'Updated Successfully!!!');
            } else {
                $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
                $pwd = $data['password'];
                $data['password'] = $this->zenareta_pasaoro($pwd);
                //generating pwd
                if (isset($permission['add'])) {
                    $this->Mdl_users->_insert($data);
                }

                $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
            }
//            var_dump('$pwd');die;
            redirect('admin/users');
        }
    }

    function chpwd() {
       $update_id = ($this->uri->segment(4));
//        var_dump($update_id);die;
       if (is_numeric($update_id)) {
         $chpwd_submit = $this->input->post('submit', TRUE);

         if ($chpwd_submit == "Submit") {
            $chpwd = $this->get_chpwd_from_post();
        }

        if (!isset($chpwd)) {
            $chpwd = $this->get_data_from_post();
        }

        $chdata['update_id'] = $update_id;
        $chdata['password'] = $chpwd;
        $chdata['old_password'] = $this->input->post('old_password');
        $chdata['view_file'] = "admin/chpwd";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($chdata);
    } else {
        redirect('admin/users');
    }
}


function change_password() {
   $update_id = ($this->uri->segment(4));
//        var_dump($update_id);die;
   if (is_numeric($update_id)) {
//             $data['old_password']= $this->input->post('old_password',TRUE);
//            die('hello');
    $chpwd_submit = $this->input->post('submit', TRUE);

//          var_dump($chpwd_submit);die('here');
    if ($chpwd_submit == "Submit") {
        $chpwd = $this->get_chpwd_from_post();
    }

    if (!isset($chpwd)) {
        $chpwd = $this->get_data_from_post();
    }

    $chdata['update_id'] = $update_id;
    $chdata['password'] = $chpwd;
    $chdata['old_password'] = $this->input->post('old_password');

//            var_dump($chdata['password']);die;


    $chdata['view_file'] = "admin/chpwd_admin";
    $this->load->module('template/admin_template');
    $this->admin_template->admin($chdata);
} else {
    redirect('admin/users');
}
}
function chpwd_submit() {
//        var_dump('test');die;
    $this->load->library('form_validation');

    $this->form_validation->set_rules('chpwd', 'New Password', 'required|xss_clean');

    if ($this->form_validation->run($this) == FALSE) {
        echo'the password pattern didnot match';
//            $this->chpwd();
    } else {
        $chdata['password'] = $this->get_chpwd_from_post();

        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('chpwd');
//            var_dump($new_password);die;
        $id = $this->session->userdata('user_id');
        $update_id = $this->input->post('update_id', TRUE);
        $chdata['password'] = $this->zenareta_pasaoro($new_password);

        $this->chpwd_update($update_id, $chdata);
//            $this->session->unset_userdata('logged_in');
        redirect('admin/users');


//             redirect('admin/users');
//             die('hello');
//            if (is_numeric($update_id)) {
//
//                $pwd = $chdata['password'];
//                
//            } else {
//                redirect('admin/users');
//            }
//            redirect('admin/users');
    }
}

public function is_valid_password() {
    $new_password = $_POST['new_password'];
//        print_r($new_password);die;
//        if (preg_match_all('$S*(?=S{8,})(?=S*[a-z])(?=S*[A-Z])(?=S*[d])(?=S*[W])S*$', $new_password)) {
    if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/', $new_password)) {
        echo'1';
    } else {
        echo'0';
    }
}

function get_chpwd_from_post() {
    $chpwd = $this->input->post('chpwd', TRUE);
    return $chpwd;
}

function get($order_by) {
    $this->load->model('mdl_users');
    $query = $this->mdl_users->get($order_by);
    return $query;
}

function get_groups() {
    $this->load->model('groups/mdl_groups');
    $query = $this->mdl_groups->get_groups_dropdown();
    if (empty($query)) {
        return NULL;
    }
    return $query;
}

//	function get_where($id){
//	$this->load->model('mdl_users');
//	$query = $this->mdl_users->get_where($id);
//	return $query;
//	}
//	
//	function _insert($data){
//	$this->load->model('mdl_users');
//	$this->mdl_users->_insert($data);
//	}
//
//	function _update($id, $data){
//	$this->load->model('mdl_users');
//	$this->mdl_users->_update($id, $data);
//	}
//	
//	function _delete($id){
//	$this->load->model('mdl_users');
//	$this->mdl_users->_delete($id);
//	}

function chpwd_update($id, $chpwd) {
    $this->load->model('mdl_users');
    $this->mdl_users->chpwd_update($id, $chpwd);
}

function zenareta_pasaoro($pasaoro) {
    $this->load->model('admin_login/mdl_admin_login');
    $query = $this->mdl_admin_login->enc_hash_pwd($pasaoro);
    return $query;
}

function sidebar($data) {
    echo 'syo';
}

function check_old_password() {

    $id = $this->session->userdata('user_id');

    $pd = $_POST['old_password'];

    $password = $this->Mdl_users->get_old_password_where($id);
//        var_dump($password);
    $hashed_pd = $this->Mdl_users->enc_hash_pwd($pd);
//        var_dump($hashed_pd);die;
    if ($password == $hashed_pd) {

        echo '1';
    } else {
        echo '0';
    }
}

}
