<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_admin_login extends Mdl_crud {

    protected $_table = "admin_login";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function check_login($username, $password) {
        $hashed_pword = $this->enc_hash_pwd($password);
        $query_str = "SELECT id, group_id FROM up_users WHERE ( username = '$username' or email = '$username') and password = '$hashed_pword'";
         $result = $this->db->query($query_str, array($username, $hashed_pword));		 
        return $result->row(0);
    }

    function enc_hash_pwd($pword) { //this function is to generate a hashed pwd
        $hashed_pwd = hash('sha512', $pword . config_item('encryption_key'));
        return $hashed_pwd;
    }

}
