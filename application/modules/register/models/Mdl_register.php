<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Mdl_register extends Mdl_crud {



    protected $_table = "up_register";

    protected $_primary_key = 'id';



    function __construct() {

        parent::__construct();

    }



    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_category'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment']; 
        return $nextId;

    }



    function get_services() {

        $table = $this->_table;
        $this->db->where('status', 'live');
        $this->db->order_by('id','ASC');
        $query = $this->db->get($table);

        return $query;

    }
    function get_services_new($slug) {
        $slug1 = str_replace('_', '-', $slug) ;
        $table = $this->_table;
        $this->db->where('status', 'live');
        $this->db->where('slug !=', $slug1);
        $this->db->order_by('id','ASC');
        $query = $this->db->get($table);

        return $query;

    }



    function get_services_dropdown() {

        $this->db->select('id, title');

        $this->db->order_by('title');

        $dropdowns = $this->db->get('services')->result();

        foreach ($dropdowns as $dropdown) {

            $dropdownlist[$dropdown->id] = $dropdown->title;

        }

        if (empty($dropdownlist)) {

            return NULL;

        }

        $finaldropdown = $dropdownlist;

        return $finaldropdown;

    }



}

