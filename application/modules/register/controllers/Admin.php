<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/register";
    private $MODULE = "register";
    private $group_id;
    private $model_name = 'Mdl_register';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('common_functions');
        $this->load->model('Mdl_register');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('register'); //module name is register here	
    }

    function index() {
     $main_table_params = 'id,email';
     if ($this->input->post('parameters')) {
        $params = json_decode($this->input->post('parameters'));

    } else {
        $params = '';

    }
    $count = $this->Mdl_register->count($params);

    if ($this->input->post('order_by')) {
        $order_by = $this->input->post('order_by');
    } else {
        $order_by = 'id';
    }

    $module_url = base_url() . $this->MODULE_PATH;
    $config = $this->up_pagination->set_pagination_config($count, $module_url);
    $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
    $config['per_page'] = $entries['per_page'];
    $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
    $this->pagination->initialize($config);
    $data['query'] = $this->Mdl_register->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
//        $data['query'] = $this->Mdl_register->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
    $data['permission'] = $this->common_functions->check_permission($this->group_id,$this->MODULE);

    $data['columns'] = array('email');

    $data['page'] = $page;
    $data['total'] = $count;
    $data['total_page'] = ceil($count / $config['per_page']);
    $data['per_page'] = '2';
    // var_dump($data['per_page']);die;
    $data['theUrl'] = $module_url;
        // var_dump($data['theUrl']);die;
//         $data['group_array'] = $this->get_groups();
    $data['page_links'] = $this->pagination->create_links();
    // var_dump($data['page_links']);die;
        // var_dump($data['query']->result());die;
    if ($this->uri->segment(3) == '' && ($params == '')) {

        $data['view_file'] = "admin/table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    } else {
            // var_dump($data['query']->result());die;
        $this->load->view('admin/new_table', $data);
    }
}
}







