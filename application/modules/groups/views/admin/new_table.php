 <div class="widget-content" id="replaceTable"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive"> 
            
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Module Name</th>  
                    <th>Set Permission</th> 
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <td><?php echo Ucfirst($row->title); ?></td>
                        <td>
                            <?php
                            if ($row->id != '1') {
                                echo '<a href="' . base_url() . 'admin/permissions/group/' . $row->id . '"><i class="icon-key"></i>  Set Permission</a>';
                            } else {
                                echo "The Admin group has access to everything";
                            }
                            ?>             
                        </td> 
                        <td class="edit">
                            <?php if ($row->id != '1') { ?>
                                <a href="<?php echo base_url() ?>admin/groups/create/<?php echo $row->id; ?>"><i class="icon-pencil"></i></a>
                            <?php } ?>
                                 <?php if ($row->id != '1') {  ?>
                            <a onclick="delete_permission(<?php echo $row->id;?>)" ><i class="icon-trash " style="cursor: pointer"  ></i></a>
                            <?php } ?>
                        </td>
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table>
        <div id="pagination">
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
                <li><div class="col-sm-2">
                        <input type='text' id="goto" class='form-control' placeholder="Goto Page Number" style='margin-left:-15px;'/>
                    </div>
                </li>
                <li>
                    <div class="col-sm-6">
                        <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div><!--end of class="widget box"-->
<script>
                $('div').on('click', '#pagination a', function (e) {
                e.preventDefault();
                    var theUrl = $(this).attr('href');
        $.ajax({
                type: 'POST',
        data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
            url: theUrl,
            success: function (data) {
            $('#replaceTable').html(data);
            }
            });
        return false;
    });
    $('#goto').on('change', function () {
    var page = parseInt($('#goto').val());
        var base_url = $('.total').attr('b');
        var simUrl = base_url + "/";
        var theUrl = simUrl.concat(page);
            var no_of_pages = parseInt($('.total').attr('a'));
            if (page != 0 && page <= no_of_pages) {
                $.ajax({
                type: 'POST',
                data: 'url=' + theUrl,
                    url: theUrl,
                success: function (data) {
                    $('#replaceTable').html(data);
                    }
                        });
                        } else if (page > no_of_pages) {
                alert('Enter a PAGE between 1 and ' + no_of_pages);
                $('#goto').val("").focus();
                    return false;
        }
                });
                    $('#search').on('keyup', function () {
                searchTable($(this).val());
    });
    function searchTable(inputVal)
    {
        var table = $('#tblData');
        table.find('tr').each(function (index, row)
        {
            var allCells = $(row).find('td');
            if (allCells.length > 0)
            {
                var found = false;
                allCells.each(function (index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if (found == true) {
                    $(row).show();
                }
                else {
                    $(row).hide();
                }
            }
        });
    }
</script>
<script>
    function delete_permission($group_id){
        
                
                $.ajax({
                type: 'POST',
                
              data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","id": $group_id},
            url:"<?php echo base_url(); ?>" + "admin/groups/delete",  
//           alert(url);
            success: function (result){
//                console.log(result);
                if( result == 'true'){
                    alert('Are yo sure, you want to delete it?');
                    location.href = "<?php echo base_url();?>"+"admin/groups";
              
                }
               else{
                   alert('You cannot delete the group');
               }
                
               
                
        
    }
    });
    }
    </script>