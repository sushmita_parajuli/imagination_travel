<div class="manage">
    <input type="button" value="Add Group" id="create" onclick="location.href = '<?php echo base_url() ?>admin/groups/create';"/> 
</div>

<div class="search-field">
    <?php echo form_input('title', '', 'class="form-control searchKeys" data-type="varchar" id="title"  placeholder="Search" onkeypress="return myKeyPress(event)"'); ?>
    <?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>

<div class="widget box" id="replaceTable"> 

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> User Groups </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive"> 
            
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Module Name</th>  
                    <th>Set Permission</th> 
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <td><?php echo Ucfirst($row->title); ?></td>
                        <td>
                            <?php
                            if ($row->id != '1') {
                                echo '<a href="' . base_url() . 'admin/permissions/group/' . $row->id . '"><i class="icon-key"></i>  Set Permission</a>';
                            } else {
                                echo "The Admin group has access to everything";
                            }
                            ?>             
                        </td> 
                        <td class="edit">
                            <?php if ($row->id != '1') { ?>
                            <a href="<?php echo base_url() ?>admin/groups/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a>
                            <?php } ?>
                               <?php if ($row->id != '1') {  ?>
                               &nbsp;&nbsp;/&nbsp;&nbsp; 
                            <a onclick="delete_permission(<?php echo $row->id;?>)" ><i class="icon-trash " style="cursor: pointer"  ></i></a>
                            <?php } ?> 
                        </td>
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table>
        <!-- <div id="pagination">
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
                <li><div class="col-sm-2">
                        <input type='text' id="goto" class='form-control' placeholder="Goto Page Number" style='margin-left:-15px;'/>
                    </div>
                </li>
                <li>
                    <div class="col-sm-6">
                        <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
                    </div>
                </li>
            </ul>
        </div> -->
        <div id="pagination">
            <div class="pagination-input">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="pagination-text">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>

</div>

</div><!--end of class="widget box"-->
<script>
    $('div').on('click', '#pagination a', function (e) {
//        alert('here');
    e.preventDefault();
    var searchParams = {};
    var theUrl = $(this).attr('href');
//    var csrf =$('[name = "csrf_test_name"]').val();
//    alert(theUrl);
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: 'parameters=' + JSON.stringify(searchParams),
            data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
        url: theUrl,
        success: function (data) {
//            alert(data);
            $('#replaceTable').html(data);
        }
    });
    return false;
});

$('thead').click(function () {
    $("#tblData").tablesorter();
});
    $('#goto').on('change', function () {
    var page = $('#goto').val();
    var base_url = $('.total').attr('b');
    var simUrl = base_url + "/";
    var theUrl = simUrl.concat(page);
     var searchParams = {};
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    var no_of_pages = parseInt($('.total').attr('a'));
    if (page != 0 && page <= no_of_pages) {
        $.ajax({
            type: 'POST',
//            data: 'parameters= ' + JSON.stringify(searchParams),
           data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
            url: theUrl,
            success: function (data) {
                $('#replaceTable').html(data);
            }
        });
    }
    else {
        alert('Enter a PAGE between 1 and ' + no_of_pages);
        $('#goto').val("").focus();
        return false;
    }
});
    $('#search').click(function () {
    var searchParams = {};
    var base_url = $('.total').attr('b');
//    var test = $(JSON.stringify(searchParams)
//   var csrf1 = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)},
//    var csrf= $("'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'");
//    var csrf= $("input[name=csrf_test_name]").val();
//    alert(csrf);
//        var post_data = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)};
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
       data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
});
    //$("#downl o adFile").on('click', function() {
    //    downloadFile('test');
    ////   alert ("inside onclick");
    ////   window.location = "http://www.google.com";
    //});
</script>
<script>
    function delete_permission($group_id){
        
                
                $.ajax({
                type: 'POST',
                
              data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","id": $group_id},
            url:"<?php echo base_url(); ?>" + "admin/groups/delete",  
//           alert(url);
            success: function (result){
//                console.log(result);
                if( result == 'true'){
                    alert('Are yo sure, you want to delete it?');
                    location.href = "<?php echo base_url();?>"+"admin/groups";
              
                }
               else{
                   alert('You cannot delete the group');
               }
                
               
                
        
    }
    });
    }
    </script>
    <script type="text/javascript">
  function myKeyPress(e){
    var code= (e.keyCode ? e.keyCode : e.which);
     var searchParams = {};
    var base_url = $('.total').attr('b');
    if(code == 13) { //Enter keycode
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
       data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
}

  }
</script>
    