<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_groups extends Mdl_crud {

    protected $_table = "up_users_groups";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

//    function get($order_by) {
//        $table = $this->get_table();
//        $this->db->order_by($order_by, 'ASC');
//        $query = $this->db->get($table);
//        return $query;
//    }
//
    function get_where($id) {
//        die('here');
        $table = $this->_table;
        
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
	//die('here');      
  $next_id = $this->get_id();
//var_dump($next_id);die;

        $table = $this->_table;
        $this->db->insert($table, $data);


        $insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
        $permission_table = 'up_permissions';
        $this->db->insert($permission_table, $insert_null_permission_array);
    }

    function get_id() { 
        //$conn = mysqli_connect('upvedatech.com','upvedate_saarc','saarc','upvedate_saarc') or die('not connected');
        //$result = mysqli_query($conn,"SHOW TABLE STATUS LIKE 'up_users_groups'");
//        var_dump($result);die;
	$this->db->select("*");
	$this->db->from('up_users_groups');
	$this->db->order_by("id","DESC");
	$result=$this->db->get($table);
	$nextId=$result+1;	
       // $row = mysqli_fetch_array($result);
        //$nextId = $row['Auto_increment'];
        return $nextId;
    }
function count_users($id){
//    $this->db->select("*");
//    $this->db->from('up_users');
    $this->db->where('up_users.group_id',$id);
    $var = $this->db->get('up_users')->result();
    return count($var); 
    
}
//    function _update($id, $data) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->update($table, $data);
//    }

    function get_modules_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('up_modules')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_groups_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by($this->_primary_key, 'DESC');
        $dropdowns = $this->db->get('up_users_groups')->result();
         $dropdownlist[0] = '--Select group--';
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

}
