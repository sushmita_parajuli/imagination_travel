<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/groups";

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->model('Mdl_groups');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('groups'); //module name is groups here	
    }

    function index() {
//        die('here');
        $main_table_params = 'id,title';

        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
        }
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        $count = $this->Mdl_groups->count();

//        var_dump($count);die('here');
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $this->load->model('Mdl_groups');
        $data['query'] = $this->Mdl_groups->get_where_dynamic('', $main_table_params, $order_by, ($page - 1) * $config['per_page'], $config['per_page'], $params);
        // $data['query'] = $this->Mdl_groups->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
//        var_dump($data['query']);die;

        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && (!$this->input->post('per_page')) && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    // function index() {
    //     $count = $this->Mdl_groups->count();
    //     $module_url = base_url() . $this->MODULE_PATH;
    //     $config = $this->up_pagination->set_pagination_config($count, $module_url);
    //     $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
    //     $this->pagination->initialize($config);
    //     $data['query'] = $this->Mdl_groups->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
    //     $data['page'] = $page;
    //     $data['total'] = $count;
    //     $data['total_page'] = ceil($count / $config['per_page']);
    //     $data['per_page'] = $config['per_page'];
    //     $data['theUrl'] = $module_url;
    //     $data['page_links'] = $this->pagination->create_links();
    //     if ($this->uri->segment(3) == '') {
    //         $data['view_file'] = "admin/table";
    //         $this->load->module('template/admin_template');
    //         $this->admin_template->admin($data);
    //     } else {
    //         $this->load->view('admin/new_table', $data);
    //     }
    // }

    function get_data_from_post() {
        $data['title'] = $this->input->post('title', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        return $data;
    }

    function get_data_from_db($update_id) {

        $query = $this->Mdl_groups->get_where($update_id);
//        var_dump($query);die;
        foreach ($query->result() as $row) {
            $data['title'] = $row->title;
        }

        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }

    function create() {
//        die('here');
        $update_id = base64_decode( $this->uri->segment(4));
//         var_dump($update_id);die;
        if ($update_id != 1) {
            $submit = $this->input->post('submit', TRUE);
            if ($submit == "Submit") {
                //person has submitted the form
                $data = $this->get_data_from_post();
            } else {
                if (is_numeric($update_id)) {
//                     die('------------+-');
                    $data = $this->get_data_from_db($update_id);
//                    var_dump($data);die;
                    // var_dump($data);
                }
            }

            if (!isset($data)) {
                $data = $this->get_data_from_post();
            }

            $data['update_id'] = $update_id;
            $data['view_file'] = "admin/form";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            redirect('admin/groups');
        }
    }

    function submit() {

        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean|is_unique[up_users_groups.title]'); //unique_validation check while creating new
        }
        /* end of validation rule */


        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();
            //var_dump($data);die;
            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $this->Mdl_groups->_update($update_id, $data);
            } else {

                $this->Mdl_groups->_insert($data);
            }
            //die('hello');
            redirect('admin/groups');
        }
    }

//	function get($order_by){
//	$this->load->model('mdl_groups');
//	$query = $this->mdl_groups->get($order_by);
//	return $query;
//	}
//	
//	function get_where($id){
//	$this->load->model('mdl_groups');
//	$query = $this->mdl_groups->get_where($id);
//	return $query;
//	}
//    function _insert($data) {
//        $this->load->model('mdl_groups');
//        $this->mdl_groups->_insert($data);
//    }
//	function _update($id, $data){
//	$this->load->model('mdl_groups');
//	$this->mdl_groups->_update($id, $data);
//	}
    function delete() {
        $delete_id = $this->input->post('id');
//        $delete_id = base64_decode($this->uri->segment(4));
//        var_dump($delete_id);die;
        $query = $this->Mdl_groups->count_users($delete_id);
        if($query == 0){
            
       
       
//        
//        if (!isset($delete_id) || !is_numeric($delete_id)) {
////            var_dump('test');die;
//            unset($delete_id);
//            redirect('admin/groups');
//        } else {
            $this->Mdl_groups->_delete($delete_id);
            echo 'true';
//            $url = 'groups';
            
//            echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
            //  redirect('admin/former_saarc_director');
//        }
    }
    else {
        echo 'false';
    }
//    redirect('admin/groups');
    }

}
