
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce_4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript"> 
     
    tinymce.init({
      selector: "textarea",
     
      // ===========================================
      // INCLUDE THE PLUGIN
      // ===========================================
    
      plugins: [
          
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages",
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools",
        
        
      ],
      
    	
      // ===========================================
      // PUT PLUGIN'S BUTTON on the toolbar
      // ===========================================
   toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | image",
//        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
   toolbar2: 'print preview media | forecolor backcolor emoticons ',
   imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
   image_advtab: true,
    // ===========================================
     // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
    // ===========================================
    relative_urls: false,
   templates: [
   { title: 'Test template 1', content: 'Test 1' },
   { title: 'Test template 2', content: 'Test 2' }
   ],
   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    '//www.tinymce.com/css/codepen.min.css'
   ]  	
    });
     
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/css/datepicker.css' ?>" />
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/less/datepicker.less' ?>" />  
<script src="<?php echo base_url() . 'bootstrap/datepicker/js/bootstrap-datepicker.js' ?>"></script>
<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i>Announcement</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/announcement/submit', 'class="form-horizontal row-border" id="validate-1" onsubmit ="multipleSelectOnSubmit()"');				
				?>                
                    
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('title', $title, 'class="form-control required"');?>
						</div> 
                    </div>
               
           <div class="form-group"> 
                    <label class="col-md-2 control-label">Description</label> 
                    <div class="col-md-10">
                        <?php echo form_textarea(array('id' => 'elm1', 'name' => 'description', 'value' => $description, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
                    </div> 
                </div>
               
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                                'live'    => 'live',
                                'draft'  => 'draft',
                              
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div> 
                    
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
<script>
    $('#datepicker').datepicker();

    $("#datepicker").on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    </script>
                