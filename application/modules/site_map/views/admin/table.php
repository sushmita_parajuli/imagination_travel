<div class="manage">
    <input type="button" value="Add Announcement" id="create" onclick="location.href='<?php echo base_url()?>admin/announcement/create';"/> 
</div>
<div class="col-sm-2">
    <?php echo form_input('title', '', 'class="form-control searchKeys" data-type="varchar" id="title" placeholder="Search"'); ?>
</div>
<div class="col-sm-2">
    <?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>
<div class="widget box" id="replaceTable"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i>Announcement</h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th>
                    <th data-class="expand">Description</th>
                   
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = ($page - 1) * $per_page + 1; ?>
               <?php 
//               var_dump($query->result());die('dfdf');
               foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->title;?></td>
                    <td><?php echo $row->description;?></td>
                  
                    <td class="edit">
                            <?php if (isset($permission['edit'])) { ?><a href="<?php echo base_url() ?>admin/announcement/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a>
                            <?php } if (isset($permission['delete'])) { ?>&nbsp;&nbsp;/&nbsp;&nbsp; 
                                <a href="<?php echo base_url() ?>admin/announcement/delete/<?php echo base64_encode($row->id); ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
                            <?php } ?>
                        </td> 
                </tr> 
               		<?php }	?>                
            </tbody> 
        </table> 
        <div id="pagination">
            <div class="col-sm-2">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="col-sm-6" style="margin-left: 30px;">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>
</div><!--end of class="widget box"-->
<script>
    $('div').on('click', '#pagination a', function (e) {
//        alert('here');
    e.preventDefault();
    var searchParams = {};
    var theUrl = $(this).attr('href');
//    var csrf =$('[name = "csrf_test_name"]').val();
//    alert(theUrl);
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: 'parameters=' + JSON.stringify(searchParams),
            data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
        url: theUrl,
        success: function (data) {
//            alert(data);
            $('#replaceTable').html(data);
        }
    });
    return false;
});

$('thead').click(function () {
    $("#tblData").tablesorter();
});
    $('#goto').on('change', function () {
    var page = $('#goto').val();
    var base_url = $('.total').attr('b');
    var simUrl = base_url + "/";
    var theUrl = simUrl.concat(page);
     var searchParams = {};
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    var no_of_pages = parseInt($('.total').attr('a'));
    if (page != 0 && page <= no_of_pages) {
        $.ajax({
            type: 'POST',
//            data: 'parameters= ' + JSON.stringify(searchParams),
           data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
            url: theUrl,
            success: function (data) {
                $('#replaceTable').html(data);
            }
        });
    }
    else {
        alert('Enter a PAGE between 1 and ' + no_of_pages);
        $('#goto').val("").focus();
        return false;
    }
});
    $('#search').click(function () {
    var searchParams = {};
    var base_url = $('.total').attr('b');
//    var test = $(JSON.stringify(searchParams)
//   var csrf1 = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)},
//    var csrf= $("'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'");
//    var csrf= $("input[name=csrf_test_name]").val();
//    alert(csrf);
//        var post_data = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>','parameters= ': JSON.stringify(searchParams)};
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
//        data: csrf ,'parameters= ' + JSON.stringify(searchParams),
       data:{"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>","parameters": JSON.stringify(searchParams)},
//       data: csrf1,
//            data: post_data,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
});
    //$("#downl o adFile").on('click', function() {
    //    downloadFile('test');
    ////   alert ("inside onclick");
    ////   window.location = "http://www.google.com";
    //});
</script>