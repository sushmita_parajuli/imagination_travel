<!--

  <a href="<?php echo base_url();?>scholarship/details"> Scholarship </a><br>
  
  <a href="<?php echo base_url();?>vacancy/details"> Vacancy </a><br>
  
  <a href="<?php echo base_url();?>tenders/details"> Tenders </a><br>
  
  <a href="<?php echo base_url();?>awards"> Awards </a><br>-->

<div class="tabs flex-tabs">
<!--                  <label for="tab-one" id="tab-one-label" class="tab">FEATURED STORIES </label>
                  <label for="tab-two" id="tab-two-label" class="tab">ANNOUCEMENTS</label>
                  <label for="tab-three" id="tab-three-label" class="tab">VACANCIES </label>
                  <label for="tab-four" id="tab-four-label" class="tab">PUBLICATIONS</label>-->
                  <div id="tab-one-panel" class="panel active">
                    
                      <div class="clearfix"></div>
                     
                    </div>
                    <div id="tab-two-panel" class="panel">

                      <div id="ChildVerticalTab_1">
                        <ul class="resp-tabs-list ver_1">
                          <?php foreach($announcement as $row){?>
                          <li><?php echo $row['title'] ;?></li>
                          <?php }?>
                        </ul>
                        <div class="resp-tabs-container ver_1">
                          <div>

                           <div class="table-responsive">
                            <table class="table table-design">
                              <thead>
                                <tr>
                                  <th>Posted Date</th>
                                  <th>Title</th>
                                  <th>Deadline  </th>
                                  <th>Terms of Reference</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach($scholarship as $row){?>
                                <tr>
                                  <td><?php echo $row['posted_date']?></td>
                                  <td><span class="table-span"><?php echo $row['title']?></span></td>
                                  <td><?php echo $row['deadline']?></td>
                                  <td><span class="table-span">  <a href="<?php echo base_url();?>download/scholarship/<?php echo $row['attachment'];?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                  <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                </tr>
                                <?php }?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div>
                          <div class="table-responsive">
                            <table class="table table-design">
                              <thead>
                                <tr>
                                  <th>Posted Date</th>
                                  <th>Job Title</th>
                                  <th>Level</th>
                                  <th>Duty Station</th>
                                  <th>Application Deadline  </th>
                                  <th>Terms of Reference</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach($vacancy as $row){?>
                                <tr>
                                  <td><?php echo $row['posted_date']?></td>
                                  <td><span class="table-span"><?php echo $row['title']?></span></td>
                                  <td><?php echo $row['level']?></td>
                                  <td><?php echo $row['duty_station']?></td>
                                  <td><?php echo $row['deadline']?></td>
                                  <td><span class="table-span">  <a href="<?php echo base_url();?>download/vacancy/<?php echo $row['attachment'];?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                  <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                </tr>
                                <?php }?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div>
                          <div class="table-responsive">
                            <table class="table table-design">
                              <thead>
                                <tr>
                                  <th>Posted Date</th>
                                  <th>Title</th>
                                  <th>Deadline  </th>
                                  <th>Terms of Reference</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach($tenders as $row){?>
                                <tr>
                                  <td><?php echo $row['posted_date']?></td>
                                  <td><span class="table-span"><?php echo $row['title']?></span></td>

                                  <td><?php echo $row['deadline']?></td>
                                  <td><span class="table-span">  <a href="<?php echo base_url();?>download/tenders/<?php echo $row['attachment'];?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                  <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                </tr>
                                <?php }?>
                              </tbody>
                            </table>
                          </div>
                          <!--<p>Suspendisse blandit velit Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravid urna gravid eget erat suscipit in malesuada odio venenatis.</p>-->
                        </div>
                        <div>
                          <ul class="list-group list-group-design">
                            <?php foreach($awards as $row){?>
                             <li class="list-group-item"><a href="<?php echo base_url();?>awards/details/<?php echo $row['slug']?>"><?php echo $row['title'];?></a></li>
                            <!-- <li class="list-group-item"><a href="#"><?php echo $row['title'];?></a></li> -->
                            <?php }?>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>
                  
                 
                </div>