<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_map extends MX_Controller
{

	function __construct() {
	$this->load->model('navigation/mdl_navigation');
	parent::__construct();
	}

	function index(){
//                       echo'here';die;
        $data['view_file']="sitemap";
      
        $this->load->module('template');
        $this->template->front($data);	
	}
         function get_parent($group_id) {
        $this->load->model('navigation/Mdl_navigation');
        $query = $this->Mdl_navigation->get_parentnav_for_frontend($group_id);
        
        $result = $this->add_href($query);
//        var_dump($result);die;
        return $result;
    }
    
     function get_child($group_id, $parent_id, $language_id) {
        $this->load->model('navigation/Mdl_navigation');
        $query = $this->Mdl_navigation->get_childnav_for_frontend($group_id, $parent_id);
        $result = $this->add_href($query);
        return $result;
    }
        
       
          function get_header($group_id,$language_id) {
        $data['parentnav'] = $this->get_parent($group_id); //
//                var_dump($data['parentnav']);die;
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
            $children = $this->get_child($group_id, $nav['id']);
            if ($children != NULL) {
                $nav['children'] = $children;
            }
            $navigation[$i] = $nav;
            $i++;
        }
        return $navigation;
    }
     function get_name_from_page($page_id) {
        $this->load->model('template/mdl_template');
        $query = $this->mdl_template->get_name_from_page($page_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }
    function add_href($result) {
        $count = count($result);
        for ($i = 0; $i < $count; $i++) {
            if ($result[$i]['navtype'] == 'Module') {
                $result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'Page') {
                $result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'URI') {
                $result[$i]['href'] = $result[$i]['site_uri'];
                $result[$i]['target'] = "_self";
            } else {
                $result[$i]['href'] = $result[$i]['link_url'];
                $result[$i]['target'] = "_blank";
            }
        }
        return $result;
    }
    
    function get_footer($group_id,$language_id) {
        
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_footernav($group_id);
        $result = $this->add_href($query->result_array());
        return $result;
    }
        function get_data_from_post()
	{
           
		$data['first_name'] = $this->input->post('first_name', TRUE);
                $data['last_name'] = $this->input->post('last_name', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['msg'] = $this->input->post('msg', TRUE);
		$data['slug'] = strtolower(url_title($data['first_name']));		
		$data['mailsubject'] = $this->input->post('mailsubject', TRUE);
//                $data['mailbody'] = $this->input->post('emailbody', TRUE);
//		$data['ent_date'] = date("Y-m-d");
//		$data['upd_date'] = NULL;
               
		return $data;
	}
        function create($status)
        {
          
		 /* for recaptcha*/
            if(!isset($status))
            {
            $status='';
            }
            $data =  $this->get_data_from_post();
            $data['message']=$status;
                 $this->load->library('recaptcha');
                 $data['recaptcha_html']=$this->recaptcha->recaptcha_get_html();
                /*end of recaptcha*/
		$data['view_file']="front";
                //$data['program_array'] = $this->get_program_if_live();
		$this->load->module('template');
		$this->template->front($data);	
	}
        function submit()
	{
         
	$this->load->library('recaptcha');
	$this->load->library('form_validation');
        $this->recaptcha->recaptcha_check_answer();
        if($this->recaptcha->getIsValid()){
       
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
                $this->form_validation->set_rules('mailsubject', 'Subject', 'required|xss_clean');
                $this->form_validation->set_rules('msg', 'Msg','required|xss_clean');
		/*end of validation rule*/
        }	
		
		if ($this->form_validation->run($this) == FALSE)
		{
                    
                    $this->create('1');
		}
		else
		{
			$data = $this->get_data_from_post();
                        $this->send_email($data);
                        $this->_insert($data);
                        $this->create('0');
                        //redirect(base_url().'contactus');
		}
			
			//redirect($this->index());
                        
        }
        function send_email($data){
            $first_name = $data['first_name'] ;
            $last_name = $data['last_name'] ;
            $email = $data['email'];
            $msg = $data['msg'];
            $subject = $data['mailsubject'];
//            $emailbody ='Services ='.$data['programs'].'<br>'.$data['mailbody'] ;
//            $emailbody = $data['mailbody'] ;
            $emailbody = ' First Name = '.$data['first_name'].'<br> Last Name = '.$data['last_name'].'<br> Email = '.$data['email'].'<br> Subject = '.$data['mailsubject'].'<br> Message = '.$data['msg'];
        

                        
                        $this->load->library('email');
                        $config['protocol'] = 'sendmail';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['charset'] = 'iso-8859-1';
                        $config['wordwrap'] = TRUE;
                        $config['mailtype'] = 'html';
                        
                        $this->email->initialize($config);
                        $this->email->from($email);
                        $this->email->to('budhathokijeena25@gmail.com');
                        
//                        $this->email->cc($emailfrom);
                        //$this->email->cc('them@their-example.com');
//$this->email->subject('INSEC SAM'."'".'S:'.$msg_from_name.':status:'.date('y-m-d',$data['ent_date']));
                        $this->email->subject('Gulf'.':'.$subject);
                        $this->email->message('<div style="color:#003366">'.$emailbody.'<div>');
                        
//                        $this->email->subject('INSEC SAM'."'".'S:'.$name.':status:'.date('y-m-d',$data['ent_date']));
//                        $this->email->message($emailbody.'<div style="color:red"><div>');
                        if (!$this->email->send())
                                {
                                   show_error($this->email->print_debugger());
                                } 
        }
        
        function get($order_by){
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_delete($id);
	}
        function generation_sitemap(){	
        $data['view_file']="sitemap";
        $this->load->module('template');
        $this->template->front($data);	
	}
        
	
}