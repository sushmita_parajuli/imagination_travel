<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{     
    private $MODULE_PATH = "admin/announcement";
    private $group_id;
    private $model_name = 'Mdl_announcement';
    private $MODULE = 'announcement';

	public function __construct()
	{
        parent::__construct();
        date_default_timezone_set('Asia/Kathmandu');
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('Mdl_announcement');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('announcement'); //module name is announcement here	
    }	
    
     function index() {
        $main_table_params = 'id,title,description';
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
           
            $count = $this->Mdl_announcement->count($params);
        } else {
            $params = '';
        }
        $count = $this->Mdl_announcement->count($params);
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_announcement->get_where_dynamic('', $main_table_params, $order_by, ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        $data['column'] = 'modules';
        $data['title'] = 'title';
        $data['slug'] = 'slug';
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }
	
    function get_data_from_post() 
    {
        $data['title'] = $this->input->post('title', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));
        $update_id = $this->input->post('update_id', TRUE);
       if (is_numeric($update_id)) {
//            $attach = $this->get_attachment_from_db($update_id);
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = date("Y-m-d");;
        }   
//        var_dump($data);die;
        return $data;
    }

	function get_data_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['title'] = $row->title;
             $data['description'] = $row->description;
            $data['status'] = $row->status;

            $data['published_date'] = $row->published_date;
        }
            $data['query_clone'] = $this->get_where($update_id)->result();
           
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }
	
	
    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            $data = $this->get_data_from_post();
        } else {
           if (is_numeric($update_id)){
                                $select = 'title,description,status';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
			}
        }
        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
//            var_dump('test');die;
            unset($delete_id);
            redirect('admin/announcement');
        } else {
            $this->Mdl_announcement->_delete($delete_id);
            $url = 'announcement';
            echo'<script>window.location.href = "' . base_url() . 'admin/' . $url . '";</script>';
            //  redirect('admin/announcement');
        }
    }
	    function submit() {
//        var_dump($_POST);die;
        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean|is_unique[up_announcement.title]|is_unique[up_pages.title]'); //unique_validation check while creating new
        }
        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();
            $update_id = $this->input->post('update_id', TRUE);
//            var_dump($data);die;
            if (is_numeric($update_id)) {
//                    var_dump($update_id);die;
              
                $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
                if (isset($permission['edit'])) {
                    $this->Mdl_announcement->_update($update_id, $data);
                    $this->session->set_flashdata('operation', 'Updated Successfully!!!');
                }
//				$this->_update($update_id, $data);
            } else {
                $nextid = $this->Mdl_announcement->get_max();
                
                $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
                if (isset($permission['add'])) {
                    $this->Mdl_announcement->_insert($data);
                    $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
                }
            }
            $url = 'announcement';
            echo'
<script>
window.location.href = "' . base_url() . 'admin/' . $url . '";
</script>
';
//            redirect('/admin/announcement', 'location', 301);
            //redirect('admin/announcement');
        }
    }
		

	function get_id(){
	$this->load->model('mdl_announcement');
	$id = $this->mdl_announcement->get_id();
	return $id;
	}
        
	function get($order_by){
	$this->load->model('mdl_announcement');
	$query = $this->mdl_announcement->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_announcement');
	$query = $this->mdl_announcement->get_where($id);
	return $query;
	}
         function get_attachment_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['attachment'] = $row->attachment;
        }
        return $data;
    }
}