<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_template extends Mdl_crud {

    protected $_table = "table_name";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_parentnav($group_id) { //header nav group is always 1
        $this->db->where('parent_id', '0');
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('up_navigation');
        return $query;
    }

    function get_childnav($group_id) { //header nav group is always 1
        $this->db->where('parent_id !=', '0');
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('up_navigation');
        return $query;
    }

    function get_footernav($group_id) { //footer nav group is always 2
        $this->db->where('parent_id', '0'); //because we don't want child navigation in footer nav
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('up_navigation');
        return $query;
    }

    /* none of below functions are used actually */


    function get_name_from_module($module_id) {
        $this->db->select('slug');
        $this->db->where($this->_primary_key, $module_id);
        $query = $this->db->get('up_modules');
        return $query->result_array();
    }

    function get_name_from_page($page_id) {
        $this->db->select('slug');
        $this->db->where($this->_primary_key, $page_id);
        $query = $this->db->get('up_pages');
        return $query->result_array();
    }

    function get_metadata_search_keys($module_name, $slug) {
        $table = 'up_' . $module_name;
//            var_dump($table); die('dk');
        $this->db->where('slug', $slug);
        $query = $this->db->get($table);
        return $query;
    }

    function get_metadata_search_module($module_name) {
        $table = 'up_' . $module_name;
//            var_dump($table); die('dk');
//            $this->db->where('slug', $slug);
        $this->db->limit(1, 0);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function insert($data){
        $table = 'up_register';
        $this->db->insert($table, $data);

    }

}
