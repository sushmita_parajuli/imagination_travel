<!doctype html>
<html lang="en">
<head>
    <!-- meta tags and title goes here -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <title>Imagination</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $favicon; ?>">
    <!-- Mobile Specific Metas here -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <!-- CSS & google font link goes here -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/bootstrap.min.css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai|Lato" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/dist/css/owl.carousel.css">
    <!-- <link rel="stylesheet" href="dist/css/simplelightbox.min.css"> -->
    <!-- <link rel="stylesheet" href="dist/css/heroslider.css"> -->

    <!-- custom css added -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/scss/style.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/custom.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/responsive.css" media="all">

    <!-- Favicons goes here -->
    <!-- <link rel="shortcut icon" href="image/favicon.ico"> -->
    
    <!-- js plugins goes here -->
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/modernizr.js"></script>
    <!--[if lt IE 9]>
        <script src="dist/js/html5shiv.js"></script>
        <![endif]-->
    </head>

    <body>
    <!-- Page structure start 
    ================================================== -->
    <div class="wrapper">
        <a href="#" class="TopButton">
            <i class="fa fa-angle-up "></i>
        </a>
        <header class="header">

            <nav class="navbar navbar-default">
                <div class="info">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <span>
                                    <i class="fa fa-phone" aria-hidden="true"></i> 9860123456
                                </span>
                                <span>
                                    <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@travel.com</a>
                                </span>
                                <span class="pull-right"><a href="#"><span class="fa fa-twitter"></span></a></span>
                                <span class="pull-right"><a href="#"><span class="fa fa-facebook"></span></a></span>
                                <span class="pull-right"><a href="#"><span class="fa fa-search"></span></a></span>
                                
                            </div>
                        <!--    <div class="col-md-6 pull-right">
                                <span>
                                <i class="fa fa-phone" aria-hidden="true"></i> log In
                                </span>
                                <span>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> Register
                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="container">

                    <div class="logo-wrap">
                        <h1 class="logo clearfix">
                           <a href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url(); ?>uploads/settings/logo.png" alt="SAARC-logo" class="img-responsive">
                        </a>  
                    </h1>

                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse main-nav">
                    <ul class="nav navbar-nav navbar-right">
                       <?php foreach ($headernav as $header) {//still need to work on slug  ?> 
                       <?php
                       if (empty($header['children'])) {
                        echo '<li>'; /* this is for no-child */
                    } else {
                        ?>                      
                        <li  class="dropdown">
                            <?php }/* this is for dropdown */ ?>

                            <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                            <?php
                            if ($header['navtype'] == 'URL') {
                                $prefix = '';
                            } else {
                                $prefix = base_url();
                            }
                            ?>
                            <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?>                                  
                                <!-- <b class="caret"></b>                                 -->
                                <?php }/* end of if */ else { /* this is for no-child */
                                    ?>                            
                                    <?php
                                    if ($header['navtype'] == 'URL') {
                                        $prefix = '';
                                    } else {
                                        $prefix = base_url();
                                    }
                                    ?>
                                    <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
                                        <?php } ?>                           
                                    </a>

                                    <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                    <ul class="dropdown-menu">
                                        <?php foreach ($header['children'] as $child) { ?>
                                        <!--code for grandchild by harry--> 
                                        <?php if (!empty($child['grand_children'])) { ?>
                                        <li class="dropdown dropdown-submenu">                                            
                                            <?php
                                            if ($child['navtype'] == 'URL') {
                                                $prefix = '';
                                            } else {
                                                $prefix = base_url();
                                            }
                                            ?>
                                            <a class="dropdown-toggle" href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>

                                            <ul class="dropdown-menu" style="background:#fcfcfc;" padding:10px;>
                                                <?php foreach ($child['grand_children'] as $grandchild) { ?>
                                                <li >                                            
                                                    <?php
                                                    if ($grandchild['navtype'] == 'URL') {
                                                        $prefix = '';
                                                    } else {
                                                        $prefix = base_url();
                                                    }
                                                    ?>
                                                    <a href="<?php echo $prefix . $grandchild['href']; ?>" target="<?php echo $grandchild['target'] ?>"><?php echo $grandchild['title']; ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                        <!--end  of code for grandchild by harry--> 
                                        <?php } else { ?> <!--this else part if grand child doesnot exist-->
                                        <li>                                            
                                            <?php
                                            if ($child['navtype'] == 'URL') {
                                                $prefix = '';
                                            } else {
                                                $prefix = base_url();
                                            }
                                            ?>
                                            <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                        </li>
                                        <?php }/* closing of else part if grand child doesnot exist */ ?>
                                        <?php }/* end of child foreach */ ?>
                                    </ul>
                                    <?php } ?>  

                                </li>
                                <?php }/* end of parent foreach */ ?>
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </div>
                </nav>
            </header>
            <?php
            $first_bit = $this->uri->segment(1);
            if ($first_bit == "" || $first_bit == "home") {
                ?>

                <div class="fluid_container">
                    <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
                       <?php $active = 'active'; ?>
                       <?php foreach ($banners as $row) { ?> 
                       <div data-src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>">
                        <img src="" alt="">
                        <div class="camera_caption fadeFromBottom">
                            <?php echo $row['title'];?>
                            <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. -->
                        </div>
                    </div>
                    <?php $active = '';
                } ?>
            </div><!-- #camera_wrap_1 -->
        </div>
        <div class="clearfix"></div>
        <!-- welcome -->
        <div class="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 welcome-txt">
<!--                         <?php foreach ($about as $row){?>
                        <h2><?php echo $row['title'];?> </h2>
                        <p><?php echo $row['description'];?></p>
<<<<<<< HEAD
                        <a href="<?php echo base_url()?>about_us" class="btn btn-success">Read More</a>
                        <?php  }?> -->
                        <?php foreach ($welcome as $row){?>
                        <h2><?php echo $row['title'];?> </h2>
                        <p><?php echo $row['description'];?></p>
                        <a href="<?php echo base_url()?>imagination-travel-and-trek" class="btn btn-success">Read More</a>
=======
                        <a href="<?php echo base_url()?>about-us" class="btn btn-success">Read More</a>
>>>>>>> aad161f9386b535049999ef66234bea95bed8db5
                        <?php  }?>
                        <!-- <h2>Imagination Travel and Trek</h2> -->
                        

                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis voluptatem quisquam provident enim voluptate aspernatur possimus animi incidunt cum alias molestias unde rem, magni neque ullam nam ut, quasi deleniti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, animi beatae fugiat est impedit nostrum quas rem vero! Delectus blanditiis aspernatur optio veritatis repellat cum quo quidem sed iste culpa.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem dicta non error id hic et recusandae. Repellat velit repellendus optio distinctio, sed, dignissimos quidem dolor reiciendis voluptates corrupti explicabo, ipsam!</p>
                        <a href="<?php echo base_url()?>about_us" class="btn btn-success">Read More</a>
                    --></div>
                </div>
            </div>
        </div>
        <!-- service -->
        <div class="service">
            <div class="container">
                <div class="row">
                    <h2 class="text-center">Our service</h2>
                    <?php foreach ($services as $row) {?>
                    <div class="col-sm-4">
                        <div class="box">
                            <?php  $title =strtolower($row['title']);
                            $title1 = str_replace(' ', '_', $title);?>
                            <a href="<?php echo base_url()?><?php echo $title1; ?>">
                                <figure>
                                    <img src="<?php echo base_url(); ?>uploads/services/<?php echo $row['attachment'] ?>" class="img-responsive"/>
                                    <figcaption><?php echo $row['title'] ?></figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/delux.jpg" class="img-responsive"/>
                                    <figcaption>Hotel Booking</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/img10.jpg" class="img-responsive"/>
                                    <figcaption>Treaking</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/1132.jpg" class="img-responsive"/>
                                    <figcaption>Mountain Flight</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                   <!--  <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/delux.jpg" class="img-responsive"/>
                                    <figcaption>Sight Seeing</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/img10.jpg" class="img-responsive"/>
                                    <figcaption>Transport</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="col-sm-4">
                        <div class="box">
                            <a href="booking.php">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/service/1132.jpg" class="img-responsive"/>
                                    <figcaption>Ticketing</figcaption>
                                </figure>
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- testimonial -->
        <div class="testimonial">
            <div class="bg">
                <div class="container">
                    <h2 class="text-center">Testimonial</h2>
                    <div class="testimonial-list">
                        <div class="row">
                            <div class="col-md-4">
                                <figure>
                                    <img src="<?php echo base_url(); ?>design/frontend/image/testimonials/img04.jpg" class="img-responsive">
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        <span>- Joe Dhoe</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <figure>
                                            <img src="<?php echo base_url(); ?>design/frontend/image/testimonials/img04.jpg" class="img-responsive">
                                        </figure>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                <span>- Joe Dhoe</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } else {
                            ?>
                            <!-- start of the content ------------------------------------------>
                            <div id="content">
                                <?php
                                if (isset($click)) {


                                    $slug = $this->uri->segment(1);
                                    // var_dump($slug);die;
                                    ?>   

                                    <div class="container" style="min-height:600px;">
                                        <div class="row">
                                            <div class="col-sm-8 col-lg-9">
                                                <div class="breadcrumb_div hidden-xs">  
                                                    <ul id="breadcrumbs" class="breadcrumb">  

                                                        <li>  
                                                            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                                                <span class="arrow"></span></a>

                                                            </li>
                                                            <?php
                                //making bread crumbs

                                                            if ($this->uri->segment(1) != '') {
//                        
                                                                $breadcrumb_url = $this->uri->segment(1);
                                                                $breadcrumb_title = ucfirst($title);
                                                                ?>  

                                                                <?php 
                                                            }
                                                            ?>

                                                            <?php if ($this->uri->segment(2)) {
//                         
                                                                ?>
                                                                <li class="current"> 
                                                                    <a class="odd"><?php echo ucfirst($this->uri->segment(2)) ?>
                                                                    </a>
                                                                </li>   
                                                                <?php }//end of breadcrumbs ?>


                                                            </ul>  
                                                        </div>
                                                        <section class="inner-section">
                                <!--                             <h2 class="section-header">
                                                                <?php
                                                                if ($this->uri->segment(3)) {
//                         
                                                                    $title_new = $this->uri->segment(3);
                                                                    $title = urldecode($title_new);
                                                                } else {
//                          die('here');
                                                                    $title = $this->uri->segment(1);
//                        var_dump($title);die;
                                                                }
                                                                if (strpos($title, '_') != false) {
//                          die('here');
                                                                    $title_array = explode('_', $title);
                                                                } else {
                                                                    $title_array = explode('-', $title);
                                                                }
//                      echo'here';
                                                                echo $breadcrumb_title;
//                      foreach ($title_array as $tarray) {
//                          
//                        echo ucwords(strtolower($tarray));
//                        echo ' ';
//                      }
                                //   echo $title_echo;
                                                                ?>
                                                            </h2> -->

                                                            <div class="inner-description" >
                                                                <?php
                                                                if (!isset($view_file)) {
                                                                    $view_file = "";
                                                                }
                                                                if (!isset($module)) {
                                                                    $module = $this->uri->segment(1);
                                                                }
                                                                if (($view_file != '') && ($module != '')) {
                                                                    $path = $module . "/" . $view_file;
                                                                    $this->load->view($path);
                                                                } else {
                                                                    ?>
                                                                    
                                                                    <div style="text-align: justify" class="row">
                                                                        <div class="col-sm-8 welcome-txt">
                                                                            <?php 

                                    $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                    echo ($new_description); //put your designs here
                                    ?>  </div>
                                    <div class="col-sm-4 image">
                                       <?php if (($option) == 'yes' && $attachment != '') { ?>
                                       <figure>
                                        <img src="<?php echo base_url(); ?>uploads/pages/<?php echo $attachment ?>" style="width:40%;float:left; padding: 2px 4px 4px 0;">
                                    </figure>
                                    <?php }
                                    ?>
                                </div>   

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
</div>
<?php } ?>
<?php if (!isset($click)) {
    ?>  
    <div class="content-wrap inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <section class="inner-section">
                        <div class="breadcrumb_div hidden-xs">  
                            <ul id="breadcrumbs" class="breadcrumb">  
                                <li>  
                                    <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                        <span class="arrow"></span></a>

                                    </li>

                                    //making bread crumbs

                                    <?php if ($this->uri->segment(3)) {
                                        ?>
                                        <li class="current"> 
                                            <a><?php
                                            $new_title = $this->uri->segment(3);

                                            $test[] = explode('_', $new_title);
                                            if(isset($details)){
                                             if(sizeof($details)>0){
                                                echo word_limiter($details[0]["title"],10);
                                            }
                                        }else{
                                          if (is_numeric($test[0][0])) {
                                            $new_titles = str_replace("-", " ", $test[0][0]);
                                            echo ucfirst($new_titles);
                                        } else {
                                            $new_titles = str_replace("-", " ", $test[0][0]);
                                            echo ucfirst($new_titles);
                                        }
                                    }

                                    ?>
                                </a>
                            </li>  
                            <?php }//end of breadcrumbs?>


                        </ul>  
                    </div>
                <!--     <h2 class="section-header">
                        <?php
                        if ($this->uri->segment(3) && $this->uri->segment(1) == "press_release") {
                            $title_new = $this->uri->segment(1);
                            $title = urldecode($title_new);
                        } else if ($this->uri->segment(3)) {

                            $title_new = $this->uri->segment(3);
                            $test[] = explode('_', $title_new);
                            if (is_numeric($test[0][0])) {

                                $new_titles = str_replace("-", " ", $test[0][0]);
                                echo ucfirst($new_titles);
                            } else {
                                $title = urldecode($title_new);
                            }
                        } else {
                            $title = $this->uri->segment(1);
                        }
                        if (strpos($title, '_') != false) {
                            $title_array = explode('_', $title);
                        } else {
                            $title_array = explode('-', $title);
                        }

                        foreach ($title_array as $tarray) {
                            echo ucwords(strtolower($tarray));
                            echo ' ';
                        }
                        ?>
                    </h2> -->
                    <div class="inner-description">
                        <?php
                        if (!isset($view_file)) {
                            $view_file = "";
                        }
                        if (!isset($module)) {
                            $module = $this->uri->segment(1);
                        }
                        if (($view_file != '') && ($module != '')) {
                            $path = $module . "/" . $view_file;
                            $this->load->view($path);
                        } else {
                                $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                echo ($new_description); //put your designs here
                            }
                            ?>
                        </div>

                    </section> 
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="clearfix"></div>                    

    <?php } ?>
    <!-- footer wrap -->
    <div class="footer-top clearfix">
        <div class="container">
            <div class="row">

                <div class="col-sm-4">
                    <h4>Service</h4>
                    <ul class="links">
                        <?php foreach ($services as $row) {?>
                        <?php  $title =strtolower($row['title']);
                        $title1 = str_replace(' ', '_', $title);?>
                        <li>
                         <a href="<?php echo base_url()?><?php echo $title1; ?>">
                             <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $row['title']; ?>
                         </a>
                     </li>
                     <?php  }?>
                 </ul>
                            <!-- <ul class="links">
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hotel Booking</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Treking</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Transportation</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ticketing</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mountain Flight</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Shightseeing</a></li>
                            </ul> -->
                        </div>
                        <div class="col-sm-3 contact">
                            <h4>Contact Us</h4>
                            <?php  foreach ($contact as $row) {?>
                            <address class="address">
                                <span><i class="fa fa-map-marker">&nbsp;</i> <?php echo $row['address'];?></span>
                                <span><i class="fa fa-phone">&nbsp;</i> <?php echo $row['contact_no'];?></span>
                                <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank"><?php echo $row['email'];?></a></span>
                            </address>
                            <?php }?>
                            <!-- <address class="address">
                                <span><i class="fa fa-map-marker">&nbsp;</i> Pulchowk, Lalitpur, Nepal</span>
                                <span><i class="fa fa-phone">&nbsp;</i> +977 015549558</span>
                                <span><i class="fa fa-phone">&nbsp;</i> +977 9818417966</span>
                                <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank">imaginationtraveltrek@gmail.com</a></span>
                            </address> -->
                        </div>
                        <div class="col-sm-5 sign-up">
                            <h4>Newsletter Sign Up</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="row">
                                <div class="col-sm-7">
                                    <input type="email" name="" placeholder="Email Address">
                                </div>
                                <div class="col-sm-5">
                                    <a href="" class="btn">Sign Up Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div> <!-- /.content-wrap end -->
        <!-- footer wrap -->
        <footer class="footer-wrap pad clearfix">
            <div class="footer-btm clearfix">
                <div class="container">
                    <div class="copyright">
                        <p>&copy; Imagination Travel & Treak Pvt.Ltd. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    </div> <!-- /.wrapper end -->
    <!-- Page structure end 
    ================================================== -->
    <!-- core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/jquery.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/responsiveTabs.js"></script>
    <!-- lightbox -->
<!--    <script src="dist/js/jquery.easing.1.3.js"></script>
    <script src="dist/js/simple-lightbox.min.js"></script> -->
    <!-- end lightbox -->
    <!-- <script src="dist/js/heroslider.js"></script> -->
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/camera.js'></script> 

    <script src="<?php echo base_url(); ?>design/frontend/dist/js/custom.js"></script>
    
    <script type="text/javascript">
    $('.testimonial-list').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots: true,
        autoplay: false,
        navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.hotel-slider').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots: false,
        autoplay: true,
        navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
    </script>
    <script>
    jQuery(function(){

        jQuery('#camera_wrap_1').camera({
            loader: 'bar',
            pagination: false,
            playPause: true,
            pauseOnClick: false,
            navigationHover: true,
            hover: false
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        RESPONSIVEUI.responsiveTabs();
    })
    </script>

</body>
</html>