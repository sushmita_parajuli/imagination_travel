<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class="booking inner">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="box">
					<div class="pull-right">
						<span>Select Location: 
							<select>
								<option>Kathmandu</option>
								<option>Pokhara</option>
								<option>Mustang</option>
								<option>Chitwan</option>
							</select>
						</span>
						
					</div>
					<h2>Hotel Booking</h2>

				</div>
				<div class="responsive-tabs">
					<h2>Our Top Picks</h2>
					<div>
						<div class="media">
							<div class="row">
								<div class="media-left col-sm-4">
									<img src="image/hotel/delux.jpg" class="media-object img-responsive">
								</div>
								<div class="media-body col-sm-8">
									<h4 class="media-heading">Hotel Kathmandu</h4>
									<p>Thamel, Kathmandu</p>
									<p>Lorem ipsum...</p>
									<a href="booking-details.php" class="btn btn-success">View Detail</a>
								</div>
							</div>
						</div>
						<hr>
						<div class="media">
							<div class="row">
								<div class="media-left col-sm-4">
									<img src="image/hotel/delux.jpg" class="media-object img-responsive">
								</div>
								<div class="media-body col-sm-8">
									<h4 class="media-heading">Hotel Kathmandu</h4>
									<p>Thamel, Kathmandu</p>
									<p>Lorem ipsum...</p>
									<a href="booking-details.php" class="btn btn-success">View Detail</a>
								</div>
							</div>
						</div>
						<hr>
						<div class="media">
							<div class="row">
								<div class="media-left col-sm-4">
									<img src="image/hotel/delux.jpg" class="media-object img-responsive">
								</div>
								<div class="media-body col-sm-8">
									<h4 class="media-heading">Hotel Kathmandu</h4>
									<p>Thamel, Kathmandu</p>
									<p>Lorem ipsum...</p>
									<a href="booking-details.php" class="btn btn-success">View Detail</a>
								</div>
							</div>
						</div>
						<hr>
						<div class="media">
							<div class="row">
								<div class="media-left col-sm-4">
									<img src="image/hotel/delux.jpg" class="media-object img-responsive">
								</div>
								<div class="media-body col-sm-8">
									<h4 class="media-heading">Hotel Kathmandu</h4>
									<p>Thamel, Kathmandu</p>
									<p>Lorem ipsum...</p>
									<a href="booking-details.php" class="btn btn-success">View Detail</a>
								</div>
							</div>
						</div>
						<hr>
						<div class="media">
							<div class="row">
								<div class="media-left col-sm-4">
									<img src="image/hotel/delux.jpg" class="media-object img-responsive">
								</div>
								<div class="media-body col-sm-8">
									<h4 class="media-heading">Hotel Kathmandu</h4>
									<p>Thamel, Kathmandu</p>
									<p>Lorem ipsum...</p>
									<a href="booking-details.php" class="btn btn-success">View Detail</a>
								</div>
							</div>
						</div>
						<hr>
						<ul class="pagination pull-right">
							<li><a href="#">1</a></li>
							<li class="active"><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
						</ul>
					</div>

					<h2>Lower First Price</h2>
					<div>
						<div>
							<div class="media">
								<div class="row">
									<div class="media-left col-sm-4">
										<img src="image/hotel/delux.jpg" class="media-object img-responsive">
									</div>
									<div class="media-body col-sm-8">
										<h4 class="media-heading">Hotel Kathmandu</h4>
										<p>Thamel, Kathmandu</p>
										<p>Lorem ipsum...</p>
										<a href="booking-details.php" class="btn btn-success">View Detail</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="media">
								<div class="row">
									<div class="media-left col-sm-4">
										<img src="image/hotel/delux.jpg" class="media-object img-responsive">
									</div>
									<div class="media-body col-sm-8">
										<h4 class="media-heading">Hotel Kathmandu</h4>
										<p>Thamel, Kathmandu</p>
										<p>Lorem ipsum...</p>
										<a href="booking-details.php" class="btn btn-success">View Detail</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="media">
								<div class="row">
									<div class="media-left col-sm-4">
										<img src="image/hotel/delux.jpg" class="media-object img-responsive">
									</div>
									<div class="media-body col-sm-8">
										<h4 class="media-heading">Hotel Kathmandu</h4>
										<p>Thamel, Kathmandu</p>
										<p>Lorem ipsum...</p>
										<a href="booking-details.php" class="btn btn-success">View Detail</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="media">
								<div class="row">
									<div class="media-left col-sm-4">
										<img src="image/hotel/delux.jpg" class="media-object img-responsive">
									</div>
									<div class="media-body col-sm-8">
										<h4 class="media-heading">Hotel Kathmandu</h4>
										<p>Thamel, Kathmandu</p>
										<p>Lorem ipsum...</p>
										<a href="booking-details.php" class="btn btn-success">View Detail</a>
									</div>
								</div>
							</div>
							<hr>
							<div class="media">
								<div class="row">
									<div class="media-left col-sm-4">
										<img src="image/hotel/delux.jpg" class="media-object img-responsive">
									</div>
									<div class="media-body col-sm-8">
										<h4 class="media-heading">Hotel Kathmandu</h4>
										<p>Thamel, Kathmandu</p>
										<p>Lorem ipsum...</p>
										<a href="booking-details.php" class="btn btn-success">View Detail</a>
									</div>
								</div>
							</div>
							<hr>
							<ul class="pagination pull-right">
								<li><a href="#">1</a></li>
								<li class="active"><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
			<div class="col-md-4">
				<div class="service">
					<h2 class="text-center">Related Activities</h2>
					<div class="box">
						<a href="#">
							<figure>
								<img src="image/service/1132.jpg" class="img-responsive"/>
								<figcaption>Mountain Flight</figcaption>
							</figure>
						</a>
					</div>
					<div class="box">
						<a href="#">
							<figure>
								<img src="image/service/delux.jpg" class="img-responsive"/>
								<figcaption>Sight Seeing</figcaption>
							</figure>
						</a>
					</div>
					<div class="box">
						<a href="#">
							<figure>
								<img src="image/service/img10.jpg" class="img-responsive"/>
								<figcaption>Transport</figcaption>
							</figure>
						</a>
					</div>
					<div class="box">
						<a href="#">
							<figure>
								<img src="image/service/1132.jpg" class="img-responsive"/>
								<figcaption>Ticketing</figcaption>
							</figure>
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php include 'footer.php'; ?>