<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>SAARC SECRETARIAT</title>
    <link rel="icon" href="image/favicon.png" type="image/png" sizes="16x16">
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['favicon']; ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url(); ?>design/frontend/css/bootstrap.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/scss/all.css" /> 
    <!--<script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>-->
    <!-- <link rel="stylesheet" href= "bootstrap/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="bootstrap/font-awesome-4.5.0/css/font-awesome.min.css" > -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/font-awesome.min.css" /> 
    <link rel='stylesheet' href='<?php echo base_url(); ?>design/frontend/libs/jquery-colorbox/colorbox.css' type='text/css' media='screen' />

    <script src="<?php echo base_url(); ?>design/frontend/js/jquery-1.12.1.min.js" type="text/javascript"></script>
</head>
<body>
   <div class="wrapper">
    <header>
        <div class="container">
            <div class="logo_and_flags logo">
                <div class="col-xs-3 col-sm-2 col-md-1 padding-reset">               
                    <div class="aParent">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url(); ?>uploads/settings/logo.png" alt="SAARC-logo" class="img-responsive">
                        </a>  
                        <!-- <a href="index.html"><img src="<?php echo base_url(); ?>design/frontend/images/logo.png" alt="SAARC-logo" height="112px" > </a> -->
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="logo_name">
                        <h2>SAARC</h2>
                        <p>South Asian Association</p>
                        <p>for Regional Cooperation</p>   
                    </div>   
                </div>
                <div class="col-sm-9 flag-list" >
                    <div class="country_flags pull-right">
                        <ul>
                            <?php foreach ($countrys as $row) { ?>
                            <li>
                                <h5><?php echo $row['title'] ?></h5> 
                                <a href="<?php // echo $row['link'] ?>" title="<?php // echo $row['title'] ?>" target="_blank">
                                    <a href="<?php echo base_url(); ?>country/details/<?php echo $row['slug'] ?>"> <img src="<?php echo base_url(); ?>uploads/country/<?php echo $row['attachment'] ?>" style="padding:0px;" alt="<?php echo $row['title'] ?>"> </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <?php // echo form_open('search', 'method="post" class="navbar-form pull-right"'); ?>
<!--                            <div class="input-group col-md-12 col-sm-12 col-xs-12" style="margin-top:5px;">
                                <input  type="text" name="key" id="search" placeholder="Search" >
                                <label for="search" class="label_image" ></label>
                                                                <button class="row s-box1 btn btn-danger icon-search" type="submit" style="height:26px; width:22px;">   
                            </button>
                        </div>-->
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <nav class="navbar navbar-inverse navbar-static-top marginBottom-0" role="navigation" style="border-color:#9e4f24;">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse " id="navbar-collapse-1" style="background-color:#fcfcfc; padding-bottom: 2px;">
                        <ul class="nav navbar-nav"> 

                            <?php foreach ($headernav as $header) {//still need to work on slug  ?> 
                            <?php
                            if (empty($header['children'])) {
                                echo '<li>'; /* this is for no-child */
                            } else {
                                ?>                      
                                <li  class="dropdown">
                                    <?php }/* this is for dropdown */ ?>

                                    <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                    <?php
                                    if ($header['navtype'] == 'URL') {
                                        $prefix = '';
                                    } else {
                                        $prefix = base_url();
                                    }
                                    ?>
                                    <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?>                                  
                                        <!-- <b class="caret"></b>                                 -->
                                        <?php }/* end of if */ else { /* this is for no-child */
                                            ?>                            
                                            <?php
                                            if ($header['navtype'] == 'URL') {
                                                $prefix = '';
                                            } else {
                                                $prefix = base_url();
                                            }
                                            ?>
                                            <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
                                                <?php } ?>                           
                                            </a>

                                            <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                            <ul class="dropdown-menu">
                                                <?php foreach ($header['children'] as $child) { ?>
                                                <!--code for grandchild by harry--> 
                                                <?php if (!empty($child['grand_children'])) { ?>
                                                <li class="dropdown dropdown-submenu">                                            
                                                    <?php
                                                    if ($child['navtype'] == 'URL') {
                                                        $prefix = '';
                                                    } else {
                                                        $prefix = base_url();
                                                    }
                                                    ?>
                                                    <a class="dropdown-toggle" href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>

                                                    <ul class="dropdown-menu" style="background:#fcfcfc;" padding:10px;>
                                                        <?php foreach ($child['grand_children'] as $grandchild) { ?>
                                                        <li >                                            
                                                            <?php
                                                            if ($grandchild['navtype'] == 'URL') {
                                                                $prefix = '';
                                                            } else {
                                                                $prefix = base_url();
                                                            }
                                                            ?>
                                                            <a href="<?php echo $prefix . $grandchild['href']; ?>" target="<?php echo $grandchild['target'] ?>"><?php echo $grandchild['title']; ?></a>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                                <!--end  of code for grandchild by harry--> 
                                                <?php } else { ?> <!--this else part if grand child doesnot exist-->
                                                <li>                                            
                                                    <?php
                                                    if ($child['navtype'] == 'URL') {
                                                        $prefix = '';
                                                    } else {
                                                        $prefix = base_url();
                                                    }
                                                    ?>
                                                    <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                                </li>
                                                <?php }/* closing of else part if grand child doesnot exist */ ?>
                                                <?php }/* end of child foreach */ ?>
                                            </ul>
                                            <?php } ?>  

                                        </li>
                                        <?php }/* end of parent foreach */ ?>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </nav>
                            <hr class="hr-style">
                        </div>
                    </header>
                    <?php
                    $first_bit = $this->uri->segment(1);
                    if ($first_bit == "" || $first_bit == "home") {
                        ?>
                        <main>
                            <div class="container">
                                <div id="myCarousel" class="carousel slide carousel-div" data-ride="carousel">
                                    <!-- Indicators -->
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <?php $active = 'active'; ?>
                                        <?php foreach ($banners as $row) { ?> 

                                        <div class="item <?php echo $active ?>" data-group="slide">
                                            <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>" style="width:1500px;height:100%;object-fit:cover" class="img-responsive">
                                            <div class="carousel-caption"><?php echo $row['title'] ?></div>
                                        </div>

                                        <?php $active = '';
                                    } ?>
                                    <!-- Left and right controls -->
                                    <div>
                                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                         <!--  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> -->
                                         <i class="fa fa-angle-left fa-3x" aria-hidden="true"></i>
                                         <span class="sr-only">Previous</span>
                                     </a>
                                     <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                         <!-- <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> -->
                                         <i class="fa fa-angle-right fa-3x" aria-hidden="true"></i>
                                         <span class="sr-only">Next</span>
                                     </a>
                                 </div>
                             </div>

                         </div>
                     </div>
                     <div class="clearfix"></div>
                     <?php if(count($news_alert)!=0) {?>
                     <div class="container">
                        <div class="news-alert">
                            <span style="width:120px;">News Alert</span>
                            <!-- <div style="width:100px;"> -->
                            <?php foreach ($news_alert as $row) {?>


                            <marquee scrollamount="3" onmouseover="this.stop();" onmouseout="this.start();">
                              <?php echo $row['description']; ?> 
                          </marquee> 
                                  <?php //  }
                              }
                              ?>
                              <!-- </div> -->
                          </div>
                      </div>
                      <?php }?>
                    <!-- <section>
                      <div class="about-us">
                        <div class="container" style="background:transparent;">
    <?php foreach ($about_saarc as $row) { ?>
                              <div class="col-sm-4">
                                <img src="<?php echo base_url(); ?>uploads/pages/<?php echo $row['attachment'] ?>" alt="" class="img-responsive img-thumbnail img-rounded">
                              </div>
                              <div class="col-sm-8">
                                <div class="aboutus-description">
                                  <h2><?php echo $row['title'] ?></h2>
                                  <p><?php echo word_limiter($row['description'], 110); ?></p>
                                  <a href="<?php echo base_url(); ?>about-saarc-secretariat" class="hvr-bubble-float-top">Read More</a>
                                </div>
                              </div>
    <?php } ?>
                        </div>
                      </div>
                  </section> -->
                  <div class="clearfix"></div>
                  <div class="container">
                    <section class="row">
                        <div class="tab-all">
                            <div class="col-sm-8 col-md-9">
                                <div class="worko-tabs">
                                    <input class="state" type="radio" title="tab-one" name="tabs-state" id="tab-one" checked />
                                    <input class="state" type="radio" title="tab-two" name="tabs-state" id="tab-two" />
                                    <input class="state" type="radio" title="tab-three" name="tabs-state" id="tab-three" />
                                    <input class="state" type="radio" title="tab-four" name="tabs-state" id="tab-four" />

                                    <div class="tabs flex-tabs">
                                        <label for="tab-one" id="tab-one-label" class="tab">Latest Updates </label>
                                        <label for="tab-two" id="tab-two-label" class="tab">Press Release</label>
                                        <label for="tab-three" id="tab-three-label" class="tab">Statements </label>
                                        <label for="tab-four" id="tab-four-label" class="tab">Announcement</label>
                                        <div id="tab-one-panel" class="panel active">
                                            <!-- <div class="row"> -->
                                                <!--    <?php // foreach ($featured_stories as $row){ ?>
                                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                                     <div class="hovereffect">
                                                       <img src="<?php echo base_url(); ?>uploads/featured_stories/<?php echo $row['attachment']; ?>" alt="<?php echo $row['title']; ?>" class="img-responsive"/>
                                                       <div class="overlay">
                                                        <h2><?php // echo $row['title']; ?></h2>
                                                        <a class="info" href="<?php echo base_url(); ?>featured_stories/details/<?php echo $row['slug'] ?>">View More</a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <?php // }?> -->
                                                  <ul>
                                                    <?php foreach ($news_for_front as $row) { ?>
                                                    <li>
                                                        <a href="<?php echo base_url(); ?>news/detail_front/<?php echo $row['slug'] ?>" class="blue-link bold-h3">
                                                            <?php echo $row['title']; ?></a>
                                                            <p class="date"><?php echo $row['published_date'] ?></p>
                                                            <!-- <p class="hidden-sm" style="font-size:11px;" ><?php echo word_limiter($row['description'], 10); ?></p> -->
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <p><a href="<?php echo base_url(); ?>news/news_front" class="view-all pull-right">View all</a></p> 

                                                    <!-- </div> -->
                                                </div>
                                            <!--         <div class="grid">
    <?php // foreach ($featured_stories as $row){ ?>
                                                     <a href="<?php echo base_url(); ?>featured_stories/details/<?php echo $row['slug'] ?>">
                                                       <figure class="effect-goliath">
                                                         <img src="<?php echo base_url(); ?>uploads/featured_stories/<?php echo $row['attachment']; ?>" alt="<?php echo $row['title']; ?>"/>
                                                         <figcaption>
                                                           <h2>
                                            <?php // echo $row['title']; ?>
                                                           </h2>
                                                           <p>  <?php // echo $row['published_date']; ?> 
    <?php // echo $row['title']; ?>
                                                           </figcaption>     
                                                         </figure>
                                                       </a>
    <?php // } ?>
</div> -->
<div class="clearfix"></div>
<!-- <p><a href="<?php echo base_url(); ?>featured_stories/view_all" class="view-all pull-right">View all</a></p> -->
<!-- </div> -->
<div id="tab-two-panel" class="panel">
    <ul>
        <?php foreach ($press_release_for_front as $row) { ?>
        <li>
            <a href="<?php echo base_url(); ?>press_release/details/<?php echo $row['slug'] ?>" class="blue-link bold-h3">
                <?php echo $row['title']; ?></a>
                <p class="date"><?php echo $row['published_date']; ?></p>
            </li>
            <?php } ?>
            <p><a href="<?php echo base_url(); ?>press_release" class="view-all pull-right">View all</a></p>
        </ul>
    </div>

    <div id="tab-three-panel" class="panel">
        <ul>
            <?php foreach ($statement as $row) { ?>
            <li>
                <a href="<?php echo base_url(); ?>digital_library/detail_menu/<?php echo $row['slug'] ?>" class="blue-link bold-h3">
                    <?php echo $row['title']; ?></a>
                    <p class="date"> <?php echo $row['published_date']; ?></p>
                </li>
                <?php } ?>
                <p><a href="<?php echo base_url(); ?>digital_library" class="view-all pull-right">View all</a></p>
            </ul>

        </div>
        <div id="tab-four-panel" class="panel">

            <div id="ChildVerticalTab_1">
                <ul class="resp-tabs-list ver_1">
                    <?php foreach ($announcement as $row) { ?>
                    <li><?php echo $row['title']; ?></li>
                    <?php } ?>
                </ul>
                <div class="resp-tabs-container ver_1">
                    <div>

                        <div class="table-responsive">
                            <table class="table table-design">
                                <thead>
                                    <tr style="width:100px">
                                        <th >Posted Date</th>
                                        <th>Title</th>
                                        <th>Deadline  </th>
                                        <th style="width:50px">Terms of Reference</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($scholarship as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['posted_date'] ?></td>
                                        <td><span class="table-span"><?php echo $row['title'] ?></span></td>
                                        <td><?php echo $row['deadline'] ?></td>
                                        <td><span class="table-span">  <a href="<?php echo base_url(); ?>download/scholarship/<?php echo $row['attachment']; ?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                        <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-design">
                                <thead>
                                    <tr>
                                        <th>Posted Date</th>
                                        <th>Title</th>
                                        <th>Level</th>
                                        <th>Duty Station</th>
                                        <th>Application Deadline  </th>
                                        <th>Terms of Reference</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vacancy as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['posted_date'] ?></td>
                                        <td><span class="table-span"><?php echo $row['title'] ?></span></td>
                                        <td><?php echo $row['level'] ?></td>
                                        <td><?php echo $row['duty_station'] ?></td>
                                        <td><?php echo $row['deadline'] ?></td>
                                        <td><span class="table-span">  <a href="<?php echo base_url(); ?>download/vacancy/<?php echo $row['attachment']; ?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                        <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-design">
                                <thead>
                                    <tr>
                                        <th>Posted Date</th>
                                        <th>Title</th>
                                        <th>Deadline  </th>
                                        <th>Terms of Reference</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tenders as $row) { ?>
                                    <tr>
                                        <td><?php echo $row['posted_date'] ?></td>
                                        <td><span class="table-span"><?php echo $row['title'] ?></span></td>

                                        <td><?php echo $row['deadline'] ?></td>
                                        <td><span class="table-span">  <a href="<?php echo base_url(); ?>download/tenders/<?php echo $row['attachment']; ?>" target="blank" class="btn btn-info">Download</a></i></span></td>
                                        <!--<td><span class="table-span"><i class="fa fa-download" aria-hidden="true"></i></span></td>-->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!--<p>Suspendisse blandit velit Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravid urna gravid eget erat suscipit in malesuada odio venenatis.</p>-->
                    </div>
                    <div>
                        <ul class="list-group list-group-design">
                            <?php foreach ($awards as $row) { ?>
                            <li class="list-group-item"><a href="<?php echo base_url(); ?>awards/details/<?php echo $row['slug'] ?>"><?php echo $row['title']; ?></a></li>
                            <!-- <li class="list-group-item"><a href="#"><?php echo $row['title']; ?></a></li> -->
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="photo-homepage">
 <!--    <div class="row">
 <div class="col-sm-12"> -->
    <!--Horizontal Tab-->
    <div id="parentHorizontalTab">
        <ul class="resp-tabs-list hor_1">
            <li>Photo Gallery</li>
            <li>Video Gallery</li>
        </ul>
        <div class="resp-tabs-container hor_1">
            <div id="amazingcarousel-container-3">
                <div id="amazingcarousel-3" style="display:block;position:relative;width:100%; max-width:900px;margin:0px auto 0px;">
                    <div class="amazingcarousel-list-container">
                        <ul class="amazingcarousel-list">
                            <?php
                            $i = 0;
                            foreach ($photos_front as $row) {

                                ?>
                                <li class="amazingcarousel-item">
                                    <div class="amazingcarousel-item-container">
                                        <div class="amazingcarousel-image">
                                            <a href="<?php echo base_url() ?>photos/photos_details/<?php echo strtolower(url_title($row['category'])) ; ?>">
                                                <img src="<?php echo base_url(); ?>uploads/photos/<?php echo $row['attachment']; ?>" style="height:150px;width:100%;object-fit: cover;"/>
                                            </a>

                                        </div>
                                    </div>
                                </li>
                                <?php
                                $i++;
                            }
                            ?>
                        </ul>
                        <div class="amazingcarousel-prev"></div>
                        <div class="amazingcarousel-next"></div>
                    </div>
                    <div class="amazingcarousel-nav"></div>
                    <p><a href="<?php echo base_url(); ?>photos/" class="view-all pull-right">View all</a></p>
                </div>
            </div>
            <div>
                <div id="gallery" style="display:none;">
                    <?php foreach ($videos as $row) { ?>
                    <div data-type="youtube"
                    data-title="<?php echo $row['title']; ?>"
                    data-description="<?php echo $row['description']; ?>"
                    data-thumb="https://i.ytimg.com/vi/<?php echo $row['link']; ?>/mqdefault.jpg"
                    data-image="https://i.ytimg.com/vi/<?php echo $row['link']; ?>/sddefault.jpg"
                    data-videoid="<?php echo $row['link']; ?>"> </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!--     </div>
    </div> -->
</div>
</div>  
</div>
<div class=" col-sm-4 col-md-3">
    <div class="right-sidebar">
        <a class="twitter-timeline" href="https://twitter.com/saarcsec" height="755">SAARC</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
</div>
</div>
</div>
</section>
<div class="clearfix"></div>
<!-- <section>
    <div class="photo-homepage container">
        <div class="row">
            <div class="col-sm-8 col-md-9"> -->
                <!--Horizontal Tab-->
                <!-- <div id="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>Photo Gallery</li>
                        <li>Video Gallery</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div id="amazingcarousel-container-3">
                            <div id="amazingcarousel-3" style="display:block;position:relative;width:100%; max-width:900px;margin:0px auto 0px;">
                                <div class="amazingcarousel-list-container">
                                    <ul class="amazingcarousel-list">
                                        <?php
                                        $i = 0;
                                        foreach ($photos_front as $row) {

                                            ?>
                                            <li class="amazingcarousel-item">
                                                <div class="amazingcarousel-item-container">
                                                    <div class="amazingcarousel-image">
                                                        <a href="<?php echo base_url() ?>photos/photos_details/<?php echo $row['category']; ?>">
                                                            <img src="<?php echo base_url(); ?>uploads/photos/<?php echo $row['attachment']; ?>" style="height:150px;width:100%;object-fit: cover;"/>
                                                        </a>

                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </ul>
                                    <div class="amazingcarousel-prev"></div>
                                    <div class="amazingcarousel-next"></div>
                                </div>
                                <div class="amazingcarousel-nav"></div>
                                <p><a href="<?php echo base_url(); ?>photos/" class="view-all pull-right">View all</a></p>
                            </div>
                        </div>
                        <div>
                            <div id="gallery" style="display:none;">
                                <?php foreach ($videos as $row) { ?>
                                <div data-type="youtube"
                                data-title="<?php echo $row['title']; ?>"
                                data-description="<?php echo $row['description']; ?>"
                                data-thumb="https://i.ytimg.com/vi/<?php echo $row['link']; ?>/mqdefault.jpg"
                                data-image="https://i.ytimg.com/vi/<?php echo $row['link']; ?>/sddefault.jpg"
                                data-videoid="<?php echo $row['link']; ?>"> </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
             <div class="twitter-feeds" style="margin-top:30px;">
                <div class="padding-reset">
                    <h3 class="heading text-center">Events</h3>
                </div>
                <div class="new_items">
                    <?php foreach ($events as $row) { ?>
                    <div class="media">
                        <div class="media-left media-middle hidden-sm">
                            <a href="<?php echo base_url(); ?>events/details/<?php echo $row['slug']; ?>">
                                <img src="<?php echo base_url(); ?>uploads/events/<?php echo $row['attachment'] ?>" alt="" width="50px" height="50px"/>
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading margin-reset" style="font-size:13px;">
                                <a href="<?php echo base_url(); ?>events/details/<?php echo $row['slug']; ?>" class="blue-link bold-h3" style="color:#da9f21;">
                                    <?php echo word_limiter($row['title'], 5); ?>
                                </a>
                            </h5>
                            <p class="hidden-sm" style="font-size:11px;" ><?php echo word_limiter($row['description'], 10); ?></p>
                        </div>
                    </div>
                    <?php } ?>
                    <p><a href="<?php echo base_url(); ?>events" class="view-all pull-right">View all</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section> -->
<div class="clearfix"></div>
<section>
    <div class="container">
        <div class="quick-links">
            <h2 class="section_header center-text">Quick Links</h2>
            <div class="links-list">
                <?php foreach ($quick_link_front as $row) { ?>
                <a href="<?php echo $row['link']; ?>" class="hvr-pulse">
                    <img src="<?php echo base_url(); ?>uploads/quick_link/<?php echo $row['attachment']; ?>"  alt="<?php echo $row['title']; ?>" style="height: 70px;;object-fit: cover;">
                </a>
                <?php } ?>
            </div>
            <p><a href="<?php echo base_url(); ?>quick_link" class="view-all pull-right">View all</a></p>

        </div>
    </div>
</section>
</div>
<div class="clearfix"></div>
</main>
<?php
} else if ($first_bit == 'search') {
    ?>

    <div class="clearfix"></div>
    <div class="container" style="padding-left: 50px; padding-right:50px; margin-top:10px;">
        <div class="row">
            <?php
            $count1 = count($query_search_pages);
            $count2 = count($query_search_modules);
            $count = $count1 + $count2;

            if ($count == 0) {
                ?><h3> No search results found for "<?php echo $searchkey; ?>"</h3><?php } else {
                    ?>
                    <h3> Showing search results for "<?php echo $searchkey; ?>"</h3><?php
                    foreach ($query_search_pages->result() as $row) {
                        ?>
                        <div class="result search-result" style='overflow:hidden; border:2px solid #f7991b; padding: 10px; border-radius: 10px;'><?php
                        echo '<a href="' . base_url() . $row->slug . '">
                        <h4 style="margin-bottom: 0px;">' . ucfirst($row->title) . '</h4></a><br>';
                        ?>
                        <!-- <div class="page_image"> -->
                        <?php if ($row->attachment != null) { ?>
                        <img class="pull-left" style="width:30%; margin-bottom:10px;" src="<?php echo base_url(); ?>uploads/pages/<?php echo $row->attachment; ?>">
                        <?php } ?>
                        <!-- </div> -->
                        <p><?php echo word_limiter($row->description, 100); ?></p>
                    </div>
                    <hr>
                    <?php
                }
                foreach ($query_search_modules->result() as $row) {
                    ?>
                    <div class="result search-result" style='border:2px solid #f7991b; padding: 10px; border-radius: 10px;'>
                        <?php
                        echo '<a href="' . base_url() . $row->slug . '">
                        <h4 style="margin-bottom: 0px;">' . ucfirst($row->title) . '</h4></a><br>';
                        ?>
                        </div><hr><?php
                    }
                }
                ?>
            </div>
        </div>
        <?php } else {
            ?>
            <!-- start of the content ------------------------------------------>
            <div id="content">
                <?php
                if (isset($click)) {
                    $slug = $this->uri->segment(1);
                    ?>   

                    <div class="container" style="min-height:600px;">
                        <div class="row">
                            <div class="col-sm-8 col-lg-9">
                                <div class="breadcrumb_div hidden-xs">  
                                    <ul id="breadcrumbs" class="breadcrumb">  

                                        <li>  
                                            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                                <span class="arrow"></span></a>

                                            </li>
                                            <?php
                                //making bread crumbs

                                            if ($this->uri->segment(1) != '') {
//                        var_dump($headernav);die;
                                                $breadcrumb_url = $this->uri->segment(1);
                                                $breadcrumb_title = ucfirst($title);
                                                ?>  
                                                <?php if ($parent_title == 'SAARC') { ?>
                                                <li > 
                                                    <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                </li>
                                                <li class="current"> 
                                                    <a  href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                                    <!-- <span class="arrow"></span> -->
                                                </li> 	
                                                <?php }
                                                if ($parent_title == 'SAARC Structure') {
                                                    ?>
                                                    <li> 
                                                        <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li> 
                                                        <a><?php echo 'SAARC Structure'; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li class="current"> 
                                                        <a class="odd" href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                                        <!-- <span class="arrow"></span> -->
                                                    </li> 
                                                    <?php }
                                                    if ($parent_title == 'SAARC Secretariat ') {
                                                        ?>
                                                        <li>  

                                                            <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li> 
                                                            <a ><?php echo 'SAARC Secretariat'; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li class="current"> 
                                                            <a class="odd" href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 

                                                        </li> 
                                                        <?php } else if ($parent_title == 'REGIONAL CENTRES ') {
                                                            ?>
                                                            <li> 
                                                                <a class="odd"><?php echo $headernav[2]['title']; ?> <span class="arrow"></span></a>
                                                            </li>
                                                            <li class="current"> 
                                                                <a  href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                                                <!-- <span class="arrow"></span> -->
                                                            </li>
                                                            <?php } else if ($parent_title == 'DIGITAL LIBRARY ') {
                                                                ?>
                                                                <li> 
                                                                    <a class="odd"><?php echo $headernav[2]['title']; ?> <span class="arrow"></span></a>
                                                                </li>
                                                                <li class="current"> 
                                                                    <a  href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                                                    <!-- <span class="arrow"></span> -->
                                                                </li>
                                                                <?php }
                                                            }
                                                            ?>

                                                            <?php if ($this->uri->segment(2)) {
//                          die('here')
                                                                ?>
                                                                <li class="current"> 
                                                                    <a class="odd"><?php echo ucfirst($this->uri->segment(2)) ?>
                                                                    </a>
                                                                </li>   
                                                                <?php }//end of breadcrumbs ?>


                                                            </ul>  
                                                        </div>
                                                        <section class="inner-section">
                                                            <h2 class="section-header">
                                                                <?php
                                                                if ($this->uri->segment(3)) {
//                          die('here');
                                                                    $title_new = $this->uri->segment(3);
                                                                    $title = urldecode($title_new);
                                                                } else {
//                          die('here');
                                                                    $title = $this->uri->segment(1);
//                        var_dump($title);die;
                                                                }
                                                                if (strpos($title, '_') != false) {
//                          die('here');
                                                                    $title_array = explode('_', $title);
                                                                } else {
                                                                    $title_array = explode('-', $title);
                                                                }
//                      echo'here';
                                                                echo $breadcrumb_title;
//                      foreach ($title_array as $tarray) {
//                          
//                        echo ucwords(strtolower($tarray));
//                        echo ' ';
//                      }
                                //   echo $title_echo;
                                                                ?>
                                                            </h2>
                                                            <div class="inner-image">
                                                                <?php if (($option) == 'yes' && $attachment != '') { ?>
                                                                <img src="<?php echo base_url(); ?>uploads/pages/<?php echo $attachment ?>" class="img-thumbnail img-rounded">
                                                                <?php } ?>
                                                            </div>
                                                            <div class="inner-description" >
                                                                <?php
                                                                if (!isset($view_file)) {
                                                                    $view_file = "";
                                                                }
                                                                if (!isset($module)) {
                                                                    $module = $this->uri->segment(1);
                                                                }
                                                                if (($view_file != '') && ($module != '')) {
                                                                    $path = $module . "/" . $view_file;
                                                                    $this->load->view($path);
                                                                } else {
                                    $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                    echo ($new_description); //put your designs here
//                        echo($option);
                                    ?>

                                    <?php
                                }

                                //echo '<pre>';
                                //print_r($this->session);
                                //die();
                                ?>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-4 col-lg-3">
                        <div class="quick-links" style="margin:0;">
                            <a class="twitter-timeline" href="https://twitter.com/saarcsec" height="340">SAARC</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>

                        <div class="twitter-feeds" style="margin-top:30px;">
                            <div class="padding-reset">
                                <h3 class="heading text-center">Top Stories</h3>
                            </div>
                            <div class="new_items">
                                <?php foreach ($news_for_front as $row) { ?>
                                <div class="media">
                                        <!-- <div class="media-left media-middle hidden-sm">
                                            <a href="<?php echo base_url(); ?>news/detail/<?php echo $row['slug'] ?>">
                                                <img src="<?php echo base_url(); ?>uploads/news/<?php echo $row['attachment'] ?>" alt="" width="50px" height="50px"/>
                                            </a>
                                        </div> -->
                                        <div class="media-body">
                                            <h5 class="media-heading margin-reset" style="font-size:13px;"><a href="<?php echo base_url(); ?>news/detail_front/<?php echo $row['slug'] ?>" class="blue-link bold-h3" style="color:#da9f21;">
                                                <?php echo $row['title']; ?></a></h5>
                                                <p style="text-align: justify !important;font-size:10px;"><?php echo $row['published_date'] ?></p>
                                                <!-- <p class="hidden-sm" style="font-size:11px;" ><?php echo word_limiter($row['description'], 10); ?></p> -->
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <p><a href="<?php echo base_url(); ?>news/news_front" class="view-all pull-right">View all</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if (!isset($click)) {
//                                die('here');
                    ?>  
                    <div class="container">
                        <div class="col-sm-8 col-md-9" style="min-height:350px; padding: 0px !important;">
                            <section class="inner-section">
                                <div class="breadcrumb_div hidden-xs">  
                                    <ul id="breadcrumbs" class="breadcrumb">  

                                        <li>  
                                            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                                <span class="arrow"></span></a>

                                            </li>
                                            <?php
                                //making bread crumbs

                                            if (($this->uri->segment(1) != '') && ($this->uri->segment(1) != 'secretary_general') && ($this->uri->segment(1) != 'former_secretaries_general') && ($this->uri->segment(1) != 'former_saarc_director') && ($this->uri->segment(1) != 'saarc_director')) {
                                                $breadcrumb_url = $this->uri->segment(1);

                                                $breadcrumb_titles = str_replace("_", " ", $breadcrumb_url);
                                                $breadcrumb_title = ucfirst($breadcrumb_titles);
                                                ?>
                                                <li class="current"> 
                                                    <a class="odd"  href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?>
                                                        <span class="arrow"></span></a> 
                                                    </li>   
                                                    <?php
                                                } else if ($this->uri->segment(1) == 'secretary_general') {
                                                    $breadcrumb_url = $this->uri->segment(1);
                                                    $breadcrumb_titles = str_replace("_", " ", $breadcrumb_url);
                                                    $breadcrumb_title = ucfirst($breadcrumb_titles);
                                                    ?>
                                                    <li> 
                                                        <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li> 
                                                        <a><?php echo 'SAARC Secretariat'; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li class="current"> 
                                                        <a class="odd" href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a>
                                                    </li>   
                                                    <?php
                                                } else if ($this->uri->segment(1) == 'saarc_director') {
                                                    $breadcrumb_url = $this->uri->segment(1);
                                                    $breadcrumb_titles = str_replace("_", " ", $breadcrumb_url);
                                                    $breadcrumb_title = ucfirst($breadcrumb_titles);
                                                    ?>
                                                    <li> 
                                                        <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li> 
                                                        <a><?php echo 'SAARC Secretariat'; ?> <span class="arrow"></span></a>
                                                    </li>
                                                    <li class="current"> 
                                                        <a class="odd"  href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?>
                                                            <span class="arrow"></span></a>
                                                        </li>   
                                                        <?php
                                                    } else if ($this->uri->segment(1) == 'former_saarc_director') {
                                                        $breadcrumb_url = $this->uri->segment(1);
                                                        $breadcrumb_titles = str_replace("_", " ", $breadcrumb_url);
                                                        $breadcrumb_title = ucfirst($breadcrumb_titles);
                                                        ?>
                                                        <li> 
                                                            <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li> 
                                                            <a><?php echo 'SAARC Secretariat'; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li class="current"> 
                                                            <a class="odd" href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a>
                                                        </li>   
                                                        <?php
                                                    } else if ($this->uri->segment(1) == 'former_secretaries_general') {
                                                        $breadcrumb_url = $this->uri->segment(1);
                                                        $breadcrumb_titles = str_replace("_", " ", $breadcrumb_url);
                                                        $breadcrumb_title = ucfirst($breadcrumb_titles);
                                                        ?>
                                                        <li> 
                                                            <a class="odd"><?php echo $headernav[0]['title']; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li> 
                                                            <a><?php echo 'SAARC Secretariat'; ?> <span class="arrow"></span></a>
                                                        </li>
                                                        <li class="current"> 
                                                            <a class="odd" href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?>
                                                            </a> 
                                                        </li>   
                                                        <?php }
                                                        ?>

                                                        <?php if ($this->uri->segment(3)) {
                                                            ?>
                                                            <li class="current"> 
                                                                <a><?php
                                                                $new_title = $this->uri->segment(3);

                                                                $test[] = explode('_', $new_title);
//                        var_dump($test[0][0]);die;  
                                                                if(isset($details)){
                                                                   if(sizeof($details)>0){
                                                                    echo word_limiter($details[0]["title"],10);

                                                                }
                                                            }else{
                                                              if (is_numeric($test[0][0])) {
                                                                $new_titles = str_replace("-", " ", $test[0][0]);
                                                                echo ucfirst($new_titles);
                                                            } else {
                                                                $new_titles = str_replace("-", " ", $test[0][0]);
                                                                echo ucfirst($new_titles);
                                                            }
                                                        }


                                                        ?></span>
                                                    </a>
                                                </li>  
                                                <?php }//end of breadcrumbs?>


                                            </ul>  
                                        </div>
                                        <h2 class="section-header">
                                            <!-- color: rgba(177, 0, 0, 0.75); --> 
                                            <?php
                                            if ($this->uri->segment(3) && $this->uri->segment(1) == "press_release") {
//                                                die('here');

                                                $title_new = $this->uri->segment(1);
                                                $title = urldecode($title_new);
                                            } else if ($this->uri->segment(3)) {

                                                $title_new = $this->uri->segment(3);
                                                $test[] = explode('_', $title_new);
//                 $title = urldecode($title_new); 
                                                if (is_numeric($test[0][0])) {

                                                    $new_titles = str_replace("-", " ", $test[0][0]);
                                                    echo ucfirst($new_titles);
                                                } else {
                                                    $title = urldecode($title_new);

//                      $new_titles  =str_replace("-"," ",$new_title);
//                      echo ucfirst($new_titles);
                                                }
                                            } else {
                                                $title = $this->uri->segment(1);
                                            }





                                            if (strpos($title, '_') != false) {
                                                $title_array = explode('_', $title);
                                            } else {
                                                $title_array = explode('-', $title);
                                            }

                                            foreach ($title_array as $tarray) {
                                                echo ucwords(strtolower($tarray));
                                                echo ' ';
                                            }
                            //   echo $title_echo;
                                            ?>
                                        </h2>
                                        <div class="inner-description">
                                            <?php
                                            if (!isset($view_file)) {
                                                $view_file = "";
                                            }
                                            if (!isset($module)) {
                                                $module = $this->uri->segment(1);
                                            }
                                            if (($view_file != '') && ($module != '')) {
                                                $path = $module . "/" . $view_file;
                                                $this->load->view($path);
                                            } else {
                                $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                echo ($new_description); //put your designs here
                            }
                            ?>
                        </div>

                    </section> 
                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="quick-links" style="margin:0;">
                        <a class="twitter-timeline" href="https://twitter.com/saarcsec" height="340">SAARC</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>

                    <div class="twitter-feeds" style="margin-top:30px;">
                        <div class="padding-reset">
                            <h3 class="heading text-center">Top Stories</h3>
                        </div>
                        <div class="new_items">
                            <?php foreach ($news_for_front as $row) { ?>
                            <div class="media">
                                  <!--   <div class="media-left media-middle hidden-sm">
                                        <a href="<?php echo base_url(); ?>news/detail/<?php echo $row['slug'] ?>">
                                            <img src="<?php echo base_url(); ?>uploads/news/<?php echo $row['attachment'] ?>" alt="" width="50px" height="50px"/>
                                        </a>
                                    </div> -->
                                    <div class="media-body">
                                        <h5 class="media-heading margin-reset" style="font-size:13px;"><a href="<?php echo base_url(); ?>news/detail_front/<?php echo $row['slug'] ?>" class="blue-link bold-h3" style="color:#da9f21;">
                                            <?php echo $row['title']; ?></a></h5>
                                            <p style="text-align: justify !important;font-size:10px;"><?php echo $row['published_date'] ?></p>
                                            <!-- <p class="hidden-sm" style="font-size:11px;" ><?php echo word_limiter($row['description'], 10); ?></p> -->
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <p><a href="<?php echo base_url(); ?>news/news_front" class="view-all pull-right">View all</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>                    

                    <?php } ?>
                    <footer style="margin-top:10px;">
                        <div class="footer-color">
                            <div class="footer-element">
                                <div class="container" style="background:transparent;">
                                    <div class="col-sm-8 col-md-9">
                                        <ul class="cl-effect-1">
                                            <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                                            <li><a href="<?php echo base_url(); ?>about-saarc-secretariat">ABOUT SECRETARIAT</a></li>
                                            <li><a href="<?php echo base_url(); ?>contacts">CONTACT US</a></li>

                                            <li><a href="<?php echo base_url(); ?>site_map">SITEMAP</a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <hr style="margin: 0px;">
                                        <div class="contact-info col-sm-6 col-lg-5">
                                            <p>SAARC Secretariat,P.O Box.4222,<br>
                                                <i class="fa fa-location-arrow" aria-hidden="true"></i> Tridevi Marg,Kathmandu,Nepal<br>
                                                <i class="fa fa-phone" aria-hidden="true"></i> -+977 014445788,014445788,014445788<br>
                                                <i class="fa fa-fax" aria-hidden="true"></i> -+977 014445788 014445788</p>
                                            </div>  
                                            <!-- <div class=" col-lg-3"></div> -->
                                            <div class="col-sm-6 col-lg-6">
                                                <?php foreach ($counter->result() as $row) { ?>
                                                <h4 class="text-center" style="color:#fff;">Website Visited:</h4>
                                                <p class="text-center" style="color:#fff;"> <?php echo $row->counter; ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="contact-info">
                            <!--                        <div class="col-lg-8">
                                                        <p>Copyright 2009 SAARC,All Rights Reserved</p>
                                                        <p>SAARC Secreteriat,P.O Box.4222,Tridevi Marg,Kathmandu,Nepal</p>
                                                        <p>Tel:-+977    014445788,014445788,014445788 Fax:-+977 014445788 014445788</p>
                                                    </div>    -->

                                                    <div class="social">
                                                        <h5>CONNECT WITH US</h5>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <hr style="margin-top: 4px; margin-bottom: 0px;">
                                                    <div class="social-bar" >
                                                        <a class="btn btn-social-icon btn-twitter">
                                                            <span class="fa fa-facebook "></span>
                                                        </a>
                                                        <a class="btn btn-social-icon btn-twitter" href="https://twitter.com/saarcsec" >
                                                            <span class="fa fa-twitter "></span>
                                                        </a>
                                <!--  <a class="btn btn-social-icon btn-twitter">
                                   <span class="fa fa-linkedin "></span>
                               </a> -->
                               <a class="btn btn-social-icon btn-twitter">
                                <span class="fa fa-youtube-play "></span>
                            </a>
                        </div>    
                    </div>
                </div>
                <div class="clearfix"></div>
                <span class="pull-right" style="color:#fff;font-size:14px;">Design & Developed by <a style="color:white" target="_blank" href="http://upvedatech.com/">Upveda Technology.</a></span>
                <p class="all-right">Copyright 2009 SAARC,All Rights Reserved</p>
            </div>
        </div>
    </div>
    <a href="#0" class="cd-top">Top</a>
</footer>
</div>
<!-- Modal -->
<script src="<?php echo base_url(); ?>design/frontend/js/amazingcarousel/amazingcarousel.js"></script>
<script src="<?php echo base_url(); ?>design/frontend/js/amazingcarousel/initcarousel-1.js"></script>
<script src="<?php echo base_url(); ?>design/frontend/js/amazingcarousel/initcarousel-3.js"></script>
<script type="text/javascript">
$(function() {
//          var $test ="<?php $this->uri->segment(1); ?>";
////          alert($test);
//          if($test==""){
    $(".loader").delay(9000).fadeOut("slow");
// setTimeout($(".loader"). 10000);
//    }
});

</script>
<!--<script src="<?php echo base_url(); ?>design/frontend/js/jquery-1.12.1.min.js"></script>-->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js">    
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php echo base_url(); ?>design/frontend/bootstrap/js/bootstrap.js"></script>

<script src="<?php echo base_url(); ?>design/frontend/js/easyResponsiveTabs.js"></script>
<!-- photo gallery -->
<!-- end photo gallery -->

<!-- end photo gallery -->
<!-- video gallery -->
<script src="<?php echo base_url(); ?>design/frontend/js/unitegallery/unitegallery.js"></script>
<script src="<?php echo base_url(); ?>design/frontend/js/unitegallery/ug-theme-video.js"></script>
<!-- end video gallery -->
<!-- event  -->

<!-- end event -->
<script src='<?php echo base_url(); ?>design/frontend/js/all.js'></script>


<script>
jQuery(function($) {
    $('.navbar .dropdown').hover(function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

  }, function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(500).slideUp();

  });

        // $('.navbar .dropdown > a').click(function(){
        //   location.href = this.href;
        // });

});
</script>

<script>
$(document).ready(function () {
    $("#send").click(function () {
//                                                                            alert ("test");
if ($('#name').val() == '')
{

    $('#name').css('border-color', '#FF0000');
    $('#name').focus();
    return false;
}

if ($('#message').val() == '')
{

    $('#message').css('border-color', '#FF0000');
    $('#message').focus();
    return false;
}
if ($('#email').val() == '')
{

    $('#email').css('border-color', '#FF0000');
    $('#email').focus();
//                                                                        alert ("failure");
return false;
}
else {
//                                                                         alert ("success");
subscribe();
}
});
});
function subscribe()

{
//        alert("testtt");
var name = $("#name").val();
var message = $("#message").val();
var email = $("#email").val();
//    alert("test");
$('#fpi_submit_submit input').attr('disabled', 'disabled');
if (grecaptcha.getResponse() == ""){
    alert("You can't proceed!") }
    else{
        $.ajax({
//        alert("test");
type: 'POST',
data: 'name=' + name + '&email=' + email + '&message=' + message,
url: <?php base_url() ?>'contact_us/submit',
                    //dataType: 'json',

                    success: function (data)
                    {

                        if (data == 'ok')
                        {
//                                                                                     alert ("pass");

$("#name").val('');
$("#email").val('');
$("#message").val('');
$("#failure").hide();
$("#success").show();
}
else {
//                                                                                     alert ("fail");

$("#failure").show();
$("#success").hide();
}
}
});
    }
}
function validateEmail(emailField){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField.value) == false)
    {
        alert('Invalid Email Address');
        return false;
    }

    return true;
}

</script>


<!--<script>
   if($('.alert').length){
       $('.').hide();
   }
</script>-->


</body>
</html>
