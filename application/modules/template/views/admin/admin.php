<!DOCTYPE html>
<html lang="en">  
    <head>  
        <meta charset="utf-8"> 	 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>      
        <title> Imagination Travel and Treks </title> 
        <!--dynamic module css-->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $favicon; ?>">
        <link href="<?php echo base_url(); ?>design/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/main.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/responsive.css" rel="stylesheet" type="text/css"/>  
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css">  
        <!--dynamic module js-->
        <script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-1.11.0.min.js"></script> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>design/admin/js/datepicker/css/datepicker.css" />
        <link rel="stylesheet/less" type="text/css" href="<?php echo base_url() ?>design/admin/js/datepicker/less/datepicker.less " />  
        <script src="<?php echo base_url() ?>design/admin/js/datepicker/js/bootstrap-datepicker.js"></script>
    </head> 

    <body>  
        <header  class=" header navbar-fixed-top" role="banner">  
            <div class="navbar">
                <div class="container">  
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">  
                        <img src="<?php echo base_url(); ?>design/admin/img/logo.png" alt="logo" style="width: 205px;"/> 
                    </a> 
                    <!-- <div class="col-md-6" style="margin-top:10px;">
                        <h2 class="heading">Imagination Travel and Treks </h2>
                    </div> -->


                    <!--    <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">  
                           <i class="icon-reorder"></i>  
                       </a>  --> 
                    <!--        <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">  
                               <li>  
                                   <a href="<?php echo base_url(); ?>admin/dashboard"> Dashboard </a>  
                               </li>  
                           </ul>   -->

                    <div class="user pull-right hidden-xs-480">
                        <div class="pull-left">
                            <img src="<?php echo base_url(); ?>design/admin/img/imagination-icon.png" alt="Profile Pic" />
                        </div>
                        <div class="pull-left">
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Hello <?php echo $user_display_name; //display name of a logged in user ?>
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo base_url(); ?>admin/dashboard/chpwd/<?php echo $user; ?>">Change Password</a></li>
                                  <li><a href="<?php echo base_url(); ?>admin_login/logout">Logout</a></li>
                                </ul>
                            </div>
                            <!-- <ul class="inline-ul floatleft"> -->
                                <!-- <li>Hello <?php echo $user_display_name; ?></li> -->

                                <!-- <li><a href="<?php echo base_url(); ?>admin/dashboard/chpwd/<?php echo $user; ?>">Change Password</a></li> -->
                                <!-- <li><a href="<?php echo base_url(); ?>admin_login/logout">Logout</a></li> -->
                            <!-- </ul> -->
                        </div>
                    </div>
                </div>   
            </div>
            <div class="menu">
                <div class="col-md-2 width19">
                    <a href="<?php echo base_url(); ?>admin/dashboard"><h2  class="hidden-xs hidden-sm"><img src="<?php echo base_url(); ?>design/admin/img/icon-dashboard.png" /> Dashboard</h2></a>
                </div>

                <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">  
                    <i class="icon-reorder"></i>  
                </a>  
                <!-- mobile view display username ends -->
                <div class="user pull-right visible-xs-480">
                       <!--  <div class="pull-left">
                            <img src="<?php echo base_url(); ?>design/admin/img/imagination-icon.png" alt="Profile Pic" />
                        </div> -->
                        <div class="pull-left">
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Hello <?php echo $user_display_name; //display name of a logged in user ?>
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo base_url(); ?>admin/dashboard/chpwd/<?php echo $user; ?>">Change Password</a></li>
                                  <li><a href="<?php echo base_url(); ?>admin_login/logout">Logout</a></li>
                                </ul>
                            </div>
                            <!-- <ul class="inline-ul floatleft"> -->
                                <!-- <li>Hello <?php echo $user_display_name; ?></li> -->

                                <!-- <li><a href="<?php echo base_url(); ?>admin/dashboard/chpwd/<?php echo $user; ?>">Change Password</a></li> -->
                                <!-- <li><a href="<?php echo base_url(); ?>admin_login/logout">Logout</a></li> -->
                            <!-- </ul> -->
                        </div>
                    </div>

                    <!-- mobile view display username ends -->

            </div>         
        </header>  

        <div id="container">  
            <div id="sidebar" class="sidebar-fixed">  
                <div id="sidebar-content">  
                    <ul id="nav">  
                        <li class="current">  
                            <a href="<?php echo base_url(); ?>admin/dashboard">  
                                <i class="icon-dashboard"></i> Dashboard  
                            </a>  
                        </li>  

                        <?php
                        if (!empty($sidebar)) {
                            foreach ($sidebar as $list) {
                                if (file_exists(APPPATH . "modules/" . $list . "/views/admin/sidebar.php")) {//this if statement is to check if there is a display page/file(i.e. sidebar.php) in admin view of any module
                                    $this->load->view($list . "/admin/sidebar"); //this is sidebar							
                                    //$sidebar = "$modulename/admin/sidebar"; //this is the format to load sidebar
                                    //the first part is modulename, second part is admin...which means admin folder from views folder and third is sidebar.php
                                }
                            }
                        }
                        ?>


                    </ul>   
                </div>  
                <div id="divider" class="resizeable"></div>  
            </div>  
            <div id="content">
                <div class="container">  
                    <div class="crumbs">  
                        <ul id="breadcrumbs" class="breadcrumb">  
                            <li>  
                                <i class="icon-home"></i>  
                                <a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a> 
                            </li>  
                            <?php
                            //making bread crumbs
                            if ($this->uri->segment(2) != 'dashboard') {
                                $breadcrumb_url = $this->uri->segment(2);
                                $breadcrumb_title = ucfirst($this->uri->segment(2));
                                $breadcrumb_new = str_replace("_"," ",$breadcrumb_title);
                                $breadcrumb_new_title = ucwords(strtolower($breadcrumb_new));

                                ?>
                                <li class="current"> 
                                    <a href="<?php echo base_url() . 'admin/' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_new_title; ?></a> 
                                </li> 							
                            <?php } ?>

                            <?php if ($this->uri->segment(3)) { ?>
                                <li class="current"> 
                                    <a><?php echo ucfirst($this->uri->segment(3)) ?></a> 
                                </li> 							
                            <?php }//end of breadcrumbs ?>

                        </ul>  
                       <!--  <ul class="crumb-buttons">  
                            <li> 
                                <a href="#" title=""> 
                                    <i class="icon-user"></i>
                                    <span>Hello, <?php echo $user_display_name; ?>!</span>
                                </a> 
                            </li>
                        </ul>  -->                   
                    </div> 

                    <div class="row"> 
                        <div class="col-md-12">  
                            <div style="padding-top:25px;"></div>
                            <!--this is where body part goes-->  

                            <?php
                            if (!isset($view_file)) {
                                $view_file = "";
                            }
                            if (!isset($module)) {
                                $module = $this->uri->segment(2);
                            }
                            if (($view_file != '') && ($module != '')) {
                                $path = $module . "/" . $view_file;
                                $this->load->view($path);
                            }
                            ?>     
                            <!--this is where body part ends--> 
                        </div>
                    </div>                       

                </div>  
            </div>  
        </div> 
        <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>  
        <!-- js for menu and breadcrum -->
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/libs/breakpoints.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/app.js"></script>  
        <script>
            $(document).ready(function () {
                App.init();
                Plugins.init();
                FormComponents.init()
            });
        </script>  
        <script>
            $('ul#nav  li ').click(function () {
                $('ul#nav').children().removeClass('current');
                $(this).addClass('current');
            });
        </script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/searchPagination.js"></script>
    </body>  
</html>