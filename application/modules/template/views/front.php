<!doctype html>
<html lang="en">
<head>
    <!-- meta tags and title goes here -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <title>Imagination Travel and Trek</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $favicon; ?>">
    <!-- Mobile Specific Metas here -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <!-- CSS & google font link goes here -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/bootstrap.min.css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai|Lato" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/dist/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/dist/css/simplelightbox.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- custom css added -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/scss/style.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/custom.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/dist/css/responsive.css" media="all">

    <script src="<?php echo base_url(); ?>design/frontend/dist/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>

<body>
        <!-- Page structure start 
        ================================================== -->
        <div class="wrapper">
            <a href="#" class="TopButton">
                <i class="fa fa-angle-up "></i>
            </a>
            <header class="header">

                <nav class="navbar navbar-default">
                    <div class="info">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php foreach ($contact as $front_contact) {?>

                                    <span class="hidden-xs">
                                        <?php list($contact1,$contact2) = explode('/', $front_contact['contact_no']); ?>
                                        <i class="fa fa-phone" aria-hidden="true"></i> <?php 
                                        echo $contact1;?>
                                        

                                    </span>
                                    
                                    <span class="hidden-xs">
                                        <a href="mailto:<?php echo $front_contact['email1']?>"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php  echo $front_contact['email1']?></a>
                                    </span>
                                    <?php  }?>


                                    <span class="pull-right"><a href="#" target="_blank"><span class="fa fa-instagram"></span></a></span>
                                    <span class="pull-right"><a href="https://www.facebook.com/imaginationtraveltrek" target="_blank"><span class="fa fa-facebook"></span></a></span>
                                    <span class="pull-right search-icon"><a href="#search"><span class="fa fa-search"></span></a></span>
                                </div>

                                <!-- Search Form -->
                                <div id="search"> 
                                    <span class="close">X</span>
                                    <?php echo form_open('search', 'method="post" class="navbar-form pull-right" id="searchform"'); ?>
                                    <!-- <form role="search" id="searchform" action="/search" method="get"> -->
                                    <input value="" name="q" type="search" placeholder="type to search"/>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">

                        <div class="logo-wrap">
                            <h1 class="logo clearfix">
                                <a href="<?php echo base_url(); ?>">
                                    <img src="<?php echo base_url(); ?>uploads/settings/logo.png" alt="SAARC-logo" class="img-responsive">
                                </a>  
                            </h1>

                        </div>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse main-nav">
                            <ul class="nav navbar-nav navbar-right">
                                <?php foreach ($headernav as $header) {//still need to work on slug  ?> 
                                <?php
                                if (empty($header['children'])) {
                                    echo '<li>'; /* this is for no-child */
                                } else {
                                    ?>                      
                                    <li  class="dropdown">
                                        <?php }/* this is for dropdown */ ?>

                                        <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                        <?php
                                        if ($header['navtype'] == 'URL') {
                                            $prefix = '';
                                        } else {
                                            $prefix = base_url();
                                        }
                                        ?>
                                        <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?> <span class="caret"></span>                                  
                                            <!-- <b class="caret"></b>                                 -->
                                            <?php }/* end of if */ else { /* this is for no-child */
                                                ?>                            
                                                <?php
                                                if ($header['navtype'] == 'URL') {
                                                    $prefix = '';
                                                } else {
                                                    $prefix = base_url();
                                                }
                                                ?>
                                                <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
                                                    <?php } ?>                           
                                                </a>

                                                <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                                <ul class="dropdown-menu">
                                                    <?php foreach ($header['children'] as $child) { ?>
                                                    <!--code for grandchild by harry--> 
                                                    <?php if (!empty($child['grand_children'])) { ?>
                                                    <li class="dropdown dropdown-submenu">                                            
                                                        <?php
                                                        if ($child['navtype'] == 'URL') {
                                                            $prefix = '';
                                                        } else {
                                                            $prefix = base_url();
                                                        }
                                                        ?>
                                                        <a class="dropdown-toggle" href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>

                                                        <ul class="dropdown-menu" style="background:#fcfcfc;" padding:10px;>
                                                            <?php foreach ($child['grand_children'] as $grandchild) { ?>
                                                            <li >                                            
                                                                <?php
                                                                if ($grandchild['navtype'] == 'URL') {
                                                                    $prefix = '';
                                                                } else {
                                                                    $prefix = base_url();
                                                                }
                                                                ?>
                                                                <a href="<?php echo $prefix . $grandchild['href']; ?>" target="<?php echo $grandchild['target'] ?>"><?php echo $grandchild['title']; ?></a>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <!--end  of code for grandchild by harry--> 
                                                    <?php } else { ?> <!--this else part if grand child doesnot exist-->
                                                    <li>                                            
                                                        <?php
                                                        if ($child['navtype'] == 'URL') {
                                                            $prefix = '';
                                                        } else {
                                                            $prefix = base_url();
                                                        }
                                                        ?>
                                                        <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                                    </li>
                                                    <?php }/* closing of else part if grand child doesnot exist */ ?>
                                                    <?php }/* end of child foreach */ ?>
                                                </ul>
                                                <?php } ?>  

                                            </li>
                                            <?php }/* end of parent foreach */ ?>
                                        </ul>

                                    </div><!-- /.navbar-collapse -->
                                </div>
                            </nav>
                        </header>
                        <?php
                        $first_bit = $this->uri->segment(1);
                        if ($first_bit == "" || $first_bit == "home") {
                            ?>

                            <div class="fluid_container">
                                <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
                                    <?php $active = 'active'; ?>
                                    <?php foreach ($banners as $row) { ?> 
                                    <?php $urls = $_SERVER['DOCUMENT_ROOT']; ?>

                                    <?php if (file_exists($urls . "/imagination_git/uploads/sightseeing/" . $row['attachment'])) { ?>
                                    <div data-src="<?php echo base_url(); ?>uploads/sightseeing/<?php echo $row['attachment'] ?>" data-link="<?php echo base_url() ?>trekking/details/<?php echo $row['slug'] ?>">
                                        <img src="" alt="">
                                        <div class="camera_caption fadeFromBottom">
                                            <a href="<?php echo base_url() ?>sightseeing/details_more/<?php echo $row['slug'] ?>"><?php echo $row['title']; ?></a> 

                                        </div>
                                    </div>
                                    <?php } else {
                                        ?>
                                        <div data-src="<?php echo base_url(); ?>uploads/trekking/<?php echo $row['attachment'] ?>" data-link="<?php echo base_url() ?>trekking/details/<?php echo $row['slug'] ?>">
                                            <img src="" alt="">
                                            <div class="camera_caption fadeFromBottom">
                                                <a href="<?php echo base_url() ?>trekking/details/<?php echo $row['slug'] ?>"><?php echo $row['title']; ?></a> 
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    $active = '';
                                }
                                ?>  

                            </div><!-- #camera_wrap_1 -->
                        </div>
                        <div class="clearfix"></div>
                        <!-- welcome -->
                        <div class="about-us">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 welcome-txt">
                                        <?php foreach ($about as $row) { ?>
                                        <h2><?php echo $row['title']; ?> </h2>
                                        <p><?php echo $row['description']; ?></p>
                                        <a href="<?php echo base_url() ?>welcome-to-imagination-trek" class="btn btn-success">Read More</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- service -->
                        <div class="service">
                            <div class="container">
                                <div class="row">
                                    <h2 class="text-center">Our service</h2>
                                    <?php foreach ($services as $row) { ?>
                                    <div class="col-sm-4">
                                        <div class="box">
                                            <?php
                                            $title = strtolower($row['title']);
                                            $title1 = str_replace(' ', '_', $title);
                                            ?>
                                            <a href="<?php echo base_url() ?><?php echo $title1; ?>">
                                                <figure>
                                                    <img src="<?php echo base_url(); ?>uploads/services/<?php echo $row['attachment'] ?>" class="img-responsive"/>
                                                    <figcaption><?php echo $row['title'] ?></figcaption>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>
                                    <?php } ?>


                                </div>
                            </div>
                        </div>
                        <!-- testimonial -->
                        <div class="testimonial">
                            <div class="bg">
                                <div class="container">
                                    <h2 class="text-center">Testimonial</h2>
                                    <div class="testimonial-list">
                                        <?php foreach ($testimonial as $row) { ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <figure>
                                                    <img src="<?php echo base_url(); ?>uploads/testimonial/<?php echo $row['attachment']; ?>" class="img-responsive">
                                                </figure>
                                            </div>
                                            <div class="col-md-8">
                                                <p>
                                                    <?php echo $row['description'] ?>
                                                </p>
                                                <span> - &nbsp<?php echo $row['author'] ?></span>
                                            </div>


                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } else {
                            ?>
                            <!-- start of the content ------------------------------------------>
                            <div class="content-wrap inner clearfix">
                                <?php
                                if (isset($click)) {

                                    $slug = $this->uri->segment(1);
                        // var_dump($slug);die;
                                    ?>   

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="breadcrumb_div hidden-xs">  
                                                    <ul id="breadcrumbs" class="breadcrumb">  

                                                        <li class="breadcrumb-item">  
                                                            <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                                                <span class="arrow"></span></a>

                                                            </li>
                                                            <li class="current"> 
                                                                <?php
                                                //making bread crumbs

                                                                if ($this->uri->segment(1) != '') {

//                        
                                                                    $breadcrumb_url = $this->uri->segment(1);
                                                                    $breadcrumb_title = ucfirst($title);
                                                                    echo $breadcrumb_title;
                                                                    ?>  

                                                                    <?php
                                                                }
                                                                ?>
                                                            </li>
                                                            <?php
                                                            if ($this->uri->segment(2)) {
//                         
                                                                ?>
                                                                <li class="breadcrumb-item current"> 
                                                                    <a class="odd"><?php echo ucfirst($this->uri->segment(2)) ?>
                                                                    </a>
                                                                </li>   
                                                                <?php }//end of breadcrumbs    ?>


                                                            </ul>  
                                                        </div>
                                                        <section>
                                                            <div>
                                                                <?php
                                                                if (!isset($view_file)) {
                                                                    $view_file = "";
                                                                }
                                                                if (!isset($module)) {
                                                                    $module = $this->uri->segment(1);
                                                                }
                                                                if (($view_file != '') && ($module != '')) {
                                                                    $path = $module . "/" . $view_file;
                                                                    $this->load->view($path);
                                                                } else {
                                                                    ?>

                                                                    <div>
                                                                        <h2>
                                                                            <?php
                                                                            $new_title = str_replace("../../../", "./", $title);
                                                                            $title_new = str_replace('-', ' ', $new_title);
                                                                            echo ucfirst($title_new);
                                                                            ?>
                                                                        </h2>
                                                                    </div>
                                                                    <div class="clearfix"></div>

                                                                    <div style="text-align: justify" class="row">
                                                                        <?php if ($this->uri->segment(1) == 'welcome-to-imagination-trek') { ?>

                                                                        <div class="col-sm-8 welcome-txt">
                                                                            <?php
                                                            $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                                            echo ($new_description); //put your designs here
                                                            ?>  </div>
                                                            <div class="col-sm-4 image">
                                                                <?php if (($option) == 'yes' && $attachment != '') { ?>
                                                                <figure>
                                                                    <img src="<?php echo base_url(); ?>uploads/pages/<?php echo $attachment ?>" class="img-responsive">
                                                                </figure>
                                                                <?php }
                                                                ?>
                                                            </div>   

                                                            <?php } else {
                                                                ?>
                                                                <div class="col-md-12 welcome-txt">
                                                                    <?php
                                                            $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                                            echo ($new_description); //put your designs here
                                                            ?>  </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!isset($click)) {
                            ?>  

                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section>
                                            <div class="breadcrumb_div hidden-xs">  
                                                <ul id="breadcrumbs" class="breadcrumb">  
                                                    <li>  
                                                        <a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>
                                                            <span class="arrow"></span></a>

                                                        </li>

                                                        <!-- //making bread crumbs -->
                                                        <?php
                                                        if ($this->uri->segment(3)) {
                                                            if ($this->uri->segment(1) == 'sightseeing' && $this->uri->segment(4)) {
                                                                ?>
                                                                <li>
                                                                    <?php
                                                                    $sec_breadcrumb = $this->uri->segment(1);
                                                                    $sec_title = ucfirst(str_replace('_', ' ', $sec_breadcrumb));
                                                                    ?>
                                                                    <a href="<?php echo base_url(); ?><?php echo $sec_breadcrumb ?>"><?php echo $sec_title; ?></i>
                                                                        <span class="arrow"></span></a>

                                                                    </li>
                                                                    <li>
                                                                        <?php
                                                                        $first_breadcrumb = $this->uri->segment(4);

                                                                        $sec_title = ucfirst(str_replace('_', ' ', $first_breadcrumb));
                                                                        ?>
                                                                        <a href="<?php echo base_url(); ?>sightseeing/details/<?php echo $first_breadcrumb ?>"><?php echo $sec_title; ?></i>
                                                                            <span class="arrow"></span></a>

                                                                        </li>

                                                                        <li class="current"> 
                                                                            <a><?php
                                                                            $new_title = $this->uri->segment(3);
                                                                            $breadcrumb_title1 = ucfirst(str_replace('_', ' ', $new_title));
                                                                            $breadcrumb_title = ucfirst(str_replace('-', ' ', $breadcrumb_title1));
                                                                            echo $breadcrumb_title;
                                                                            ?>
                                                                        </a>
                                                                    </li> 
                                                                    <?php } else { ?>


                                                                    <li>
                                                                        <?php
                                                                        $sec_breadcrumb = $this->uri->segment(1);
                                                                        $sec_title = ucfirst(str_replace('_', ' ', $sec_breadcrumb));
                                                                        ?>
                                                                        <a href="<?php echo base_url(); ?><?php echo $sec_breadcrumb ?>"><?php echo $sec_title; ?></i>
                                                                            <span class="arrow"></span></a>

                                                                        </li>


                                                                        <li class="current"> 
                                                                            <a><?php
                                                                            $new_title = $this->uri->segment(3);
                                                                            $breadcrumb_title1 = ucfirst(str_replace('_', ' ', $new_title));
                                                                            $breadcrumb_title = ucfirst(str_replace('-', ' ', $breadcrumb_title1));
                                                                            echo $breadcrumb_title;
                                                                            ?>
                                                                        </a>
                                                                    </li> 
                                                                    <?php } ?>

                                                                    <?php } else { ?>
                                                                    <li class="current"> 
                                                                        <?php
                                                                        $new_title = $this->uri->segment(1);
                                                                        $breadcrumb_title1 = ucfirst(str_replace('-', ' ', $new_title));
                                                                        $breadcrumb_title = ucfirst(str_replace('_', ' ', $breadcrumb_title1));
                                                                        echo $breadcrumb_title;
                                                                        ?>
                                                                    </li>

                                            <?php } //end of breadcrumbs
                                            ?>


                                        </ul>  
                                    </div>

                                    <div class="inner-description">
                                        <?php
                                        if (!isset($view_file)) {
                                            $view_file = "";
                                        }
                                        if (!isset($module)) {
                                            $module = $this->uri->segment(1);
                                        }
                                        if (($view_file != '') && ($module != '')) {
                                            $path = $module . "/" . $view_file;
                                            $this->load->view($path);
                                        } else {
                                            $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                            echo ($new_description); //put your designs here
                                        }
                                        ?>
                                    </div>

                                </section> 
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>                    

                    <?php } ?>
                    <!-- footer wrap -->
                </div> <!-- /.content-wrap end -->

                <div class="footer-top clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="row">
                                    <div class="col-sm-7 col-md-6">
                                        <h4>Service</h4>
                                        <ul class="links">
                                            <?php foreach ($services as $row) { ?>
                                            <?php
                                            $title = strtolower($row['title']);
                                            $title1 = str_replace(' ', '_', $title);
                                            ?>
                                            <li>
                                                <a href="<?php echo base_url() ?><?php echo $title1; ?>">
                                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $row['title']; ?>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-sm-5 col-md-6">
                                        <h4>We Associate</h4>
                                        <ul class="partners">
                                            <li>
                                                <img src="<?php echo base_url() ?>design/frontend/image/partners/ntb.jpg">
                                            </li>
                                            <li>
                                                <img src="<?php echo base_url() ?>design/frontend/image/partners/nepal.png">
                                            </li>
                                            <li>
                                                <img src="<?php echo base_url() ?>design/frontend/image/partners/lal.jpg">
                                            </li>
                                        </ul>
                                        <h3 class="we-accept">We Accept</h3>
                                        <ul class="we-accept-list">
                                            <li>
                                                <img src="<?php echo base_url() ?>design/frontend/image/partners/visa.jpg">
                                            </li>
                                            <li>
                                                <img src="<?php echo base_url() ?>design/frontend/image/partners/mastercard.png">
                                            </li>
                                            <li>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="row">
                                    <div class="col-sm-5 contact">
                                        <h4>Contact Us</h4>
                                        <?php foreach ($contact as $row) { ?>
                                        <address class="address">
                                            <span><i class="fa fa-map-marker">&nbsp;</i> <?php echo $row['address']; ?></span>
                                            <?php  list($contact2,$contact3) = explode('/', $row['contact_no']);  ?>
                                            <span><i class="fa fa-phone">&nbsp;</i> <?php echo $contact2; ?></span>
                                            <span><i class="fa fa-phone">&nbsp;</i> <?php echo $contact3; ?></span>
                                            <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank"><?php echo $row['email']; ?></a></span>
                                            <span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank"><?php echo $row['email1']; ?></a></span>
                                        </address>
                                        <?php } ?>

                                    </div>
                                    <div class="col-sm-7 sign-up">
                                        <h4>Newsletter Sign Up</h4>
                                        <p>Enter your email for getting updates on new packages </p>
                                        <div class="row">
                                            <div class="col-sm-7 padding-right">
                                                <input type="email" name="" placeholder="Email Address" id="email" >
                                            </div>
                                            <div class="col-sm-5">
                                                <a id="submit_front"  class="btn" >Sign Up Now</a>
                                            </div>
                                            <div id="correct" class="correctMsg"></div>
                                            <div id="incorrect" class="incorrectMsg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>



                <!-- footer wrap -->
                <footer class="footer-wrap pad clearfix">
                    <div class="footer-btm clearfix">
                        <div class="container">
                            <div class="copyright">
                                <p>&copy; <?php echo date("Y"); ?> Imagination Travel & Trek Pvt.Ltd. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div> <!-- /.wrapper end -->
    <!-- Page structure end 
    ================================================== -->
    <!-- core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/modernizr.js"></script>

    <script src="<?php echo base_url(); ?>design/frontend/dist/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/responsiveTabs.js"></script>
    <!-- lightbox -->
    <script src="<?php echo base_url(); ?>design/frontend/dist/js/simple-lightbox.js"></script>
    <script>
    $(function () {
        var $gallery = $('.gallery a').simpleLightbox();
    });
    </script> 
    <!-- end lightbox -->
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='<?php echo base_url(); ?>design/frontend/dist/js/camera.js'></script> 

    <script src="<?php echo base_url(); ?>design/frontend/dist/js/custom.js"></script>

    <script type="text/javascript">
    $('.testimonial-list').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        autoplay: false,
        navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $('.hotel-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        autoplay: true,
        navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    </script>
    <script>
    jQuery(function () {
        jQuery('#camera_wrap_1').camera({
            loader: 'bar',
            pagination: false,
            playPause: true,
            pauseOnClick: false,
            navigationHover: true,
            hover: false
        });
    });
    </script>

    <script>

    $(document).ready(function() {
        $("#submit_front").click(function() {
            $("#incorrect").html(" ");
            $("#correct").html(" ");
            var base_url = ' <?php echo base_url(); ?>template/submit';
            var email = $("#email").val();
            var test_case = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (email.match(test_case)) {

                $.ajax({
                    type: 'POST',
                    url: base_url,
                    data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", "email": email},
                    success: function (data) {
                        $('#correct').html("Thank you for registering");
                            // $("#correct").css("color", "green");
                            $('#correct').val('');
                        }
                    });

            } else{

                $("#incorrect").html("Incorrect email Format")
                    // $("#incorrectMsg").css("color", "red")
                    $("#incorrectMsg").val('');

                }
            });
});


        // dropdown animation    
        $(function () {
            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                });
        });
        // drodown animation ends

        </script>
        <!--  dropdown design     -->
        <script type="text/javascript">
        $('select[name*="country"]').removeAttr('multiple')
        </script>
        <!--  dropdown design ends  -->
        <script>
        $('.datepicker input').datepicker({
            autoclose:true
        });
        </script>
    </body>
    </html>
