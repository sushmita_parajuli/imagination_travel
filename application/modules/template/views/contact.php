<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class="content-wrap pad inner clearfix">
	<div class="container">
		<div class="contact-wrap">
			<!-- contact details -->
			<div class="contact-holder clearfix">
				<div class="row">
					<div class="col-sm-7 map-wrap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56516.27689201643!2d85.29111313687447!3d27.709031933645715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a307baabf%3A0xb5137c1bf18db1ea!2z4KSV4KS-4KSg4KSu4KS-4KSh4KWM4KSBIDQ0NjAw!5e0!3m2!1sne!2snp!4v1498213612709" width="600" height="570" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="col-sm-5 contact-details">
						<div class="details-wrap clearfix">
							<h3>Contact Detail</h3>
							<address class="address">
								<span><i class="fa fa-map-marker">&nbsp;</i> Shankhamul Marga, New Baneshwor, Kathmandu, Nepal</span>
								<span><i class="fa fa-phone">&nbsp;</i> +977 4788600</span>
								<span><i class="fa fa-phone">&nbsp;</i> +977 9861829863</span>
								<span><i class="fa fa-envelope-o">&nbsp;</i> <a href="mailto:info@gmail.com">info@royaltaasghar.com.np</a></span>
							</address>
						</div>
						<div class="contact-form-wrap">
							<div class="contact-form clearfix">
								<p>Fill the form below to contact with us.</p>
								<form>
									<div class="row">
										<div class="form-group col-sm-6">
											<label for="name">Name</label>
											<input type="text" class="form-control" id="name" placeholder="Your Full Name">
										</div>
										<div class="form-group col-sm-6">
											<label for="phone">Phone</label>
											<input type="text" class="form-control" id="phone" placeholder="Your Phone Number">
										</div>
										<div class="form-group col-sm-12">
											<label for="email">Email</label>
											<input type="email" class="form-control" id="email" placeholder="Your email address">
										</div>
										<div class="form-group col-sm-12">
											<label for="message">Message</label>
											<textarea class="form-control" id="message" cols="30" rows="3"></textarea>
										</div>
									</div>
									<div class="btn-wrap">
									<button class="btn btn-success" type="submit">Send</button>
									</div>
								</form>
							</div>
						</div>
					</div> <!-- /.contact-details -->
					
				</div>
			</div>
		</div>
	</div>
</div> <!-- /.content-wrap end -->
<?php include 'footer.php'; ?>