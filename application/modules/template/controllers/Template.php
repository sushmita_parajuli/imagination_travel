<?php //

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template extends MX_Controller {

    function __construct() {
        parent::__construct();

    }

    function front($data){

        $todays_date=date('Y-m-d');
        $nav = $this->session->userdata('navtype');
        $page = $this->get_page_live();
        $data['banners'] = $this->get_banner();
        $data['testimonial'] = $this->get_testimonial();
        $data['about']=$this->get_about();
        $data['services']= $this->get_services();
        $data['contact']= $this->get_contact();
        // var_dump($data['contact']);die;
        $data['favicon'] = $this->get_attachment('favicon');
        $headernav = $this->all_nav('1'); 
        $data['headernav']=$headernav;
        $data['footernav'] = $this->get_footer('2'); //similar as above but 2 is for footer nav
        $data['site_settings'] = $this->get_site_settings();
        $slug = $this->uri->segment(1);
        $parent_slug=$this->get_parent_slug($slug);/*to get parent slug*/
        if($parent_slug!='')
        {
            $slug=$parent_slug[0]['slug'];

            $data['parent_title']=$parent_slug[0]['title'];
        }
        $this->load->view('front', $data);
    }
    function get_parent_slug($slug)
    {
        $this->load->model('navigation/Mdl_navigation');
        $query = $this->Mdl_navigation->get_parent_slug($slug);
        return $query;
    }
    function all_nav($group_id) {//to get navigation list of specific group
        $data['parentnav'] = $this->get_parent($group_id); //
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
           $children = $this->get_child($group_id, $nav['id']);
           if ($children != NULL) {
            $nav['children'] = $children;
        }
        $navigation[$i] = $nav;
        $i++;
    }
    return $navigation;
}


function get_site_settings() {
    $this->load->model('settings/mdl_settings');
    $query = $this->mdl_settings->get_settings();
    $result = $query->result_array();
    return $result[0];
}


function get_navigation_from_navigation_name($navigation_name) {
    $this->load->model('navigation/mdl_navigation');
    $query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
    $result = $this->add_href($query->result_array());
    return $result;
}

function errorpage() {
    $this->load->view('404');
}

function userlogin($data) {
    $this->load->view('userlogin', $data);
}

function get_banner() {
    $this->load->model('banner/mdl_banner');
    $query = $this->mdl_banner->get_banner();
    return $query->result_array();
}

function get_testimonial() {
    $this->load->model('testimonial/Mdl_testimonial');
    $query = $this->Mdl_testimonial->get_testimonial();
    // var_dump($query->result());die;
    return $query->result_array();
}

function get_about() {

    $this->load->model('pages/Mdl_pages');
    $query = $this->Mdl_pages->get_about_us();
//        var_dump($query->result());die;

    return $query->result_array();
}

function get_services() {
    $this->load->model('services/Mdl_services');
    $query = $this->Mdl_services->get_services();
    return $query->result_array();
}
function get_contact() {
    $this->load->model('contact_us/Mdl_contact_us');
    $query = $this->Mdl_contact_us->get_contact_us();
    return $query->result_array();
}

function get_parent($group_id) {
    $this->load->model('navigation/mdl_navigation');
    $query = $this->mdl_navigation->get_parentnav($group_id);
    $result = $this->add_href($query);
    return $result;
}
function get_child($group_id, $parent_id) {
    $this->load->model('navigation/mdl_navigation');
    $query = $this->mdl_navigation->get_childnav($group_id, $parent_id);
//        var_dump($query);die;
    $result = $this->add_href($query);
    $result = $this->add_hrefgrand($result);

    return $result;
}

function add_href($result) {
    $count = count($result);
//        var_dump($count);die;
    for ($i = 0; $i < $count; $i++) {
        if ($result[$i]['navtype'] == 'Module') {
            $result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);
            $result[$i]['target'] = "_self";
        } elseif ($result[$i]['navtype'] == 'Page') {
            $result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);
            $result[$i]['target'] = "_self";
        } elseif ($result[$i]['navtype'] == 'URI') {
            $result[$i]['href'] = $result[$i]['site_uri'];
            $result[$i]['target'] = "_self";
        } else {
            $result[$i]['href'] = $result[$i]['link_url'];
            $result[$i]['target'] = "_blank";
        }
    }
//        var_dump($result);die;
    return $result;
}

function add_hrefgrand($result) {

    $i = 0;
    if (isset($result)) {
        foreach ($result as $sub_child) {
            $grand_children = $this->get_grandchild('1', $sub_child['id']);
            if ($grand_children != NULL) {
                $result[$i]['grand_children'] = $grand_children;
            }

            $i++;
        }
    }

    return $result;
}



function get_header($group_id) {
        $data['parentnav'] = $this->get_parent($group_id); //
//                var_dump($data['parentnav']);die;
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
            $children = $this->get_child($group_id, $nav['id']);
            if ($children != NULL) {
                $nav['children'] = $children;
            }
            $navigation[$i] = $nav;
            $i++;
        }
        return $navigation;
    }

    function get_footer($group_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_footernav($group_id);
        $result = $this->add_href($query->result_array());
        return $result;
    }



    function get_name_from_module($module_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_module($module_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_name_from_page($page_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_page($page_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_page_live() {
        $this->load->model('pages/mdl_pages');
        $query = $this->mdl_pages->get_page_live();
        return $query->result_array();
    }

    function get_metadata_search_keys($module_name, $slug) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_keys($module_name, $slug);
        return $query->result_array();
    }

    function get_metadata_search_module($module_name) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_module($module_name);
        return $query;
    }
    
    function get_grandchild($group_id, $parent_id) {

        $this->load->model('navigation/mdl_navigation');
        $query = $this->mdl_navigation->get_childnav($group_id, $parent_id);

        $result = $this->add_href($query);
        return $result;
    }
    function get_attachment($name) {
        $this->load->model('settings/mdl_settings');
        $query = $this->mdl_settings->get_where_dynamic('', $name);
        $result = $query->result_array();
        return $result[0][$name];
    }


    function submit(){
        $data['email'] = $this->input->post('email',TRUE);
        if($data['email']!=''){
            $this->load->model('Mdl_template');
            $this->Mdl_template->insert($data);
        }
        

    }
}
