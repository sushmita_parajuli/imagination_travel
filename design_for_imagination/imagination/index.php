<?php include 'header.php'; ?>
<div class="content-wrap clearfix">
	<!-- banner starts-->
</div><!-- #back_to_camera -->

<div class="fluid_container">
	<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
		<div data-src="image/slider/slider1.jpg">
			<img src="" alt="">
			<div class="camera_caption fadeFromBottom">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua.
			</div>
		</div>
		<div data-src="image/slider/slider2.jpg">
			<div class="camera_caption fadeFromBottom">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua.
			</div>
		</div>
		<div data-src="image/slider/slider3.jpg">
			<div class="camera_caption fadeFromBottom">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua.
			</div>
		</div>
		
		
	</div><!-- #camera_wrap_1 -->
</div>
<div class="clearfix"></div>
<!-- welcome -->
<div class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 welcome-txt">
				<h2>Imagination Travel and Trek</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis voluptatem quisquam provident enim voluptate aspernatur possimus animi incidunt cum alias molestias unde rem, magni neque ullam nam ut, quasi deleniti.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, animi beatae fugiat est impedit nostrum quas rem vero! Delectus blanditiis aspernatur optio veritatis repellat cum quo quidem sed iste culpa.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem dicta non error id hic et recusandae. Repellat velit repellendus optio distinctio, sed, dignissimos quidem dolor reiciendis voluptates corrupti explicabo, ipsam!</p>
				<a href="about.php" class="btn btn-success">Read More</a>
			</div>
		</div>
	</div>
</div>
<!-- service -->
<div class="service">
	<div class="container">
		<div class="row">
			<h2 class="text-center">Our service</h2>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/delux.jpg" class="img-responsive"/>
							<figcaption>Hotel Booking</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/img10.jpg" class="img-responsive"/>
							<figcaption>Treaking</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/1132.jpg" class="img-responsive"/>
							<figcaption>Mountain Flight</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/delux.jpg" class="img-responsive"/>
							<figcaption>Sight Seeing</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/img10.jpg" class="img-responsive"/>
							<figcaption>Transport</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="booking.php">
						<figure>
							<img src="image/service/1132.jpg" class="img-responsive"/>
							<figcaption>Ticketing</figcaption>
						</figure>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- testimonial -->
<div class="testimonial">
	<div class="bg">
		<div class="container">
			<h2 class="text-center">Testimonial</h2>
			<div class="testimonial-list">
				<div class="row">
					<div class="col-md-4">
						<figure>
							<img src="image/testimonials/img04.jpg" class="img-responsive">
						</figure>
					</div>
					<div class="col-md-8">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<span>- Joe Dhoe</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<figure>
									<img src="image/testimonials/img04.jpg" class="img-responsive">
								</figure>
							</div>
							<div class="col-md-8">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
										<span>- Joe Dhoe</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- footer wrap -->
				<div class="footer-top clearfix">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<h4>Service</h4>
								<ul class="links">
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hotel Booking</a></li>
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Treking</a></li>
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Transportation</a></li>
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ticketing</a></li>
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mountain Flight</a></li>
									<li><a href=""><i class="fa fa-angle-double-right" aria-hidden="true"></i> Shightseeing</a></li>
								</ul>
							</div>
							<div class="col-sm-3 contact">
								<h4>Contact Us</h4>
								<address class="address">
									<span><i class="fa fa-map-marker">&nbsp;</i> Pulchowk, Lalitpur, Nepal</span>
									<span><i class="fa fa-phone">&nbsp;</i> +977 015549558</span>
									<span><i class="fa fa-phone">&nbsp;</i> +977 9818417966</span>
									<span><i class="fa fa-envelope">&nbsp;</i> <a href="mailto:mail@gmail.com" target="_blank">imaginationtraveltrek@gmail.com</a></span>
								</address>
							</div>
							<div class="col-sm-5 sign-up">
								<h4>Newsletter Sign Up</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<div class="row">
									<div class="col-sm-7">
										<input type="email" name="" placeholder="Email Address">
									</div>
									<div class="col-sm-5">
										<a href="" class="btn">Sign Up Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



			</div> <!-- /.content-wrap end -->
			<?php include 'footer.php'; ?>