<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class="content-wrap pad inner clearfix">
	<!-- service -->
	<div class="clearfix">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Treking</a></li>
				<li class="breadcrumb-item active">Ghorepani Poon Hill</li>
			</ol>
			<div class="row">
				<div class="clearfix">
					<!-- 	<div class="col-sm-12">
								<figure class="photo">
									<img class="img-responsive" src="image/service/img10.jpg" alt="">
								</figure>
							</div> -->
							<div class="col-sm-12">
								<div class="trek-description clearfix">
									<figure class="photo">
										<img class="img-responsive" src="image/service/img10.jpg" alt="">
									</figure>
									<div class="title">
										<h2>Ghorepani Poon Hill Trekking - Details</h2>
									</div>
									<div class="cont">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
												consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
												cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
												proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
											</div>

											
										</div>
									</div>
									<div class="col-sm-8">
										<div class="responsive-tabs itinerary">
											<h2>Itinerary</h2>
											<div>

												<ul>
													<li>
														Day 01: Arrival in Kathmandu
													</li>
													<li>
														Day 02: Kathmandu sightseeing
													</li>
													<li>
														Day 03: Drive to Pokhara
													</li>
													<li>
														Day 04: Drive to Nayapul and Trek to Tikhedhunga
													</li>
													<li>
														Day 05: Tikhedunga – Ghorepani
													</li>
													<li>
														Day 06: Ghorepani – Poon Hill – Tadapani
													</li>
													<li>
														Day 07: Tadapani – Ghandrunk
													</li>
													<li>
														Day 08: Ghandruk – Nayapul – Pokhara
													</li>
													<li>
														Day 09: Drive back to Kathmandu
													</li>
													<li>
														Day 10: Back to your destination
													</li>
												</ul>
											</div>

											<h2>Map</h2>
											<div>
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3509.622259796673!2d83.69187431467418!3d28.40047498250986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995fb974774e9cb%3A0x36846e2b0a54a8d2!2sPoon+Hill+Marga%2C+Ghode+Pani+33200!5e0!3m2!1sen!2snp!4v1502518406606" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
											</div>
											<h2>Gallary</h2>
											<div>

											</div>
										</div>
									</div>
									<div class="col-md-4 trek-package">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">Treking Activities</h3>
											</div>
											<div class="panel-body">
												<ul class="activities-links">
													<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mustang Treking</a></li>
													<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Poon Hill Treking</a></li>
													<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sagarmatha Treking</a></li>
													<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ghandruk Treking</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="book-form-wrap col-sm-10 col-sm-offset-1">
										<div class="book-form-holder">
											<div class="heading">
												<h2 class="text-center">Book Now</h2>
											</div>
											<div class="form-wrapper clearfix">
												<div class="col-sm-12 holder">
													<form action="">
														<div class="col-sm-4">
															<h4 class="col-sm-12">Package Info</h4>
															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="name">Name</label> -->
																<div class="col-sm-12">
																	<input type="text" class=" form-control" name="package name" placeholder="package name">
																</div>
															</div>
															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="name">Email</label> -->
																<div class="col-sm-12">
																	<input type="email" class="form-control" name="dob" placeholder="Start Date">
																</div>

															</div>
															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="phone">Phone</label> -->
																<div class="col-sm-12">
																	<input type="text" class="form-control" name="country" placeholder="End Date">
																</div>
															</div>
															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="name">Name</label> -->
																<div class="col-sm-12">
																	<input type="text" class=" form-control" name="citizen no" placeholder="Number of People">
																</div>
															</div>
														</div>
														<div class="col-sm-8">
															<h4 class="col-sm-12">Personal Info</h4>
															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="name">Name</label> -->
																<div class="col-sm-12">
																	<input type="text" class=" form-control" name="name" placeholder="Your Full Name">
																</div>
															</div>

															<div class="form-group clearfix">
																<!-- <label class="col-sm-2" for="phone">Phone</label> -->
																<div class="col-sm-12">
																	<input type="text" class="form-control" name="country" placeholder="Enter Your Country">
																</div>
															</div>
															<div class="form-group col-sm-6">
																<!-- <label class="col-sm-2" for="name">Email</label> -->
																<div class="">
																	<input type="email" class="form-control" name="dob" placeholder="Your DOB">
																</div>

															</div>
															<div class="form-group col-sm-6">
																<!-- <label class="col-sm-2" for="name">Name</label> -->
																<div class="">
																	<input type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
																</div>
															</div>
															<div class="form-group col-sm-6">
																<!-- <label class="col-sm-2" for="name">Email</label> -->
																<div class="">
																	<input type="email" class="form-control" name="email" placeholder="Your Email Address">
																</div>

															</div>
															<div class="form-group col-sm-6">
																<!-- <label class="col-sm-2" for="phone">Phone</label> -->
																<div class="">
																	<input type="text" class="form-control" name="phone" placeholder="Your Number">
																</div>
															</div>
															<div class="col-sm-12">
																<input type="checkbox" name="vehicle1" value="Bike"> I agree all terms and condition<br>
															</div>
																<div class="btn-wrap col-sm-12">
															<button type="submit" class="btn btn-success">Submit</button>
														</div>
														</div>

													
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div> <!-- /.service end -->
				</div> <!-- /.content-wrap end -->
				<?php include 'footer.php'; ?>