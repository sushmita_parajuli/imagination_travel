// Scroll to Top Button
jQuery(document).ready(function(){
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 100) {
			jQuery('.TopButton').fadeIn();
		} else {
			jQuery('.TopButton').fadeOut();
		}
	});
	jQuery('.TopButton').click(function(){
		jQuery('html, body').animate({scrollTop : 0},800);
		return false;
	});
});

// full screen search
$(function () {
	$('a[href="#search"]').on('click', function(event) {
		event.preventDefault();
		$('#search').addClass('open');
		$('#search > form > input[type="search"]').focus();
	});
	
	$('#search, #search button.close').on('click keyup', function(event) {
		if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
			$(this).removeClass('open');
		}
	});
	
	
	//Do not include! This prevents the form from submitting for DEMO purposes only!
	$('form').submit(function(event) {
		event.preventDefault();
		return false;
	})
});

// collapse change icon
$(function(){
	$('.collapse-wrap .btn').on('click', function(){
		$(this).find('.fa').toggleClass('fa-minus');
		
		$(this).toggleClass('btn1');
		
	});
});

// navigation
$(document).on("scroll", function() {

	if($(document).scrollTop()>100) {
		$(".header").removeClass("large").addClass("shrink");
	} else {
		$(".header").removeClass("shrink").addClass("large");
	}
});
// search
$(document).ready(function(){
	$('a[href="#search"]').on('click', function(event) {                    
		$('#search').addClass('open');
		$('#search > form > input[type="search"]').focus();
	});            
	$('#search, #search button.close').on('click keyup', function(event) {
		if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
			$(this).removeClass('open');
		}
	});            
});
