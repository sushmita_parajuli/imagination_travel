<?php include 'header.php'; ?>
<div class="clearfix"></div>

<div class=" inner sightseeing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Sightseeing</li>
				</ol>
			</div>
			<h2 class="text-center">Sightseeing</h2>
			<div class="col-sm-4">
				<div class="box">
					<a href="sightcategory.php">
						<figure>
							<img src="image/service/delux.jpg" class="img-responsive"/>
							<figcaption>Kathmandu</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="sightcategory.php">
						<figure>
							<img src="image/service/delux.jpg" class="img-responsive"/>
							<figcaption>Pokhara</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<a href="sightcategory.php">
						<figure>
							<img src="image/service/delux.jpg" class="img-responsive"/>
							<figcaption>Chitwan</figcaption>
						</figure>
					</a>
				</div>
			</div>

		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
