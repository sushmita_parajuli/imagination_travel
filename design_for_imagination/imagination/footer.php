		<!-- footer wrap -->
		<footer class="footer-wrap pad clearfix">
			<div class="footer-btm clearfix">
				<div class="container">
					<div class="copyright">
						<p>&copy; Imagination Travel & Treak Pvt.Ltd. All rights reserved.</p>
					</div>
				</div>
			</div>
		</footer>
	</div> <!-- /.wrapper end -->
	<!-- Page structure end 
	================================================== -->
	<!-- core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="dist/js/jquery.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
	<script src="dist/js/bootstrap.min.js"></script>
	<script src="dist/js/owl.carousel.min.js"></script>
		<script src="dist/js/responsiveTabs.js"></script>
	<!-- lightbox -->
<!-- 	<script src="dist/js/jquery.easing.1.3.js"></script>
	<script src="dist/js/simple-lightbox.min.js"></script> -->
	<!-- end lightbox -->
	<!-- <script src="dist/js/heroslider.js"></script> -->
	<script type='text/javascript' src='dist/js/jquery.mobile.customized.min.js'></script>
	<script type='text/javascript' src='dist/js/jquery.easing.1.3.js'></script> 
	<script type='text/javascript' src='dist/js/camera.js'></script> 

	<script src="dist/js/custom.js"></script>
	
	<script type="text/javascript">
		$('.testimonial-list').owlCarousel({
			loop:true,
			margin:10,
			nav:false,
			dots: true,
			autoplay: false,
			navText: [
			"<i class='fa fa-chevron-left'></i>",
			"<i class='fa fa-chevron-right'></i>"
			],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});

		$('.hotel-slider').owlCarousel({
			loop:true,
			margin:0,
			nav:true,
			dots: false,
			autoplay: true,
			navText: [
			"<i class='fa fa-chevron-left'></i>",
			"<i class='fa fa-chevron-right'></i>"
			],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
	</script>
	<script>
		jQuery(function(){
			
			jQuery('#camera_wrap_1').camera({
				loader: 'bar',
				pagination: false,
				playPause: true,
				pauseOnClick: false,
				navigationHover: true,
				hover: false
			});
		});
	</script>
		<script>
		$(document).ready(function() {
			RESPONSIVEUI.responsiveTabs();
		})
	</script>
<!-- 	<script>
		$(function(){
			var $gallery = $('.gallery-hold a').simpleLightbox();
		});
	</script> -->
</body>
</html>