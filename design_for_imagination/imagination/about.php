<?php include 'header.php'; ?>
<div class="clearfix"></div>
	<div class="content-wrap inner clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 welcome-txt">
					<h2>Welcome to Imagination Travel</h2>
					<p>Impedit nam itaque asperiores dicta laudantium fuga corporis dolor ipsa quod voluptatem, culpa perspiciatis est rerum quasi quis officia sapiente necessitatibus. Minima.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati sed eius magnam ex provident dolorem laudantium, nulla commodi, magni eum deleniti enim, illum repellendus cum aliquam itaque dicta! Perspiciatis, ut!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quis cum illum quidem error, atque iste voluptas adipisci pariatur quaerat quisquam accusantium ad nobis alias sapiente nisi magni culpa id.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quidem eaque velit debitis, ex quaerat cum deserunt necessitatibus commodi rem temporibus illum atque expedita dicta beatae eveniet ipsa animi odio.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae beatae, repellendus nobis. Sapiente ex, eos eaque maxime nisi dignissimos vero obcaecati expedita quaerat, mollitia, deserunt, distinctio quam animi vel nesciunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim aut a ut, iure nihil laborum obcaecati. Ratione sunt explicabo eveniet odio laudantium esse facere. Magnam modi, totam commodi explicabo minus?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus asperiores facere numquam quam eligendi soluta voluptatem ab natus doloremque aspernatur provident dolorum inventore itaque molestiae, vero voluptate, voluptas amet voluptatum!</p>
				</div>
				<div class="col-sm-4 image">
					<figure>
						<img class="img-responsive" src="image/service/img10.jpg" alt="">
					</figure>
				</div>
			</div>
			
		</div>
	</div> <!-- /.content-wrap end -->
<?php include 'footer.php'; ?>