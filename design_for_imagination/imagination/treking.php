<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class=" inner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Treking</li>
				</ol>
				<div class="">
					<figure class="photo">
						<img class="img-responsive" src="image/trek.jpg" alt="">
					</figure>
				</div>
			</div>
			<div class="col-md-8">
				<div class="trek-list">
					<h2>Treaking in Nepal</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div class="col-md-4 trek-package">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Treking Activities</h3>
							</div>
							<div class="panel-body">
								<ul class="activities-links">
									<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mustang Treking</a></li>
									<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Poon Hill Treking</a></li>
									<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sagarmatha Treking</a></li>
									<li><a href="treking-details.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ghandruk Treking</a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

		<?php include 'footer.php'; ?>