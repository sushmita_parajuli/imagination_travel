<?php include 'header.php'; ?>
<div class="clearfix"></div>

<div class=" inner sightseeing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Sightseeing</a></li>
					<li class="breadcrumb-item active">Kathmandu</li>
				</ol>
			</div>
			<h2 class="text-center">Kathmandu</h2>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="image/package/kathmandu.jpg" alt="...">
					<div class="caption">
						<h3>Boudha Nath Stupa</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. </p>
						<p><a href="sightdetail.php" class="btn btn-primary" role="button">Read More</a> 
						</p>
					</div>
				</div>
			</div>
					<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="image/package/kathmandu.jpg" alt="...">
					<div class="caption">
						<h3>Boudha Nath Stupa</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. </p>
						<p><a href="sightdetail.php" class="btn btn-primary" role="button">Read More</a> 
						</p>
					</div>
				</div>
			</div>
					<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="image/package/kathmandu.jpg" alt="...">
					<div class="caption">
						<h3>Boudha Nath Stupa</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. </p>
						<p><a href="sightdetail.php" class="btn btn-primary" role="button">Read More</a> 
						</p>
					</div>
				</div>
			</div>
	

		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
