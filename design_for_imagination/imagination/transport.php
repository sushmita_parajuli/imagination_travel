<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class="content-wrap pad inner clearfix">
	<!-- service -->
	<div class="clearfix">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active">Transportation</li>
			</ol>
			<div class="row">
				<div class="clearfix">
					<div class="col-sm-12">
						<figure class="photo">
							<img class="img-responsive" src="image/trek.jpg" alt="">
						</figure>
					</div>
				</div>
				<div class="col-md-12">
					<div class="trek-list">
						<h2>Transportation in Nepal</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="responsive-tabs itinerary">
								<h2>Half day</h2>
								<div>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
										proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

									</div>
									<h2>Full Day</h2>
									<div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
										</div>
									</div>
								</div>
								<div class="book-form-wrap col-sm-10 col-sm-offset-1">
									<div class="book-form-holder">
										<div class="heading">
											<h2 class="text-center">Book Now</h2>
										</div>
										<div class="form-wrapper clearfix">
											<div class="col-sm-12 holder">
												<form action="">
													<div class="col-sm-4">
														<h4 class="col-sm-12">Package Info</h4>
														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="name">Name</label> -->
															<div class="col-sm-12">
																<input type="text" class=" form-control" name="package name" placeholder="package name">
															</div>
														</div>
														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="name">Email</label> -->
															<div class="col-sm-12">
																<input type="email" class="form-control" name="dob" placeholder="Start Date">
															</div>

														</div>
														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="phone">Phone</label> -->
															<div class="col-sm-12">
																<input type="text" class="form-control" name="country" placeholder="End Date">
															</div>
														</div>
														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="name">Name</label> -->
															<div class="col-sm-12">
																<input type="text" class=" form-control" name="citizen no" placeholder="Number of People">
															</div>
														</div>
													</div>
													<div class="col-sm-8">
														<h4 class="col-sm-12">Personal Info</h4>
														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="name">Name</label> -->
															<div class="col-sm-12">
																<input type="text" class=" form-control" name="name" placeholder="Your Full Name">
															</div>
														</div>

														<div class="form-group clearfix">
															<!-- <label class="col-sm-2" for="phone">Phone</label> -->
															<div class="col-sm-12">
																<input type="text" class="form-control" name="country" placeholder="Enter Your Country">
															</div>
														</div>
														<div class="form-group col-sm-6">
															<!-- <label class="col-sm-2" for="name">Email</label> -->
															<div class="">
																<input type="email" class="form-control" name="dob" placeholder="Your DOB">
															</div>

														</div>
														<div class="form-group col-sm-6">
															<!-- <label class="col-sm-2" for="name">Name</label> -->
															<div class="">
																<input type="text" class=" form-control" name="citizen no" placeholder="Your citizenship No">
															</div>
														</div>
														<div class="form-group col-sm-6">
															<!-- <label class="col-sm-2" for="name">Email</label> -->
															<div class="">
																<input type="email" class="form-control" name="email" placeholder="Your Email Address">
															</div>

														</div>
														<div class="form-group col-sm-6">
															<!-- <label class="col-sm-2" for="phone">Phone</label> -->
															<div class="">
																<input type="text" class="form-control" name="phone" placeholder="Your Number">
															</div>
														</div>
														<div class="col-sm-12">
															<input type="checkbox" name="vehicle1" value="Bike"> I agree all terms and condition<br>
														</div>
														<div class="btn-wrap col-sm-12">
															<button type="submit" class="btn btn-success">Submit</button>
														</div>
													</div>


												</form>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div> <!-- /.service end -->
			</div> <!-- /.content-wrap end -->
			<?php include 'footer.php'; ?>