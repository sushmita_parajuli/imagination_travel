<!doctype html>
<html lang="en">
<head>
	<!-- meta tags and title goes here -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="UTF-8">
	<title>Imagination</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Mobile Specific Metas here -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
	<!-- CSS & google font link goes here -->
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css" media="all">
	<link href="https://fonts.googleapis.com/css?family=Arima+Madurai|Lato" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="dist/css/font-awesome.min.css" media="all">
	<link rel="stylesheet" href="dist/css/owl.carousel.css">
	<!-- <link rel="stylesheet" href="dist/css/simplelightbox.min.css"> -->
	<!-- <link rel="stylesheet" href="dist/css/heroslider.css"> -->

	<!-- custom css added -->
	<link rel="stylesheet" type="text/css" href="scss/style.css" media="all">
	<link rel="stylesheet" type="text/css" href="dist/css/custom.css" media="all">
	<link rel="stylesheet" type="text/css" href="dist/css/responsive.css" media="all">

	<!-- Favicons goes here -->
	<!-- <link rel="shortcut icon" href="image/favicon.ico"> -->
	
	<!-- js plugins goes here -->
	<script src="dist/js/modernizr.js"></script>
	<!--[if lt IE 9]>
		<script src="dist/js/html5shiv.js"></script>
		<![endif]-->
	</head>

	<body>
	<!-- Page structure start 
	================================================== -->
	<div class="wrapper">
		<a href="#" class="TopButton">
			<i class="fa fa-angle-up "></i>
		</a>
		<header class="header">
			
			<nav class="navbar navbar-default">
				<div class="info">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<span>
									<i class="fa fa-phone" aria-hidden="true"></i> 9860123456
								</span>
								<span>
									<a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@travel.com</a>
								</span>
								<span class="pull-right"><a href="#" ><span class="fa fa-instagram"></span></a></span>
								<span class="pull-right"><a href="#"><span class="fa fa-facebook"></span></a></span>
								<span class="pull-right search-icon"><a href="#search"><span class="fa fa-search"></span></a></span>
								
							</div>
						<!-- 	<div class="col-md-6 pull-right">
								<span>
								<i class="fa fa-phone" aria-hidden="true"></i> log In
								</span>
								<span>
								<i class="fa fa-envelope-o" aria-hidden="true"></i> Register
								</span>
							</div> -->
						</div>
					</div>
				</div>
				<div class="container">
					
					<div class="logo-wrap">
						<h1 class="logo clearfix">
							<a class="" href="index.php"><img src="image/logo.png" class="img-responsive"></a>
						</h1>
						
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse main-nav">
						<ul class="nav navbar-nav navbar-right">
							<li class="active"><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="booking.php">Hotel Booking</a></li>
									<li><a href="treking.php">Treking</a></li>
									<li><a href="booking.php">Mountain Flight</a></li>
									<li><a href="sightseeing.php">Shight Seeing</a></li>
									<li><a href="transport.php">Transport</a></li>
									<li><a href="booking.php">Ticketing</a></li>
								</ul>
							</li>
							<!-- <li><a href="service.php">Services</a></li> -->
							<li><a href="contact.php">Contact</a></li>
						</ul>
						
					</div><!-- /.navbar-collapse -->
				</div>
			</nav>

			<!-- Search Form -->
			<div id="search"> 
				<span class="close">X</span>
				<form role="search" id="searchform" action="/search" method="get">
					<input value="" name="q" type="search" placeholder="type to search"/>
				</form>
			</div>

		</header>