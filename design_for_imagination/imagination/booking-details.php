<?php include 'header.php'; ?>
<div class="clearfix"></div>
	<div class="content-wrap pad inner clearfix">
		<!-- service -->
		<div class="clearfix">
			<div class="container">
				<div class="row">
					<div class="rooms-wrap hotel-details clearfix">
						<div class="col-sm-12">
								<figure class="photo">
									<img class="img-responsive" src="image/service/img10.jpg" alt="">
								</figure>
						</div>
						<div class="col-sm-12 slider-wrap">
							<div class="hotel-description clearfix">
								<div class="title">
									<h2>Hotel booking</h2>
								</div>
								<div class="cont">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, aliquid hic minima iste modi deleniti et, distinctio quia cupiditate iusto quisquam porro delectus doloremque temporibus rerum sapiente sunt magnam suscipit.</p>
									
								</div>
								<div class="list-wrap clearfix">
								<h3>Most Popular Facilities</h3>
									<ul class="clearfix">
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>Bed </strong>2 Double Beds</span>
										</li>
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>View </strong>City</span>
										</li>
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>Wifi </strong>Yes</span>
										</li>
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>Room Service </strong>Yes</span>
										</li>
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>Breakfast </strong>Yes</span>
										</li>
										<li>
											<span><i class="fa fa-hand-o-right">&nbsp;</i> <strong>Hot &amp; Cold Water </strong>Yes</span>
										</li>
										
									</ul>
								</div>
								
								
							</div>
						</div>
						<div class="book-form-wrap col-sm-8">
							<div class="book-form-holder">
								<div class="heading">
									<h2>Book The Room Now</h2>
								</div>
								<div class="form-wrapper clearfix">
									<div class="col-sm-12 holder">
										<form action="">
											<div class="form-group clearfix">
												<!-- <label class="col-sm-2" for="name">Name</label> -->
												<div class="col-sm-12">
													<input type="text" class=" form-control" name="name" placeholder="Your Full Name">
												</div>
											</div>
											<div class="form-group clearfix">
												<!-- <label class="col-sm-2" for="name">Email</label> -->
												<div class="col-sm-12">
													<input type="email" class="form-control" name="email" placeholder="Your Email Address">
												</div>
												
											</div>
											<div class="form-group clearfix">
												<!-- <label class="col-sm-2" for="phone">Phone</label> -->
												<div class="col-sm-12">
													<input type="text" class="form-control" name="phone" placeholder="Your Number">
												</div>
											</div>
											<div class="btn-wrap">
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div> <!-- /.service end -->
	</div> <!-- /.content-wrap end -->
<?php include 'footer.php'; ?>