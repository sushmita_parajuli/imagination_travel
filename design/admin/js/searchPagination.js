
//page links clicks
$('div').on('click', '#pagination a', function (e) {
    e.preventDefault();
    var searchParams = {};
    var theUrl = $(this).attr('href');
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
        data: 'parameters=' + JSON.stringify(searchParams),
        url: theUrl,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
});

//sorting 
$('thead').click(function () {
    $("#tblData").tablesorter();
});
//selecting per page
//$('#per_page').on('change', function () {
//    var page = $('#per_page').val();
//    var base_url = $('.total').attr('b');
////        var simUrl = base_url + "/";
////        var theUrl = simUrl.concat(page);
////        var no_of_pages = parseInt($('.total').attr('a'));
////        if (page != 0 && page <= no_of_pages) {
//    $.ajax({
//        type: 'POST',
//        data: 'per_page=' + page,
//        url: base_url,
//        success: function (data) {
//            $('#replaceTable').html(data);
//        }
//    });
////        }
//});
//on goto page insertion
$('#goto').on('change', function () {
    var page = $('#goto').val();
    var base_url = $('.total').attr('b');
    var simUrl = base_url + "/";
    var theUrl = simUrl.concat(page);
     var searchParams = {};
     $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    var no_of_pages = parseInt($('.total').attr('a'));
    if (page != 0 && page <= no_of_pages) {
        $.ajax({
            type: 'POST',
            data: 'parameters= ' + JSON.stringify(searchParams),
            url: theUrl,
            success: function (data) {
                $('#replaceTable').html(data);
            }
        });
    }
    else {
        alert('Enter a PAGE between 1 and ' + no_of_pages);
        $('#goto').val("").focus();
        return false;
    }
});
$('#search').click(function () {
    var searchParams = {};
    var base_url = $('.total').attr('b');
    $(".searchKeys").each(function () {
        searchParams[$(this).attr('name')] = $(this).attr('data-type') + '=>' + $(this).val();
    });
    $.ajax({
        type: 'POST',
        data: 'parameters= ' + JSON.stringify(searchParams),
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
        }
    });
    return false;
});
//search
//$('#search').on('keyup', function () {
//    searchTable($(this).val());
//});
//function searchTable(inputVal)
//{
//    var table = $('#tblData');
//    table.find('tr').each(function (index, row)
//    {
//        var allCells = $(row).find('td');
//        if (allCells.length > 0)
//        {
//            var found = false;
//            allCells.each(function (index, td)
//            {
//                var regExp = new RegExp(inputVal, 'i');
//                if (regExp.test($(td).text()))
//                {
//                    found = true;
//                    return false;
//                }
//            });
//            if (found == true) {
//                $(row).show();
//            }
//            else {
//                $(row).hide();
//            }
//        }
//    });
//}